package com.shengsheng.fish.wxapi;

import org.cocos2dx.javascript.AppActivity;
import com.games.GameLib.WeixinManager;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler {

	public static final String APP_ID = "wx7aa98fd5a2f88b71"; // 微信sdk-超级
	public static final String APP_SECRET = "7774021abe3829f28fcdd65989b233a8"; // 微信sdk-超级
	//public static final String APP_ID_PAY = ""; // 微信sdk-超级
	//public static final String APP_SECRET_PAY = ""; // 微信sdk-超级

	public static final String APP_ID_PAY = "wx7aa98fd5a2f88b71"; // 微信sdk-超级
	public static final String APP_SECRET_PAY = "7774021abe3829f28fcdd65989b233a8"; // 微信sdk-超级
	public static final String PARTNER_ID = "1605471595";
	public static final String APP_KEY = "uYQdYIwS4ag2EfhQAhUHpMosMoan9KO4";


	// IWXAPI 是第三方app和微信通信的openapi接口
	private IWXAPI api;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 通过WXAPIFactory工厂，获取IWXAPI的实例
		api = WXAPIFactory.createWXAPI(this, APP_ID, false);
		api.handleIntent(getIntent(), this);
		Log.v("WXEntryActivity", "WXEntryActivity onCreate");
		api.registerApp(APP_ID);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		setIntent(intent);
		api.handleIntent(intent, this);
	}

	// 微信发送请求到第三方应用时，会回调到该方法
	@Override
	public void onReq(BaseReq req) {
		finish();

		Log.e("WXEntryActivity", "onReq");

		switch (req.getType()) {
			case ConstantsAPI.COMMAND_SHOWMESSAGE_FROM_WX: {
				Log.e("WXEntryActivity", " COMMAND_SHOWMESSAGE_FROM_WX");
				/*
				 * ShowMessageFromWX.Req req1 = (ShowMessageFromWX.Req) req;
				 * WXMediaMessage wxMsg = req1.message; WXAppExtendObject obj =
				 * (WXAppExtendObject) wxMsg.mediaObject;
				 *
				 * Intent intent = new Intent(this, MainActivity.class);
				 * intent.putExtra("title", wxMsg.title); intent.putExtra("ext",
				 * obj.extInfo); startActivity(intent);
				 */
			}
			break;
			default: {

				Intent intent = new Intent(this, AppActivity.class);
				startActivity(intent);
			}
			break;
		}
	}

	// 第三方应用发送到微信的请求处理后的响应结果，会回调到该方法
	@Override
	public void onResp(BaseResp resp) {
		int result = 0;
		Log.e("WXEntryActivity", "onResp errcode = " + resp.errCode + " errstr = "
				+ resp.errStr);

		switch (resp.errCode) {
			case BaseResp.ErrCode.ERR_OK: {
				if (resp.getType() == ConstantsAPI.COMMAND_SENDAUTH) {
					SendAuth.Resp sendResp = (SendAuth.Resp) resp;
					Log.e("texaspoker", "微信code=" + sendResp.code);
					WeixinManager.checkWxCode(sendResp.code);

				} else if (resp.getType() == ConstantsAPI.COMMAND_SENDMESSAGE_TO_WX) {
					WeixinManager.shareCallBackToJs(1);
					Toast.makeText(this, "分享成功！", Toast.LENGTH_LONG).show();
				}
			}
			break;
			case BaseResp.ErrCode.ERR_SENT_FAILED : {
				if (resp.getType() == ConstantsAPI.COMMAND_SENDAUTH) {
					WeixinManager.checkWxCode("");
				}
			}
			break;
			case BaseResp.ErrCode.ERR_UNSUPPORT  : {
				if (resp.getType() == ConstantsAPI.COMMAND_SENDAUTH) {
					WeixinManager.checkWxCode("");
				}
			}
			break;
			case BaseResp.ErrCode.ERR_COMM : {
				if (resp.getType() == ConstantsAPI.COMMAND_SENDAUTH) {
					WeixinManager.checkWxCode("");
				}
			}
			break;
			case BaseResp.ErrCode.ERR_USER_CANCEL: {
				if (resp.getType() == ConstantsAPI.COMMAND_SENDAUTH) {
					WeixinManager.checkWxCode("");
				}
			}
			break;
			case BaseResp.ErrCode.ERR_AUTH_DENIED:
				if (resp.getType() == ConstantsAPI.COMMAND_SENDAUTH) {
					WeixinManager.checkWxCode("");
				} else if (resp.getType() == ConstantsAPI.COMMAND_SENDMESSAGE_TO_WX) {
					WeixinManager.shareCallBackToJs(0);
					Toast.makeText(this, "分享失败！", Toast.LENGTH_LONG).show();
				}
				break;
			default:
				break;
		}

		this.finish();
	}

}