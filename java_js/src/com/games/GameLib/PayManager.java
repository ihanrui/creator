package com.games.GameLib;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.games.GameLib.Js;
import com.shengsheng.fish.wxapi.WXEntryActivity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;


public class PayManager {
	protected static final int PAY_GENITEM = 0;
	protected static final int PAY_DOPAY = 1;
	private static Handler m_handler = null;
	private static Activity m_activity;
	private static String payFinishedCallbackFunc;
	private final static String TAG = "PayManager";

	public static void onInit(Activity act) {
		m_activity = act;
		initHandler();
	}

	public static void PayForFee(String orderURL,int nUserID, int nPrice,
								 String strProductID, String szProductName,
								 String szAppID,String szPartnerID,String payFinishedCallback) {
		Log.v("PayManager", "PayForFee " + nUserID + "," + "ProductID" + ","
				+ strProductID + ",Url = " + orderURL);
		payFinishedCallbackFunc  = payFinishedCallback;
		Message chargeMsg = new Message();
		Bundle chargeData = new Bundle();
		chargeData.putString("orderURL", orderURL);
		chargeData.putInt("nUserID", nUserID);
		chargeData.putInt("nPrice", nPrice);
		chargeData.putString("strProductID", strProductID);
		chargeData.putString("szProductName", szProductName);
		chargeData.putString("szAppID", szAppID);
		chargeData.putString("szPartnerID", szPartnerID);
		chargeMsg.setData(chargeData);
		chargeMsg.what = PAY_GENITEM;
		Log.v("PayManager", "ready to send message");
		m_handler.sendMessage(chargeMsg);
	}

	private static void initHandler() {
		if (m_handler != null)
			return;

		m_handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);

				switch (msg.what) {
				case PAY_GENITEM: {
					Bundle chargeData = msg.getData();
					genOrder(chargeData);
					break;
				}
				case PAY_DOPAY:
					Bundle chargeData = msg.getData();
					doPay(chargeData);
					break;
				}
			}
		};
	}

	private static void doPay(Bundle chargeData) {
		if (chargeData == null)
			return;

		String orderID = chargeData.getString("PaySelfID");
		int nUserID = chargeData.getInt("UserID");
		String szAppID = chargeData.getString("AppID");
		String szPartnerID = chargeData.getString("PartnerID");
		// 调用SDK支付函数
		sdkPay(nUserID,orderID,szAppID,szPartnerID);
	}

	private static void sdkPay(int nUserID, String orderID,String szAppID,String szPartnerID) {
		Log.v("PayManager", "sdkPay order = " + orderID);
		WeixinManager.weixinPay(nUserID,orderID,szAppID,szPartnerID);
		onPayFinished(1);
	}

	private static void genOrder(final Bundle chargeData) {
		if (chargeData == null)
			return;
		/*chargeData.putString("orderURL", orderURL);
		chargeData.putInt("nUserID", nUserID);
		chargeData.putInt("nPrice", nPrice);
		chargeData.putString("strProductID", strProductID);
		chargeData.putString("szProductName", szProductName);
		chargeData.putString("szAppID", szAppID);
		chargeData.putString("szPartnerID", szPartnerID);*/
		final int nUserID = chargeData.getInt("nUserID", 0);
		final int nPrice = chargeData.getInt("nPrice", 0);
		final String strProductID = chargeData.getString("strProductID");
		final String orderURL = chargeData.getString("orderURL");
		final String szAppID = chargeData.getString("szAppID");
		final String szPartnerID = chargeData.getString("szPartnerID");
		if (nUserID <= 0 || nPrice <= 0)
			return;
		// 启动线程，取得流水号
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				String PayId = getItemBuyID(orderURL, nUserID,nPrice,strProductID,szAppID,szPartnerID);
				if (!PayId.equals("")) {
					Message recvSelfIDMsg = new Message();
					Bundle payData = new Bundle();

					payData.putString("AppID", szAppID);
					payData.putInt("UserID", nUserID);
					payData.putString("PaySelfID", PayId);
					payData.putString("PartnerID",szPartnerID);
					recvSelfIDMsg.setData(payData);
					recvSelfIDMsg.what = PAY_DOPAY;

					m_handler.sendMessage(recvSelfIDMsg);
				} else {
					// 支付失败
					onPayFinished(0);
				}

			}
		}).start();
	}

	private static String getItemBuyID(String orderURL,int nUserID, int nPrice,
			String strProductID,String szAppID,String szPartnerID) {
		// 访问地址
		String path = orderURL;

		// 注意字符串连接时不能带空格
		String result = "";

		HttpPost httpost = new HttpPost(path);// 编者按：与HttpPost区别所在，这里是将参数在地址中传递

		List<NameValuePair> params = new ArrayList<NameValuePair>();

		params.add(new BasicNameValuePair("UserID", String.valueOf(nUserID)));
		params.add(new BasicNameValuePair("Price", String.valueOf(nPrice)));
		params.add(new BasicNameValuePair("ProductID", strProductID));
		params.add(new BasicNameValuePair("PartnerID", String.valueOf(JsbGame
				.getPartnerID())));
		params.add(new BasicNameValuePair("appid", szAppID));
		params.add(new BasicNameValuePair("wxmchid", szPartnerID));

		HttpResponse response;
		try {
			httpost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
			response = new DefaultHttpClient().execute(httpost);
			if (response.getStatusLine().getStatusCode() == 200) {
				System.out.println("取得状态码");
				HttpEntity entity = response.getEntity();
				result = EntityUtils.toString(entity, HTTP.UTF_8);
				System.out.println("result:" + result);
			} else {
				System.out.println("返回的StatusCode:"
						+ response.getStatusLine().getStatusCode());
			}
		} catch (ClientProtocolException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// 预防含有标签，对标签进行过滤
		result = result.replaceAll("<[.[^<]]*>", "");

		return result;
	}

	// 调用底层，充值完毕,Result:1，成功。 其他为失败 payType 0其他 1短信
	public static void onPayFinished(final int nResult) {

	    System.out.println("nResult : " + nResult);
	    Js.call(payFinishedCallbackFunc,  nResult);
	}
}
