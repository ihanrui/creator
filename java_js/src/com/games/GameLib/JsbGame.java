package com.games.GameLib;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import org.cocos2dx.javascript.AppActivity;
import org.cocos2dx.lib.Cocos2dxActivity;
import android.content.ClipData;
import android.content.ClipboardManager; //导入需要的库
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.bun.miitmdid.interfaces.IdSupplier;

import static android.content.Context.MODE_PRIVATE;

public class JsbGame {
	private static AppActivity m_activity;
	private static String clibboardText;

	public static void init(AppActivity app) {
		m_activity = app;
		PayManager.onInit(app);
		List<String> permissionList = new ArrayList<>();
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
			permissionList.add(Manifest.permission.READ_PHONE_STATE);
		}
		permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
		permissionList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
		permissionList.add(Manifest.permission.CAMERA);
		requestPermissions(permissionList);
	}

	public static void requestPermission(String permission){
		if (ContextCompat.checkSelfPermission(m_activity,
				permission)
				!= PackageManager.PERMISSION_GRANTED){

			ActivityCompat.requestPermissions(m_activity,
					new String[]{permission},
					1);
		}
	}

	public static void requestPermissions(List<String> permissions){
		List<String> permissionList = new ArrayList<>();
		for (String p: permissions) {
			Log.v("JsbGame","requestPermissions:" + p);
			if (ContextCompat.checkSelfPermission(m_activity,
					p)
					!= PackageManager.PERMISSION_GRANTED)
			{
				permissionList.add(p);
			}
		}
		if(!permissionList.isEmpty()){
			ActivityCompat.requestPermissions(m_activity,
					permissionList.toArray(new String[permissionList.size()]),
					1);
		}
	}

	public static String getAndroidDeviceName() {
		return Build.MODEL;
	}

	public static String getPartnerID() {
        Log.v("JsbGame","getPartnerID");
		ApplicationInfo appInfo = null;
		try {
			appInfo = m_activity.getPackageManager().getApplicationInfo(
					m_activity.getPackageName(), PackageManager.GET_META_DATA);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		if (appInfo != null)
			return appInfo.metaData.getInt("qudao",10101) + "";
		return "10101";
	}

	public static String getOAID(){
		return m_activity.getOAID();
	}

	public static String getIMEI() {
		Log.v("JsbGame", "getIMEI");
		//if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P){
			try {
				final TelephonyManager manager = (TelephonyManager) m_activity.getSystemService(Context.TELEPHONY_SERVICE);
				if (manager.getDeviceId() == null || manager.getDeviceId().equals("")) {
					Log.v("JsbGame", "failed to get imei");
				} else {
					return manager.getDeviceId();
				}
			} catch (Exception e) {
				Log.v("JsbGame", e.toString());
				e.printStackTrace();
			}
			return null;
		//} else {
//			SharedPreferences pre = m_activity.getSharedPreferences("Database",MODE_PRIVATE);
//			String ret = pre.getString("my_uuid",""); //第二个参数为 取不到时的默认值
//			if(ret.equals("")) {
//				ret = UUID.randomUUID().toString();
//				SharedPreferences.Editor editor = pre.edit();
//				editor.putString("my_uuid",ret);
//				editor.commit();
//			}
			//return m_activity.getOAID();
		//}
	}

	public static String getVersionCode() {
		PackageInfo pi = null;
		try {
			pi = m_activity.getPackageManager().getPackageInfo(
					m_activity.getPackageName(), 0);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
			return "0";
		}
		return pi.versionCode + "";
	}

	public static void testReturnCall(String func) {
		Js.call(func,"testReturnCall");
	}

	public static String getCopyZoneString(){
		m_activity.runOnUiThread(
		new Runnable()
		{
			public void run()
			{
				ClipData localClipData = ((ClipboardManager)m_activity.getContext().getSystemService(Context.CLIPBOARD_SERVICE)).getPrimaryClip();
				if (localClipData != null)
				{
					ClipData.Item localItem = localClipData.getItemAt(0);
					if ((localItem != null) && (localItem.getText() != null))
						JsbGame.clibboardText = localItem.getText().toString();
				}
			}
		});
      	return clibboardText;
	}

	public static void setCopyZoneString(final String text){
		try
        {           
            Runnable runnable = new Runnable() {
                public void run() {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) m_activity.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("simple text copy", text);
                    clipboard.setPrimaryClip(clip);
                }
            };			
            m_activity.runOnUiThread(runnable); 
        }catch(Exception e){           
            e.printStackTrace();
        }
	}
	public static void sendEmail(final String email) {
		try
		{
			Runnable runnable = new Runnable() {
				public void run() {
					Uri uri = Uri.parse ("mailto: " + email);
					Intent intent = new Intent (Intent.ACTION_SENDTO, uri);
					m_activity.startActivity(intent);
				}
			};
			m_activity.runOnUiThread(runnable);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void callPhone(final String phoneNumber) {
		try {
			Runnable runnable = new Runnable() {
				public void run() {
					Uri uri = Uri.parse("tel: " + phoneNumber);
					Intent intent = new Intent(Intent.ACTION_DIAL, uri);
					m_activity.startActivity(intent);
				}
			};
			m_activity.runOnUiThread(runnable);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


    public static boolean isWxEnabled() {
        return WeixinManager.getWxAppID().length() > 0;
    }
    public static void WxLoginAction(String func) {
        WeixinManager.WxLoginAction(func);
    }

	public static boolean shareWx(int scene, int type,String uri, String title, String description,String func) {
		return WeixinManager.shareWx(scene, type, uri,  title,  description,func);
	}

	public static void PayForFee(String orderURL,int nUserID, int nPrice,
								 String strProductID, String szProductName,
								 String szAppID,String szPartnerID,String payFinishedCallback) {
		PayManager.PayForFee(orderURL,nUserID,nPrice,strProductID,szProductName,szAppID,szPartnerID,payFinishedCallback);
	}

	public static boolean exitSdk(String callback) {
		return false;
	}
}