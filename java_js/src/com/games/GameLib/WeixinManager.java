package com.games.GameLib;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.cocos2dx.lib.Cocos2dxActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.shengsheng.fish.wxapi.WXEntryActivity;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXImageObject;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXTextObject;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.tencent.mm.opensdk.modelbiz.*;

public class WeixinManager {
	public static final String TAG = "WeixinManager";
	private static IWXAPI m_wxAPI;
	private static Activity m_activity;
	private static Handler m_handler = null;
	private static String m_wxAccessToken = "";
	private static String m_wxOpenID = "";
	private static String m_wxUnionID = "";
	private static int THUMB_SIZE_WIDTH = 267;
	private static int THUMB_SIZE_HEIGHT = 150;
	private static String WX_APPID = "";
	private static String WX_APPSECRET = "";
	private static String m_refreshToken = "";
	private static String WX_APPID_PAY = "";
	private static String WX_APPSECRET_PAY = "";
	private static int WX_Sex=1;
	private static String WX_HeadURL="";
	private static String WX_NickName="";
	private static String WX_LoginCallBackFunc="";
	private static  String WX_ShareCallBackFunc="";
	//private static Boolean m_bPayAuth = false;

	public static void onInit(Activity activity, String APP_ID,
			String APP_SECRET, String strPayAppid, String strPayAppSecret) {
		WX_APPID = APP_ID;
		WX_APPSECRET = APP_SECRET;
		WX_APPID_PAY = strPayAppid;
		WX_APPSECRET_PAY = strPayAppSecret;
		m_activity = activity;

		if (WX_APPID.length() < 1)
			return;
		m_wxAPI = WXAPIFactory.createWXAPI(activity, APP_ID, true);
		if (m_wxAPI.isWXAppInstalled()) {
			Log.v("MainActivity", "register wx");
			m_wxAPI.registerApp(APP_ID);
			m_wxAPI.registerApp(WX_APPID_PAY);
		}
		initHandler();
	}

	private static void initHandler() {
		if (m_handler != null)
			return;

		m_handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);

				switch (msg.what) {
				case 1: {
					final Bundle chargeData = msg.getData();
					// 启动线程，取得流水号
					new Thread(new Runnable() {

						@Override
						public void run() {
							doAuthToken(chargeData.getString("token"));
						}
					}).start();

					break;
				}
				case 2: {
					// 启动线程，取得流水号
					new Thread(new Runnable() {

						@Override
						public void run() {
							refreshToken();
						}
					}).start();

					break;
				}
				}
			}
		};
	}

	public static boolean isWxEnabled() {
		Log.v(TAG, "isWxEnabled");
		return WX_APPID.length() > 0;
	}

	public static boolean isWxLogined() {
		return m_wxUnionID.length() > 0;
	}

	public static boolean checkWxInstall() {
		if (!m_wxAPI.isWXAppInstalled()) {
			m_activity.runOnUiThread(new Runnable() {
				public void run() {
					Builder tip = new AlertDialog.Builder(m_activity);
					tip.setTitle("提示");
					tip.setMessage("请先安装微信");
					tip.setPositiveButton("确定", null);
					tip.show();
				}
			});
			return false;
		}
		return true;
	}

	public static void WxLoginAction(String func) {
		if (!checkWxInstall())
			return;
		m_wxUnionID = "";
		m_wxAccessToken = "";
		m_wxOpenID = "";
		WX_HeadURL="";
		WX_NickName="";
		WX_LoginCallBackFunc = func;
		m_wxAPI = WXAPIFactory.createWXAPI(m_activity, WX_APPID, false);
		m_wxAPI.registerApp(WX_APPID);
		SendAuth.Req req = new SendAuth.Req();
		req.scope = "snsapi_userinfo";
		req.state = "AuthRequest";
		m_wxAPI.sendReq(req);
		Log.v(TAG, func);
	}


	public static void nativeWxLogin(String openID, String nickName,
											String headUrl)	{
		Log.e(TAG, openID);
		Log.e(TAG, nickName);
		Log.e(TAG, headUrl);
		Js.call(WX_LoginCallBackFunc,openID,nickName,headUrl,getWxAppID(),getWxOpenID());
	}


	static private Cocos2dxActivity getCtx() {
		return (Cocos2dxActivity) Cocos2dxActivity.getContext();
	}

	public static void checkWxCode(String code) {
		if (code.length() == 0) {
			nativeWxLogin("", "", "");
			return;
		}
		Message recvSelfIDMsg = new Message();
		recvSelfIDMsg.what = 1;
		Bundle chargeData = new Bundle();
		chargeData.putString("token", code);
		recvSelfIDMsg.setData(chargeData);
		m_handler.sendMessage(recvSelfIDMsg);
	}

	public static void doAuthToken(String code) {
		String strAppID = WX_APPID;
		String strAppSecret = WX_APPSECRET;
		// 访问地址
		String path = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="
				+ strAppID
				+ "&secret="
				+ strAppSecret
				+ "&code="
				+ code
				+ "&grant_type=authorization_code";

		Log.v(TAG, path);

		// 注意字符串连接时不能带空格
		String result = "";

		HttpGet httpost = new HttpGet(path);// 编者按：与HttpPost区别所在，这里是将参数在地址中传递

		HttpResponse response;
		try {
			// httpost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
			response = new DefaultHttpClient().execute(httpost);
			if (response.getStatusLine().getStatusCode() == 200) {
				System.out.println("取得状态码");
				HttpEntity entity = response.getEntity();
				result = EntityUtils.toString(entity, HTTP.UTF_8);
				System.out.println("result:" + result);
			} else {
				System.out.println("返回的StatusCode:"
						+ response.getStatusLine().getStatusCode());
			}
		} catch (ClientProtocolException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// 预防含有标签，对标签进行过滤
		Log.v(TAG, "获取token:" + result);

		try {
			JSONArray resultArray = new JSONArray("[" + result + "]");
			JSONObject resultObj = resultArray.getJSONObject(0);
			;
			m_wxOpenID = resultObj.getString("openid");
			m_wxAccessToken = resultObj.getString("access_token");
			m_wxUnionID = resultObj.getString("unionid");
			m_refreshToken = resultObj.getString("refresh_token");
			getWxUserInfo();
			return;
		} catch (JSONException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		nativeWxLogin("", "", "");
	}

	public static void getWxUserInfo() {
		if (m_wxAccessToken == "") {
			nativeWxLogin("", "", "");
			return;
		}

		// 访问地址
		String path = "https://api.weixin.qq.com/sns/userinfo?access_token="
				+ m_wxAccessToken + "&openid=" + m_wxOpenID;

		// 注意字符串连接时不能带空格
		String result = "";

		HttpGet httpost = new HttpGet(path);// 编者按：与HttpPost区别所在，这里是将参数在地址中传递

		HttpResponse response;
		try {
			// httpost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
			response = new DefaultHttpClient().execute(httpost);
			if (response.getStatusLine().getStatusCode() == 200) {
				System.out.println("取得状态码");
				HttpEntity entity = response.getEntity();
				result = EntityUtils.toString(entity, HTTP.UTF_8);
				System.out.println("result:" + result);
			} else {
				System.out.println("返回的StatusCode:"
						+ response.getStatusLine().getStatusCode());
			}
		} catch (ClientProtocolException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// 预防含有标签，对标签进行过滤
		Log.v(TAG, "获取用户信息:" + result);
		try {
			JSONArray resultArray = new JSONArray("[" + result + "]");
			JSONObject resultObj = resultArray.getJSONObject(0);
			String openID = resultObj.getString("openid");
			String nickname = resultObj.getString("nickname");
			String headUrl = resultObj.getString("headimgurl");
			WX_Sex = resultObj.getInt("sex");
			WX_HeadURL = headUrl;
			WX_NickName = nickname;
			Log.v("LogonManager", "用户ID:" + m_wxOpenID + ",nickname="
					+ nickname + ",headurl = " + headUrl);
			nativeWxLogin(m_wxUnionID, nickname, headUrl);
			return;
		} catch (JSONException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		nativeWxLogin(m_wxUnionID, JsbGame.getAndroidDeviceName(), "");
	}

	private static void refreshToken() {
		// Log.v("WeixinManager","refreshToken");
		if (m_refreshToken.length() < 2)
			return;
		// 访问地址
		String path = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid="
				+ WX_APPID
				+ "&grant_type=refresh_token&refresh_token="
				+ m_refreshToken;

		// 注意字符串连接时不能带空格
		String result = "";

		HttpGet httpost = new HttpGet(path);// 编者按：与HttpPost区别所在，这里是将参数在地址中传递

		HttpResponse response;
		try {
			// httpost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
			response = new DefaultHttpClient().execute(httpost);
			if (response.getStatusLine().getStatusCode() == 200) {
				System.out.println("取得状态码");
				HttpEntity entity = response.getEntity();
				result = EntityUtils.toString(entity, HTTP.UTF_8);
				System.out.println("result:" + result);
			} else {
				System.out.println("返回的StatusCode:"
						+ response.getStatusLine().getStatusCode());
			}
		} catch (ClientProtocolException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			JSONArray resultArray = new JSONArray("[" + result + "]");
			JSONObject resultObj = resultArray.getJSONObject(0);
			m_wxAccessToken = resultObj.getString("access_token");
			m_refreshToken = resultObj.getString("refresh_token");
			return;
		} catch (JSONException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	//分享回调  resultCode = 1 成功 0是失败
	public static void shareCallBackToJs(int resultCode)
	{
		if(!"".equals(WX_ShareCallBackFunc))
		{
			Js.call(WX_ShareCallBackFunc,resultCode);
		}
	}


	// 分享相关
	// shareScene,分享场景
	// type,媒体类型，0：text，1：image，2：url
	// public static boolean shareWx(int scene, int type, byte[] imageData,
	//			String uri, String title, String description)

	public static boolean shareWx(int scene, int type,
			String uri, String title, String description,String func) {
		Log.v("WeixinManager", "shareWx type = " + type);
		if (!checkWxInstall())
			return false;
		WX_ShareCallBackFunc = func;
		if (type == 0)
			return shareWxText(scene, description);
		else if (type == 2)
			return shareWxUrl(scene, uri, title, description);
		return true;
	}

	private static boolean shareWxText(int scene, String description) {
		// 初始化一个WXTextObject对象
		WXTextObject textObj = new WXTextObject();
		textObj.text = description;

		// 用WXTextObject对象初始化一个WXMediaMessage对象
		WXMediaMessage msg = new WXMediaMessage();
		msg.mediaObject = textObj;
		// 发送文本类型的消息时，title字段不起作用
		// msg.title = "Will be ignored";
		msg.description = description;

		// 构造一个Req
		SendMessageToWX.Req req = new SendMessageToWX.Req();
		req.transaction = buildTransaction("text"); // transaction字段用于唯一标识一个请求
		req.message = msg;
		req.scene = scene;

		// 调用api接口发送数据到微信
		m_wxAPI.sendReq(req);
		return true;
	}

	private static boolean shareWxImage(int scene, byte[] imageData) {
		try {
			WXImageObject imgObj = new WXImageObject();
			imgObj.imageData = imageData;
			WXMediaMessage msg = new WXMediaMessage();
			msg.mediaObject = imgObj;
			Bitmap bmp = ImageTools.byteToBitmap(imageData);
			int sizeWidth = THUMB_SIZE_WIDTH;
			int sizeHeight = THUMB_SIZE_HEIGHT;
			Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, sizeWidth,
					sizeHeight, true);
			msg.thumbData = ImageTools.bmpToByteArray(thumbBmp, true);
			while(msg.thumbData.length > 32700)
			{
				sizeWidth *= 0.8;
				sizeHeight *= 0.8;
				thumbBmp = Bitmap.createScaledBitmap(bmp, sizeWidth,
						sizeHeight, true);
				msg.thumbData = ImageTools.bmpToByteArray(thumbBmp, true);
				Log.v("WeixinManage","msg.thumbData.length = " + msg.thumbData.length);
			}
			bmp.recycle();


			SendMessageToWX.Req req = new SendMessageToWX.Req();
			req.transaction = buildTransaction("img");
			req.message = msg;
			req.scene = scene;
			Log.v("WeixinManager", "send image share req len = "
					+ msg.thumbData.length);
			m_wxAPI.sendReq(req);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private static Bitmap getAppIcon() {
		PackageManager packageManager = null;
		ApplicationInfo applicationInfo = null;
		try {
			packageManager = m_activity.getApplicationContext()
					.getPackageManager();
			applicationInfo = packageManager.getApplicationInfo(
					m_activity.getPackageName(), 0);
		} catch (PackageManager.NameNotFoundException e) {
			applicationInfo = null;
		}
		Drawable icon = packageManager.getApplicationIcon(applicationInfo);
		if (icon instanceof BitmapDrawable) {
			return ((BitmapDrawable) icon).getBitmap();
		} else {
			return Bitmap.createBitmap(icon.getIntrinsicWidth(),
					icon.getIntrinsicHeight(), Config.ARGB_8888);
		}
	}

	private static boolean shareWxImageByPath(int scene, String filePath) {
		Log.v("WeixinManager", "shareWxImageByPath filePath = " + filePath);
		try {
			Bitmap bmp = BitmapFactory.decodeFile(filePath);
			WXImageObject imgObj = new WXImageObject(bmp);
			WXMediaMessage msg = new WXMediaMessage();
			msg.mediaObject = imgObj;
			int sizeWidth = THUMB_SIZE_WIDTH;
			int sizeHeight = THUMB_SIZE_HEIGHT;
			Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, sizeWidth,
					sizeHeight, true);
			msg.thumbData = ImageTools.bmpToByteArray(thumbBmp, true);
			while(msg.thumbData.length > 32700)
			{
				sizeWidth *= 0.8;
				sizeHeight *= 0.8;
				thumbBmp = Bitmap.createScaledBitmap(bmp, sizeWidth,
						sizeHeight, true);
				msg.thumbData = ImageTools.bmpToByteArray(thumbBmp, true);
				Log.v(TAG,"msg.thumbData.length = " + msg.thumbData.length);
			}
			bmp.recycle();


			SendMessageToWX.Req req = new SendMessageToWX.Req();
			req.transaction = buildTransaction("img");
			req.message = msg;
			req.scene = scene;
			Log.v("WeixinManager", "send image share req len = "
					+ msg.thumbData.length);
			m_wxAPI.sendReq(req);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private static boolean shareWxUrl(int scene, String uri, String title,
			String description) {
		try {
			FileInputStream fis = new FileInputStream(uri);
			Bitmap bitmap = BitmapFactory.decodeStream(fis);
			if (bitmap != null) {
				return shareWxImageByPath(scene, uri);
			}
		} catch (FileNotFoundException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		WXWebpageObject webpage = new WXWebpageObject();
		webpage.webpageUrl = uri;
		WXMediaMessage msg = new WXMediaMessage(webpage);
		msg.title = title;
		msg.description = description;
		Bitmap appIcon = getAppIcon();
		if (appIcon != null) {
			Bitmap thumbBmp = Bitmap.createScaledBitmap(appIcon, 72, 72, true);
			if (thumbBmp != null) {
				msg.thumbData = ImageTools.bmpToByteArray(thumbBmp, true);
			}
		}
		SendMessageToWX.Req req = new SendMessageToWX.Req();
		req.transaction = buildTransaction("webpage");
		req.message = msg;
		req.scene = scene;
		m_wxAPI.sendReq(req);
		return true;
	}

	public static void JumpToBizProfile(String strBizProfile) {

	}

	private static String buildTransaction(final String type) {
		return (type == null) ? String.valueOf(System.currentTimeMillis())
				: type + System.currentTimeMillis();
	}

	public final static String MD5(String s) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'A', 'B', 'C', 'D', 'E', 'F' };

		try {
			byte[] btInput = s.getBytes();

			MessageDigest mdInst = MessageDigest.getInstance("MD5");

			mdInst.update(btInput);

			byte[] md = mdInst.digest();

			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private static String genNonceStr() {
		Random random = new Random();
		return MD5(String.valueOf(random.nextInt(10000)));
	}

	private static long genTimeStamp() {
		return System.currentTimeMillis() / 1000;
	}

	private static String genAppSign(List<NameValuePair> params, String strKey) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < params.size(); i++) {
			sb.append(params.get(i).getName());
			sb.append('=');
			sb.append(params.get(i).getValue());
			sb.append('&');
		}
		sb.append("key=");
		sb.append(strKey);
		Log.v(TAG, "genAppSign = " + sb.toString());

		// sb.append("sign str\n" + sb.toString() + "\n\n");
		String appSign = MD5(sb.toString()).toUpperCase();
		Log.e("orion", appSign);
		return appSign;
	}

	public static boolean weixinPay(int nUserID, String strPrePayID,String strAppPayID,String strWxPartnerID) {
		String strAppKey = WXEntryActivity.APP_KEY;
		Log.v("weixinPay", "partnerID = " + strWxPartnerID + ",strPrePayID = "
				+ strPrePayID + ",strAppKey = " + strAppKey);
		PayReq request = new PayReq();
		if (strAppPayID.length() <= 0)
			request.appId = WX_APPID_PAY;
		else {
			Log.v("WeixinManager","appid = " + strAppPayID);
			//m_wxAPI = WXAPIFactory.createWXAPI(m_activity, strAppPayID, true);
			m_wxAPI.registerApp(strAppPayID);
			request.appId = strAppPayID;
		}

		request.partnerId = strWxPartnerID;
		request.prepayId = strPrePayID;
		request.packageValue = "Sign=WXPay";
		request.nonceStr = genNonceStr();
		request.timeStamp = String.valueOf(genTimeStamp());
		List<NameValuePair> signParams = new LinkedList<NameValuePair>();
		signParams.add(new BasicNameValuePair("appid", request.appId));
		signParams.add(new BasicNameValuePair("noncestr", request.nonceStr));
		signParams.add(new BasicNameValuePair("package", request.packageValue));
		signParams.add(new BasicNameValuePair("partnerid", request.partnerId));
		signParams.add(new BasicNameValuePair("prepayid", request.prepayId));
		signParams.add(new BasicNameValuePair("timestamp", request.timeStamp));
		request.sign = genAppSign(signParams, strAppKey);
		Log.v(TAG,"sending pay request");
		m_wxAPI.sendReq(request);
		return true;
	}

	public static String getWxOpenID() {
		return m_wxOpenID;
	}

	public static String getWxAppID() {
		return WX_APPID;
	}

	public static int WX_GetLoginSex() {
		return WX_Sex;
	}
	public static String WX_GetLoginHeadURL() {
		return WX_HeadURL;
	}
	public static String WX_GetLoginNickName() {
		return WX_NickName;
	}
}
