package org.cocos2dx.javascript;

import android.content.Context;
import android.util.Log;

//import com.tencent.bugly.crashreport.CrashReport;

import com.games.GameLib.JsbGame;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BuglyAgent {
    public static void initSDK(Context context, String appId) {
        //CrashReport.initCrashReport(context.getApplicationContext(), appId, false);
    }

    public static void postException(int category, String name, String reason, String stack){
        Log.d("cocosdebug",name);
        String serverIp="http://fishlog.ss2007.com/clientlog.aspx";
        String str="UserId=8888&msg=";
        String msg = category+name+reason+stack;


        try {
            msg = URLEncoder.encode(msg,"utf-8");

            String path=serverIp; //+ str+msg;

            HttpPost httpost = new HttpPost(path);// 编者按：与HttpPost区别所在，这里是将参数在地址中传递

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("UserId", String.valueOf(8888)));
            params.add(new BasicNameValuePair("Msg", msg));

            HttpResponse response;

            httpost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
            response = new DefaultHttpClient().execute(httpost);
            if (response.getStatusLine().getStatusCode() == 200) {
                System.out.println("取得状态码");
                HttpEntity entity = response.getEntity();
                String result = EntityUtils.toString(entity, HTTP.UTF_8);
                System.out.println("result:" + result);
            } else {
                System.out.println("返回的StatusCode:"
                        + response.getStatusLine().getStatusCode());
            }
        } catch (ClientProtocolException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    //public static void postException(int category, String name, String reason, String stack, Map<String, String> extraInfo){
        //CrashReport.postException(category, name, reason, stack, extraInfo);

    //}
}
