
import { WebCallback } from "../web/WebData";

export class GameInvoker {

    private static callbacks: Array<WebCallback>;

    public static init(len: number) {
        GameInvoker.callbacks = new Array(len);
    }
    public static register(index: number, target: any, func: Function) {
        let callback = new WebCallback();
        callback.Target = target;
        callback.Listener = func;
        GameInvoker.callbacks[index] = callback;
    }
    public static removeListener(index: number) {
        if (GameInvoker.callbacks[index]) GameInvoker.callbacks[index] = null;
    }
    public static call(index: number, ...arg: any) {
        let callback = GameInvoker.callbacks[index];
        if (callback) {
            return callback.Listener.call(callback.Target, ...arg);
        }
        return null;
    }
    public static clear() {
        GameInvoker.callbacks.length = 0;
    }
}
