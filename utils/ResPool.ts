import { Prefab, Node, instantiate, isValid, Component } from 'cc';

export class ResPool<T extends Component>{
    public type: T;//脚本类型
    public m_pre: any;//可以支持摆放在场景里的节点
    public m_pool: Array<any>;//脚本数组
    public opacity: number;//最大缓存数量

    constructor(type: any, pre: Prefab | Node, opacity: number) {
        this.type = type;
        this.m_pre = pre;
        this.opacity = opacity;
        this.m_pool = new Array();
    }

    /**
     * @returns 返回node上绑定的脚本
     */
    public alloc(): T {
        let element = null;
        if (this.m_pool.length > 0) element = this.m_pool.pop();
        if (!element) {
            let node = instantiate(this.m_pre);
            element = node.getComponent(this.type);
        }
        return element;
    }
    public recycle(element: any) {
        if (!isValid(element)) {
            console.warn("回收了无效內容:", element);
            return;
        }
        // if (this.m_pool.indexOf(element) > -1) {
        //     console.log("重复回收", element);
        //     console.log(this.m_pool);
        //     console.trace();
        //     return;
        // }
        if (this.m_pool.length < this.opacity) {
            if (element.reset) element.reset();
            if (element.node) element.node.setParent(null);
            this.m_pool.push(element);
        } else {
            element.node.destroy();
        }
    }
    public recyAll(arr: Array<any>) {
        if(arr.length == 0)return;
        let recy_count = Math.min(arr.length, this.opacity - this.m_pool.length);
        for (let i = 0; i < recy_count; i++) {
            let element = arr[i];
            if (element.reset) element.reset();
            if (element.node) element.node.setParent(null);
            this.m_pool.push(element);
        }
        for(let i = recy_count; i < arr.length; i++){
            arr[i].node.destroy();
        }
        arr.length = 0;
    }
    public clear() {
        console.log("clear");
        for (let i = this.m_pool.length - 1; i >= 0; i--) {
            if (isValid(this.m_pool[i].node)) {
                this.m_pool[i].node.destroy();
            }
        }
        this.m_pool.length = 0;
    }
}
