import { Label, tween, Tween, UITransform, Vec3, _decorator } from 'cc';

const limit = [1000000000000, 1000000000, 1000000, 1000];
const chars = ["T", "B", "M", "K"];
export const SexKey = ["us_unspecified", "us_male", "us_female"];

const Time_Desc = ["d","h","m","s"];

export class UtilsFormat {
    public static toThousands(v: number) {
        let prefix = "";
        if (v < 0) {
            prefix = "-";
            v = -v;
        }
        let s = "" + v;
        let p = s.indexOf(".");
        let i = p >= 0 ? p : s.length;
        let str = "";
        while (i > 0) {
            if (!!str) {
                str = "," + str;
            }
            str = s.substring(i > 3 ? i - 3 : 0, i) + str;
            i -= 3;
        }
        if (p >= 0) {
            str = str + s.substring(p, s.length);
        }
        return !!prefix ? prefix + str : str;
    }

    //动态字符串 需要父节点要mask
    public static setMoveName(labelNode: Label, name: string) {
        labelNode.string = name;
        labelNode.updateRenderData(true);
        let nameMoveWidth = labelNode.getComponent(UITransform).width - labelNode.node.parent.getComponent(UITransform).width;

        Tween.stopAllByTarget(labelNode.node);
        labelNode.node.setPosition(0, 0);

        if (nameMoveWidth > 0) {
            let speed = labelNode.node.parent.getComponent(UITransform).width / 3;
            tween(labelNode.node)
                .delay(2)
                .to(3, { position: new Vec3(-labelNode.node.parent.getComponent(UITransform).width, 0, 0) })
                .delay(0.5)
                .call(() => { labelNode.node.toX(labelNode.getComponent(UITransform).width); })
                .to(labelNode.getComponent(UITransform).width / speed, { position: new Vec3(0, 0, 0) })
                .union()
                .repeatForever()
                .start()
        }
    }

    public static getShortName(name: string, num: number = 10, isReversed = false) {
        if (!isReversed) return name.length > num ? name.slice(0, num) + '...' : name;
        else return name.length > num ? '...' + name.slice(name.length - num, name.length) : name;
    }

    public static formatRate(rate: number) {
        return Math.floor(rate * 10000) / 100 + "%";
    }

    public static formatMoney(num: number) {
        //避免低于10000的金额被格式化
        if (Math.abs(num) < 10000) {
            return "" + num;
        }
        num = Math.floor(num / 1000) * 1000;
        let is_minus = num < 0;
        if (is_minus) {
            num = -num;
        }
        for (let i = 0; i < 4; i++) {
            if (num >= limit[i]) {
                num = (num * 1) / limit[i];
                let remain = num % 1;
                let str = "";
                if (remain > 0) {
                    str = remain >= 0.01 ? num.toFixed(2) : Math.floor(num) + "";
                } else {
                    str += num;
                }
                return is_minus ? "-" + str + chars[i] : str + chars[i];
            }
        }
        return is_minus ? "-" + UtilsFormat.toThousands(num) : UtilsFormat.toThousands(num);
    }

    public static PrefixInteger(num, n) {
        return (Array(n).join("0") + num).slice(-n);
    }

    public static schoolServerTime(s) {
        return s;
    }

    public static addZero(n) {
        if (n < 10) return "0" + n;
        return "" + n;
    }

    public static getDateStr(date: Date, format_str:string) {
        let str = "";
        let temp = "";
        for (let i = 0, len = format_str.length; i < len; i++) {
            temp = "";
            switch (format_str[i]) {
                case "n":
                    temp = date.getFullYear() + "";
                    temp = temp.slice(2, 4);
                    break;
                case "N":
                    temp = date.getFullYear() + "";
                    break;
                case "Y":
                case "y":
                    temp = UtilsFormat.addZero(date.getMonth() + 1);
                    break;
                case "R":
                case "r":
                    temp = UtilsFormat.addZero(date.getDate());
                    break;
                case "S":
                case "s":
                    temp = UtilsFormat.addZero(date.getHours());
                    break;
                case "F":
                case "f":
                    temp = UtilsFormat.addZero(date.getMinutes());
                    break;
                case "M":
                case "m":
                    temp = UtilsFormat.addZero(date.getSeconds());
                    break;
            }
            if (temp != "") {
                str = str + temp;
            } else {
                str = str + format_str[i];
            }
        }
        return str;
    }
    /** !#zh isToday 判断是否为今天
     * @param {any} timestamp 时间戳
     */
    public static isToday(timestamp) {
        if (new Date(timestamp).toDateString() === new Date().toDateString()) {
            return true;
        }
        return false;
    }
    /**几天前 */
    public static getDaysAgo(timestamp: Date) {
        let curStamp = new Date().getTime()
        const aDayStamp = 1000 * 60 * 60 * 24
        let offset = (curStamp - timestamp.getTime())
        return Math.ceil(offset / aDayStamp)
    }

    public static getCountdown(s: number, strs: Array<string>) {
        if (!strs) strs = ["d","h","m"];
        if (s > 86400) {
            let all_hour = Math.floor(s / 3600);
            let hour = all_hour % 24;
            let day = (all_hour - hour) / 24;
            return `${day}${strs[0]}:${hour}${strs[1]}`;
        } else {
            let all_minute = Math.floor(s / 60);
            let minute = all_minute % 60;
            let hour = (all_minute - minute) / 60;
            return `${hour}${strs[1]}:${minute}${strs[2]}`;
        }
    }
    public static getCountdown2(s:number, format_str:string, desc:Array<string>, limit:number){
        let str = "";
        if(!desc)desc = Time_Desc;
        let count = 0;
        for (let i = 0, len = format_str.length; i < len; i++) {
            switch (format_str[i]) {
                case "R":
                case "r":
                    count = Math.floor(s / 86400);
                    if(count > 0){
                        str += (count + desc[0]);
                        s -= count * 86400;
                        limit--;
                    }
                    break;
                case "S":
                case "s":
                    count = Math.floor(s / 3600);
                    if(count > 0){
                        str += (count + desc[1]);
                        s -= count * 3600;
                        limit--;
                    }
                    break;
                case "F":
                case "f":
                    count = Math.floor(s / 60);
                    if(count > 0){
                        str += (count + desc[2]);
                        s -= count * 60;
                        limit--;
                    }
                    break;
                case "M":
                case "m":
                    if(s >= 0){
                        str += (s + desc[3]);
                        limit--;
                    }
                    break;
            }
            if(limit == 0)break;
        }
        return str;
    }

    public static firstUpper(str: string) {
        return str[0].toUpperCase() + str.slice(1);
    }
}
