

import { _decorator, Sprite, Label, Button, Node, UITransform } from "cc";
import { DialogCommon } from "../common/DialogCommon";
import { Dialog, DialogAction } from "../dialog/Dialog";
import { GameFrame } from "../gameframe/gameframe";
import { UtilsFormat } from "../utils/UtilsFormat";
import { UtilsPanel } from "../utils/UtilsPanel";
import { own_info } from "../OwnInfo";
import { UtilsDeco } from "../utils/UtilsDeco";
import { DecorationType } from "../data/ShopBag";

const { ccclass, property } = _decorator;

const CD = 5;

@ccclass
export class PlayerInfo extends Dialog {
    public play_index: DialogAction = DialogAction.None;

    public cache_mode: number = 2;
    public auto_mask = 2;
    public mask_opacity = 0;
    public game_dialog = true;

    private uitrans_bg: UITransform = null;
    private sp_head: Sprite = null;
	private sp_frame: Sprite = null;
	private sp_sex: Sprite = null;
	private node_gold: Node = null;
	private lb_name: Label = null;
	private lb_gold: Label = null;
	private lb_id: Label = null;
	private node_prop: Node = null;

    @property
    public have_interact = false;

    private data: any = null;

    private sps_btn: Array<Sprite>;

    private send_flag = 0;
    private times: Array<number>;
    private residue_time: Array<number>;
    /*
        data:玩家数据
        pos:[0,0] 需要显示在的位置
        auto_align:屏蔽互动时是否需要下移100像素
        hide_gold:是否屏蔽金币
    */
    public onLoad() {
        UtilsPanel.getAllNeedCom(this, this.node,true);
        if (this.have_interact) {
            let len = 12;
            let nodes = UtilsPanel.getArrCom(this.node_prop, "btn", len);
            this.sps_btn = UtilsPanel.getArrCom(this.node_prop, "sp", len, Sprite);
            this.times = new Array(len).fill(0);
            this.residue_time = new Array(len).fill(0);
            for (let i = 0; i < len; i++) {
                let btn = UtilsPanel.addBtnEvent2(nodes[i], "sendProp", this, i.toString());
                btn.transition = Button.Transition.NONE;
            }
        }
    }
    public init(data: any) {
        DialogCommon.removeLoading();
        let user_data = this.data = data.data;
        console.log(user_data);
        let pos = data.pos;
        this.sp_head.spriteFrame = null;
        UtilsPanel.setHead(user_data.FaceId, user_data.FaceUrl, this.sp_head, 100);
        let item_id = UtilsDeco.getUserDeco(data, DecorationType.HeadFrame);
        UtilsDeco.setHeadFrame("game", item_id, this.sp_frame);
        this.sp_sex.spriteFrame = UtilsPanel.getSexIcon(user_data.Sex);
        // UtilsPanel.setVipIcon(data.Vip, this.sp_vip);
        this.lb_name.string = UtilsFormat.getShortName(user_data.NickName);
        this.lb_id.string = "ID:" + user_data.UserId;
        
        this.node_gold.active = !data.hide_gold;
        this.lb_gold.string = data.hide_gold ? "" : UtilsFormat.formatMoney(user_data.Gold);
        
        if (this.have_interact) {
            let close_interaction = own_info.UserId == this.data.UserId;
            let height = 360, add_y = 0;
            this.node_prop.active = !close_interaction;
            if (close_interaction) {
                if(data.auto_align)add_y = -200;
                height = 160;
            }
            else{
                this.resetCd();
            }
            this.uitrans_bg.height = height;
            this.node.toXY(pos[0], pos[1] + add_y);
        }
    }

    public sendProp(e: any, i: string) {
        let index = parseInt(i);
        let flag = 1 << index;
        if ((this.send_flag & flag) > 0) return;
        this.send_flag |= flag;
        this.times[index] = new Date().getTime();
        this.residue_time[index] = CD;
        GameFrame.getInstance().sendGameCmd("CMD_TABLECHAT", "3/" + i + "/" + own_info.UserId + "/" + this.data.UserId);
        this.hide();
    }
    public resetCd() {
        let cur_time = new Date().getTime();
        for (let i = 0; i < 12; i++) {
            let flag = 1 << i;
            if ((this.send_flag & flag) == 0 || this.times[i] <= 0) continue;
            let offset = cur_time - this.times[i];
            if (offset >= 5000) {
                this.send_flag &= (~flag);
                this.times[i] = 0;
                this.residue_time[i] = 0;
                this.sps_btn[i].fillRange = 0;
            }
            else {
                this.residue_time[i] = CD - offset / 1000;
                this.sps_btn[i].fillRange = this.residue_time[i] / CD;
            }
        }
    }
    public update(dt: number) {
        if (!this.have_interact) return;
        if (this.send_flag == 0) return;
        for (let i = 0; i < 12; i++) {
            let flag = 1 << i;
            if ((this.send_flag & flag) == 0) continue;
            this.residue_time[i] -= dt;
            if (this.residue_time[i] <= 0) {
                this.residue_time[i] = 0;
                this.send_flag &= (~flag);
            }
            this.sps_btn[i].fillRange = this.residue_time[i] / CD;
        }
    }

    // private initFriendState() {
    //     this.updateFriendState(111)//都隐藏

    //     if (this.data.UserId == own_info.UserId) return
    //     log('searchUser', this.data.UserId)
    //     WebControl.getInstance().searchUser(this.data.UserId, this, this.onSearchResult);
    //     this.btn_friend.node.on('click', this.onClickAddFriend, this)
    // }

    // private onClickAddFriend() {
    //     this.friendApply(this.data.UserId)
    // }

    // private onSearchResult(data) {
    //     log('onSearchResult', data)
    //     let obj = JSON.parse(data);
    //     if (obj.FriendID == 0) {
    //         DialogCommon.removeLoading();
    //         DialogCommon.showTip1(i18nMgr.getString(105));
    //         return;
    //     }

    //     this.updateFriendState(obj.IsFriend)
    // }

    // //添加好友
    // public friendApply(UserId) {
    //     WebControl.getInstance().friendApply(UserId, this, (data) => {
    //         DialogCommon.removeLoading();
    //         //申请好友  0=不能申请自己  1=申请成功  2=已经是好友  3=已经申请，待审核
    //         log('friendApply',data)
            
    //         this.updateFriendState(2)

    //         //不能申请自己   已经是好友  申请成功等待审核
    //         DialogCommon.showTip1(`${i18nMgr.getString(data == 0 ? 180 : data == 2 ? 181 : 128)}`);
    //     });

    //     HallControl.getInstance().trackRecord(64);//点击添加好友按钮
    // }

    // //  
    // //IsFriend 0=不是好友  1=好友  2=已申请(自己申请的，待对方审核)  3=待审核（对方申请的，待自己审核）
    // updateFriendState(state) {
    //     this.sp_is_friend.node.active = state == 1
    //     this.sp_wait_friend.node.active = state == 2
    //     this.btn_friend.node.active = state == 0
        
    //     this.lb_friend.node.active = state == 0
    // }
}
