import { _decorator, Component, Tween, Node, tween, v3, view, UITransform } from 'cc';
const { ccclass, property } = _decorator;

import { HallControl } from "../HallControl";
import { DialogMgr } from "../manager/DialogMgr"

/**
 * 声明周期函数顺序
 * show->init->showAnim->afterShow->
 * hide->hideAnim->DialogMgr.Hide
 */
export enum DialogAction {
    None,
    Scale,
    R2L,
    L2R,
    B2T,
    T2B,
    Auto,
}

@ccclass
export class Dialog extends Component {
    //1 缩放  2 从右进入 
    public play_index: DialogAction = DialogAction.Scale;
    public hide_index: DialogAction = DialogAction.Auto;
    public cache_mode = 0;
    // 0 不创建背景  1 创建 2点击背景关闭界面
    public auto_mask = 1;
    // 背景透明度
    public mask_opacity = 0.8;
    // 是否为全屏界面
    public fill_screen?: boolean;
    public game_dialog?: boolean;
    public hold_index = -1;
    public auto_hide = false;

    protected dialog_name = "";

    protected action_tween: Tween<Node> = null;

    public init(data: any) {
    }
    public updateData(data: any) { }

    protected afterShow() { }
    protected afterHide(info: any) {
        DialogMgr.afterHide(info);
    }
    public show(data: any) {
        this.init(data);
        if (this.dialog_name.length > 0) HallControl.getInstance().trackRecord("打开界面", this.dialog_name);
        if (this.play_index > 0) {
            this.showAnim();
        }
    }

    protected showAnim() {
        if (this.action_tween) this.action_tween.stop();
        this.action_tween = tween(this.node);
        let vise_size = view.getVisibleSize();
        let trans = this.node.getComponent(UITransform);
        let begin = 0, end = 0;
        switch (this.play_index) {
            case DialogAction.Scale:
                this.node.toScaleXY(0.6);
                // this.node.toOpacity(0.4);
                this.action_tween.toPro(0.2, { scale: 1 }, { easing: "backOut" });
                // this.action_tween.toPro(0.2, { scale: 1 }, {easing:"backOut"})
                break;
            case DialogAction.R2L:
                begin = vise_size.width / 2 + trans.width * trans.anchorX;
                end = begin - trans.width;
                this.node.toX(begin);
                this.action_tween.toPro(0.2, { x: end }, { easing: "quadOut" });
                break;
            case DialogAction.L2R:
                begin = -vise_size.width / 2 - trans.width * (1 - trans.anchorX);
                end = begin + trans.width;
                this.node.toX(begin);
                this.action_tween.toPro(0.2, { x: end }, { easing: "quadOut" });
                break;
            case DialogAction.B2T:
                begin = -vise_size.height / 2 - trans.height * (1 - trans.anchorY);
                end = begin + trans.height;
                this.node.toY(begin);
                this.action_tween.toPro(0.2, { y: end });
                break;
            case DialogAction.T2B:
                begin = vise_size.height / 2 + trans.height * (1 - trans.anchorY);
                end = begin - trans.height;
                this.node.toY(begin);
                this.action_tween.toPro(0.2, { y: end });
                break;
        }
        this.action_tween.call(() => {
            this.action_tween = null;
            this.afterShow();
        }).start();
    }
    protected onCloseClick() {
        this.hide();
    }
    protected hide() {
        DialogMgr.hide(this);
        if (this.dialog_name.length > 0) HallControl.getInstance().trackRecord("关闭界面", this.dialog_name);
    }
    protected hideAnim(info: any) {
        let index = this.hide_index;
        if (index == DialogAction.Auto) index = this.play_index;
        if (index == 0) {
            this.afterHide(info);
            return;
        }
        let vise_size = view.getVisibleSize();
        let trans = this.node.getComponent(UITransform);
        let end = 0;
        if (this.action_tween) this.action_tween.stop();
        this.action_tween = tween(this.node);

        switch (index) {
            case DialogAction.Scale:
                this.action_tween.toPro(0.2, { scale: 0 })
                break;
            case DialogAction.R2L:
                end = vise_size.width / 2 + trans.width * trans.anchorX;
                this.action_tween.toPro(0.2, { x: end });
                break;
            case DialogAction.L2R:
                end = -vise_size.width / 2 - trans.width * (1 - trans.anchorX);
                this.action_tween.toPro(0.2, { x: end });
                break;
            case DialogAction.B2T:
                end = -vise_size.height / 2 - trans.height * (1 - trans.anchorY);
                this.action_tween.toPro(0.2, { y: end });
                break;
            case DialogAction.T2B:
                end = vise_size.height / 2 + trans.height * (1 - trans.anchorY);
                this.action_tween.toPro(0.2, { y: end });
                break;
        }
        this.action_tween.call(() => {
            this.action_tween = null;
            this.afterHide(info);
        })
        .start();
    }
}
