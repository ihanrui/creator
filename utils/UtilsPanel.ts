
import { _decorator, SpriteFrame, Node, Sprite, Button, Label, Toggle, EditBox, RichText, sp, isValid, Prefab, instantiate, assetManager, UITransform, UIOpacity, ImageAsset, Component, js, builtinResMgr, UIRenderer, Material } from 'cc';
import { UtilsFormat } from './UtilsFormat'
import { ResMgr } from '../manager/ResMgr'
import { i18nLabel } from '../components/i18nLabel';
import { i18nSkeleton } from '../components/i18nSkeleton';
import { Generate } from '../components/Generate';
import { BUILD } from 'cc/env';
import { own_info, User } from '../OwnInfo';
import { GameUserInfo } from '../gameframe/gameuserlist';
import { DecorationType, ShopBagData } from '../data/ShopBag';

export class UtilsPanel {

    /**通过 数据列表 生成 子物体集合 并 初始化
     * @param parent 承载节点
     * @param subPath 子节点 动态加载路径
     * @param dataList 数据列表
     * @param initFun 初始化方法 (subNode:Node, data: any , index)=> {...}
     */
    public static initChildByData(parent: Node, subPath = "", dataList: any[], initFun: Function) {
        if (!dataList || !parent) {
            console.error("initChildByData 初始化失败:", dataList, parent);
            return;
        }
        var count = parent.children.length;
        let signPref = null;
        //根据预制体创建
        let spawnFunc = () => {
            for (let index = 0; index < dataList.length; index++) {//当前子节点不足 则 动态生成选项
                let subNode: Node = null;
                if (index < count) {
                    subNode = parent.children[index];
                } else {
                    subNode = instantiate(signPref);
                    subNode.parent = parent;
                }
                subNode.active = true;
                initFun && initFun(subNode, dataList[index], index)
            }
            if (dataList.length < count) {//将多余的选项节点隐藏
                for (let index = dataList.length; index < count; index++) {
                    const node = parent.children[index];
                    node.active = false;
                }
            }
        }
        //获取预制体
        if (count > 0) {
            signPref = parent.children[0];
            spawnFunc();
        } else if (subPath) {
            ResMgr.loadWithTag("common", subPath, Prefab, (err: string, pref: Prefab) => {
                if (err) { console.log(err); return; }
                if (isValid(parent)) {
                    signPref = pref;
                    spawnFunc();
                }
            })
        } else {
            console.error("initChildByData 初始化失败:查找不到子预制体");
            return;
        }
    }


    public static showNodeByIndex(arr: Array<Node>, index: number) {
        if (index >= arr.length) return;
        arr.forEach((node, i) => {
            node.active = i == index;
        })
    }
    public static setLbNum(lb, num) {
        lb.string = "x" + UtilsFormat.formatMoney(num);
    }
    public static setPayType(type: number, sp_icon: Sprite, lb_curr: Label) {
        sp_icon.node.active = type != 1;
        lb_curr && (lb_curr.node.active = type == 1);
        if (type == 1) lb_curr.string = own_info.Currency;
        else {
            let sf_name = type == 0 ? "icon_diamond" : "icon_gold";
            sp_icon.spriteFrame = ResMgr.atlas_common.getSpriteFrame(sf_name);
        }
    }
    public static setItemIcon(flag: string, url: string, sp_icon: Sprite) {
        ResMgr.loadWithTag(flag, url, SpriteFrame, (err: string, sp: SpriteFrame) => {
            if (err) return;
            if (isValid(sp_icon)) {
                sp_icon.spriteFrame = sp;
            }
        })
    }
    public static limitNode(node: Node, width: number, height: number) {
        if (width == 0 || height == 0) return;
        let uitrans = node.getComponent(UITransform);
        let min: any = Math.min(width / uitrans.width, height / uitrans.height);
        node.toScaleXY(min);
    }
    public static limitHead(node: Node, width: number) {
        if (width == 0) return;
        let uitrans = node.getComponent(UITransform);
        node.toScaleX(width / uitrans.width);
        node.toScaleY(width / uitrans.height);
    }

    public static getAudioRoomImg() {
        return ResMgr.atlas_head.getSpriteFrame("audioRoomIMG");
    }

    public static getVipIcon(vip_level: number) {
        return ResMgr.atlas_head.getSpriteFrame("vip" + vip_level);
    }
    public static getCountryIcon(country: string) {
        return ResMgr.atlas_head.getSpriteFrame("country_" + country);
    }
    public static getHeadFrame(vip_level: number) {
        return ResMgr.atlas_head.getSpriteFrame("frame0");
    }
    //性别 1男 2女
    public static getSexIcon(sex: number) {
        return ResMgr.atlas_head.getSpriteFrame("icon_sex" + sex);
    }
    public static getHeadIcon(index: number) {
        return ResMgr.atlas_head.getSpriteFrame("head" + index);
    }
    public static getSpineNode() {
        let new_node = new Node("spine");
        let spine = new_node.addComponent(sp.Skeleton);
        spine.premultipliedAlpha = false;
        return spine;
    }

    public static setHead(face_id: number, face_url: string, sp_head: Sprite, width = 100) {
        if (face_id == -1) face_id = face_url.charCodeAt(face_url.length - 5) % 10;
        sp_head.spriteFrame = ResMgr.atlas_head.getSpriteFrame("head" + face_id);
        UtilsPanel.limitHead(sp_head.node, width);
        sp_head.customMaterial = null;

        if (face_url && face_url.length && BUILD) {
            assetManager.loadRemote(face_url, { maxRetryCount: 1, ext: '.png' }, function (err, tex: ImageAsset) {
                if (!isValid(sp_head)) return;
                if (err) {
                    console.log(err);
                    return;
                }
                sp_head.spriteFrame = SpriteFrame.createWithImage(tex);
                UtilsPanel.limitHead(sp_head.node, width);
                sp_head.customMaterial = ResMgr.mat_round;
            });
        }
    }

    public static setHead2(user_id: number, face_id: number, face_url: string, callback: (err: string, user_id: number, spriteFrame: SpriteFrame, material: Material) => void) {
        if (face_id == -1) face_id = face_url.charCodeAt(face_url.length - 5) % 10;
        if (face_url && face_url.length && BUILD) {
            assetManager.loadRemote(face_url, { xhrTimeout: 5, maxRetryCount: 1, ext: '.png' }, function (err, tex: ImageAsset) {
                if (err) {
                    console.log(err);
                    callback("", user_id, ResMgr.atlas_head.getSpriteFrame("head" + face_id), null);
                }
                else {
                    callback("", user_id, SpriteFrame.createWithImage(tex), ResMgr.mat_round);
                }
            });
        }
        else {
            callback("", user_id, ResMgr.atlas_head.getSpriteFrame("head" + face_id), null);
        }
    }

    public static setAudioRoomImg(_url: string, sp_img: Sprite, width = 100, height = 100) {

        //初始化 默认图片 圆角
        let _default = ResMgr.getSfByAtlas("audioRoom", `roomImgDefault`);
        if(!_default) {
            ResMgr.loadWithTag("audioRoom", "texture/audioRoom/common/roomImgDefault/spriteFrame", SpriteFrame,  (err:any, frame:SpriteFrame) => {
                if (err) { 
                    console.log(err); 
                    return; 
                }
                sp_img.spriteFrame = frame;
            })
        }
        else sp_img.spriteFrame = _default;
        
        UtilsPanel.limitNode(sp_img.node, width, height);
        sp_img.customMaterial = null;

        if (_url && _url.length && BUILD) {
            assetManager.loadRemote(_url, { maxRetryCount: 1, ext: '.jpg' }, function (err, tex: ImageAsset) {
                if (!isValid(sp_img)) return;
                if (err) {
                    console.log(err);
                    return;
                }
                //圆角裁剪
                sp_img.spriteFrame = SpriteFrame.createWithImage(tex);
                UtilsPanel.limitNode(sp_img.node, width, height);
                sp_img.setMaterial(ResMgr.mat_comer, 0);
            });
        }
    }

    public static getAllNeedCom(sc: any, node: Node, recursive: boolean = false) {
        SC = sc;
        let children = node.children;
        if (recursive) {
            for (let i = 0, len = children.length; i < len; i++) {
                children[i].walk(getComByName);
            }
        }
        else {
            for (let i = 0, len = children.length; i < len; i++) {
                getComByName(children[i]);
            }
        }
    }


    public static getCom(node: Node, path: string[], type = null): any {
        for (let i = 0, len = path.length; i < len; i++) {
            node = node.getChildByName(path[i]);
        }
        return type ? node.getComponent(type) : node;
    }

    public static getArrCom(node: Node, name: string, count: number, type = null) {
        let arr = new Array(count);
        let children = node.children;
        let cur_connt = 0;
        for (let i = 0; i < children.length; i++) {
            if (children[i].name.startsWith(name)) {
                cur_connt++;
                let index = parseInt(children[i].name.slice(name.length));
                arr[index] = type ? children[i].getComponent(type) : children[i];
                if (cur_connt >= count) break;
            }
        }
        return arr;
    }
    public static getChildMap(node: Node) {
        let map = new Map<string, Node>()
        let children = node.children;
        for (let i = 0; i < children.length; i++) {
            map.set(children[i].name, children[i]);
        }
        return map;
    }
    public static playSpine(node, name: string, callback = null, track = 0) {
        let com: sp.Skeleton = node.getComponent(sp.Skeleton)
        com.setAnimation(track, name, false)
        if (callback) {
            com.setCompleteListener((e: sp.spine.TrackEntry) => {
                if (e.trackIndex == track) {
                    com.setCompleteListener(null)
                    callback()
                }
            })
        }
    }
    public static autoHideSpine(spine: sp.Skeleton) {
        spine.setCompleteListener(function () {
            spine.node.active = false;
        })
    }
    public static addBtnEvent(btn_node: Node, func: Function, target?: any) {
        let btn = btn_node.getComponent(Button);
        if (!btn) {
            btn = btn_node.addComponent(Button);
            btn.transition = Button.Transition.SCALE;
            btn.target = btn_node;
            btn.zoomScale = 0.9;
            btn.duration = 0.1;
        }
        btn_node.on("click", func, target);
        return btn;
    }
    public static addBtnEvent2(btn_node: Node, func: string, target: any, param: string | number) {
        let btn = btn_node.getComponent(Button);
        if (!btn) {
            btn = btn_node.addComponent(Button);
            btn.transition = Button.Transition.SCALE;
            btn.target = btn_node;
            btn.zoomScale = 0.9;
            btn.duration = 0.1;
        }
        let handler = new Component.EventHandler();
        handler.target = target.node;
        handler.component = js.getClassName(target);
        handler.handler = func;
        //@ts-ignore
        handler.customEventData = param;
        btn.clickEvents.push(handler);
        return btn;
    }
    public static clearBtnEvent(node: Node, remove_btn: boolean) {
        node.off("click");
        let btn = node.getComponent(Button);
        if (btn) {
            btn.clickEvents.length = 0;
            if (remove_btn) btn.destroy();
        }
    }
    public static removeBtns(node:Node){
        node.walk(function(node1:Node){
            let btn = node1.getComponent(Button);
            if(btn)btn.destroy();
            node1.off("click");
        })
    }
    public static setNodeGray(node_or_btn: Node | Button, state: boolean, recursive = false) {
        let node = node_or_btn;
        let btn = null;
        if (node_or_btn instanceof Button) {
            btn = node_or_btn;
            node = node_or_btn.node;
        }
        else {
            btn = (node as Node).getComponent(Button);
        }
        if (btn) btn.interactable = state;
        // let mat_name = state ? "ui-sprite-material" : "";
        let mat = state ? null : builtinResMgr.get("ui-sprite-gray-material");

        let func = function (node0: any) {
            let btn = node0.getComponent(Button)
            btn && (btn.interactable = state);
            let com_rendener = node0.getComponent(UIRenderer);
            if (com_rendener && !com_rendener.setAnimation) com_rendener.customMaterial = mat;
        }
        if (recursive) {
            (node as Node).walk(func);
        }
        else {
            func(node);
        }
    }

    public static addToggleCheckEvent(toggleNode: Node, func: string, target: any, params: string) {
        let toggle = toggleNode.getComponent(Toggle);
        if (!toggle) {
            toggle = toggle.addComponent(Toggle);
            toggle.transition = Button.Transition.SCALE;
            toggle.target = toggleNode;
            toggle.zoomScale = 0.9;
            toggle.duration = 0.1;
        }

        let handler = new Component.EventHandler();
        handler.target = target.node;
        handler.component = js.getClassName(target);
        handler.handler = func;
        handler.customEventData = params;
        toggle.checkEvents.push(handler)
    }

    //检查游戏是否要加锁
    public static checkGameLock(node: Node, gameType: Number) {
        if ((gameType !== 1 && gameType !== undefined)
            || (gameType === 1 && own_info.PayAmount > 0)) {
            node.active = false;
        } else {
            node.active = true;
        }
    }
}

var SC = null;
function getComByName(node: Node) {
    let name = node.name;
    let com = null;
    switch (name[0]) {
        case "a":
            if (name.startsWith("auto_btn_")) {
                let click_name = name.slice(10);
                let func_name = "on" + name[9].toUpperCase() + click_name + "Click";
                if (SC[func_name]) UtilsPanel.addBtnEvent(node, SC[func_name], SC);
            }
            break;
        case "b":
            if (name.startsWith("btn_")) com = node.getComponent(Button);
            break;
        case "e":
            if (name.startsWith("edit_")) com = node.getComponent(EditBox);
            break;
        case "g":
            if (name.startsWith("gene_")) com = node.getComponent(Generate);
            break;
        case "i":
            if (name.startsWith("ilb_")) com = node.getComponent(i18nLabel);
            else if (name.startsWith("isk_")) com = node.getComponent(i18nSkeleton);
            break;
        case "l":
            if (name.startsWith("lb_")) com = node.getComponent(Label);
            break;
        case "n":
            if (name.startsWith("node_")) com = node;
            break;
        case "r":
            if (name.startsWith("rich_")) com = node.getComponent(RichText);
            break;
        case "s":
            if (name.startsWith("spine_")) com = node.getComponent(sp.Skeleton);
            else if (name.startsWith("sp_")) com = node.getComponent(Sprite);
            else if (name.startsWith("sc_")) {
                name = name.slice(3);
                //@ts-ignore
                com = node._components[0];//cocos creator 中组件排序 如果取不到留意顺序是否出错
            }
            break;
        case "t":
            if (name.startsWith("toggle_")) com = node.getComponent(Toggle);
            break;
        case "u":
            if (name.startsWith("uitrans_")) com = node.getComponent(UITransform);
            if (name.startsWith("uiopca_")) com = node.getComponent(UIOpacity);
            break;
    }
    if (com && SC.hasOwnProperty(name)) {
        if(SC[name])console.warn("重复调用?",name);
        SC[name] = com;
    }
}
