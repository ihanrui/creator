const fs = require("fs");
const xlsx = require("node-xlsx");

const TargetPath = "../assets/master/i18n/label/"
const data_path = "lobby/";

let all_data = new Array();
let names = null;
function parseOneExcel(path){
	console.log(`读取${path}中...`);
	let list = xlsx.parse(data_path + path);
	let tableData = list[0].data;
	let item_len = tableData[0].length;
	let prefix = path.slice(0, 2) + "_";

	if(!names){
		names = new Array();
		for (let i = 2; i < item_len; i++) {
			names.push(tableData[0][i]);//读取第一行，语言类型
		}
	}
	let len = tableData.length;
	for (let line = 2; line < len; line++) {
		let item = tableData[line]; //第i行
		if(item.length == 0)break;
		let arr = new Array(item_len);
		arr[0] = prefix;
		arr[1] = item[0];
		//遍历列
		for (let col = 2; col < item_len; col++) {
			arr[col] = item[col];
		}
		all_data.push(arr);
	}
}

function parseExcel() {
	let all_path = fs.readdirSync(data_path);
	console.log(all_path);
	for (let i = 0; i < all_path.length; i++) {
		parseOneExcel(all_path[i]);
	}
	console.log(names)
	for (let i = 0; i < names.length; i++) {
		if(names[i] != "ara")continue;
		let index = i + 2;
		let obj = {};
		let is_ara = names[i] == "ara";
		for(let j = 0 ; j < all_data.length ; j++){
			let one_data = all_data[j];
			if(is_ara)one_data[index] = " "+one_data[index]+" ";
			obj[one_data[0] + one_data[1]] = one_data[index];
		}
		let str= JSON.stringify(obj, null, 4);
		if(is_ara){
			str = str.replace(/\\\\n/g, "\\n");
		}
		writeFile(TargetPath + names[i] + ".json", str);
		
	}

}


function writeFile(fileName, data) {
	function complete(err) {
		if (!err) {
			console.log(fileName + " 生成成功");
		}
		else{
			console.log(fileName + " 生成失败");
		}
	}
	fs.writeFile(fileName, data, 'utf-8', complete);
}

parseExcel();