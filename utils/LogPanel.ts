import { _decorator, Label } from 'cc';
const {ccclass, property} = _decorator;

import { Dialog } from "./Dialog";

@ccclass('LogPanel')
export class LogPanel extends Dialog {

    public lb_log: Label | null = null;

    protected onLoad(){
        this.lb_log = this.node.getComponent(Label);
    }

    public init(data:any){
        if(typeof(data) == "object"){
            data = JSON.stringify(data);
        }
        this.lb_log.string = data;
    }
 
}
