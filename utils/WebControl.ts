import { _decorator, game, Game } from 'cc';
import { WebData, WebCallback } from "./WebData";
import { WebBase } from "./WebBase";
import { JsbCommon } from "../JsbCommon";
import { TSCommon } from "../TSCommon";
import { GEventType } from "../GEventType";
import { HallControl } from "../HallControl";
import { Domain } from '../Domain';


/*
    -登录-
    -游戏列表-
    -世界聊天-
    -获取游戏状态-
    -救济金-
*/

const temp_wd = new WebData();

export class WebControl extends WebBase {
    private static _instance: WebControl;

    public static getInstance(): WebControl {
        if (this._instance == null) {
            this._instance = new WebControl();
        }
        return this._instance;
    }

    // private m_request_index:number;
    private m_pingData: string;
    private m_sessionData: string;
    public isGameHide: boolean;
    public constructor() {
        super();
        this.m_pingData = "";
        this.m_sessionData = "";
        let self = this;
        game.on(Game.EVENT_HIDE, function () {
            self.isGameHide = true
            console.log("EVENT_HIDE");
        });
        game.on(Game.EVENT_SHOW, function () {
            if (self.isGameHide == false) {
                return;
            }
            self.isGameHide = false
            if (!self.isConnected()) {
                if (self.m_socketObject && self.m_onCloseConnect) {
                    self.m_onCloseConnect.call(self.m_socketObject);
                }
            }
            console.log("EVENT_SHOW");
        });
    }

    protected onReceiveMessage(data): void {
        var wd = JSON.parse(data);
        if (wd.Msg == "ping") {
            this.onPingReturn()
            return;
        }
        if (wd.Data == "null") {
            wd.Data = "";
        }

        // 如果被服务器踢了
        if (wd.Msg == "logout" || wd.Msg == "login_failed") {
            console.log("WebControl.kick", data)
            this.reconnect_state = 2;
            this.m_loginData = "";
            this.m_pingData = "";
            TSCommon.dispatchEvent(GEventType.LOGIN_ERR, wd);
            return;
        }


        if (wd.Msg == "loginByIMei" || wd.Msg == "loginByToken") {
            var loginRet = JSON.parse(wd.Data);
            if (loginRet.UserId > 0) {
                // 成功了
                var loginData = new WebData();
                loginData.Msg = "loginByToken";
                loginData.Data = JSON.stringify({ UserId: loginRet.UserId, Token: loginRet.Session })
                this.m_sessionData = loginData.Data;
                this.m_loginData = JSON.stringify(loginData);
                this.m_pingData = loginData.Data;
                this.sendCacheData();
                if (this.m_socketObject && this.m_onLogined) {
                    this.m_onLogined.call(this.m_socketObject, loginRet);
                }
            }
        }

        //console.log("onReceiveMessage msg = " + wd.Msg + ",Data = " + wd.Data);

        var callback = this.m_callbackPersist.get(wd.Msg);
        if (callback && callback.Target) {
            callback.Listener.call(callback.Target, wd.Data);
            return;
        }
        callback = this.m_callbackOnce.get(wd.Msg);
        if (callback != null) {
            this.m_callbackOnce.delete(wd.Msg);
            callback.Listener.call(callback.Target, wd.Data);
            return;
        }
    }

    public sendPing(): boolean {
        if (this.m_socket == null || this.m_socket.readyState != WebSocket.OPEN) {
            console.log("not connected", this.m_socket)
            return false;
        }
        if (this.m_pingData == "") {
            console.log("no m_pingData");
            return;
        }
        var wd: WebData = new WebData();
        wd.Msg = "ping"
        wd.Data = this.m_pingData;
        this.sendData(JSON.stringify(wd));
        return true;
    }

    public getSessionData() {
        return this.m_sessionData;
    }

    public getLoginData() {
        return this.m_loginData
    }

    // 退出登陆
    public logOut() {
        this.m_loginData = "";
        this.m_pingData = "";
        this.closeSocket();
        // WebControl._instance = null;//避免切换场景导致显示异常
    }

    public loginByIMei(imei: string, nickName: string, faceUrl: string, accessToken: string, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "loginByIMei";
        wd.Data = JSON.stringify({
            IMei: imei,
            NickName: nickName,
            FaceUrl: faceUrl,
            AccessToken: accessToken,
            PartnerID: parseInt(JsbCommon.getPartnerID()),
            Version: Domain.getVersion(),
            Deviceid: JsbCommon.getImei(),
            Referrer: JsbCommon.getReferrer(),
            DeviceName: JsbCommon.getDeviceName(),
            MessageToken: JsbCommon.getRemoteMessageToken(),
        });
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 游客登录
    public guestLogin(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "loginByIMei";

        let DeviceName = JsbCommon.getDeviceName();
        let NickName = DeviceName;
        if (!NickName) {
            NickName = 'guest' + Math.floor(Math.random() * 10000)
        }
        var imei = JsbCommon.getImei()
        var data = {
            IMei: imei,
            Version: Domain.getVersion(),
            PartnerID: parseInt(JsbCommon.getPartnerID()),
            NickName: NickName,
            Deviceid: imei,
            Referrer: JsbCommon.getReferrer(),
            DeviceName: DeviceName,
            MessageToken: JsbCommon.getRemoteMessageToken(),
        };
        wd.Data = JSON.stringify(data);
        this.sendData(JSON.stringify(wd));
        wd.Data = JSON.stringify(data);
        this.addCallback(wd.Msg, obj, listener);
        console.log(wd);
    }

    //-登录-
    public loginByToken(userID, token, obj: any, listener: Function) {
        userID = parseInt(userID);
        var wd: WebData = new WebData();
        wd.Msg = "loginByToken";
        var msgData = { UserID: userID, Token: token };
        wd.Data = JSON.stringify(msgData);
        this.m_sessionData = wd.Data;
        console.log("loginByToken", wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    public sendEmptyCmd(cmd: string, obj: any, listener: Function) {
        temp_wd.Msg = cmd;
        console.log("%c" + cmd, "color:green");
        this.sendData(JSON.stringify(temp_wd));
        this.addCallback(cmd, obj, listener);
    }
    /*签到相关*/
    public doUserSignIn(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "doUserSignIn";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    //--月卡
    public monthlyCardSysInfo(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "monthlyCardSysInfo";
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    public monthlyCardBuy(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "monthlyCardBuy";
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh getSysTaskList 系统任务列表 
    * @param {any} obj 对象
    * @param {Function} listener 返回方法
    */
    public monthlyCardGift(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "monthlyCardGift";
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /** !#zh getSysTaskList 系统任务列表 
    * @param {any} obj 对象
    * @param {Function} listener 返回方法
        TaskScene_All       = iota // 0=游戏和大厅都显示
        TaskScene_Hall             // 1=大厅
        TaskScene_Game             // 2=游戏
        TaskScene_Reserved         // 3=系统任务，不显示
        TaskScene_Agent            // 4=代理会员任务
        TaskScene_AudioRoom        // 5=语聊房任务
    */
    public getSysTaskList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getSysTaskList";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh getSysTask 系统任务信息(单个任务)
    * @param {any} taskId 任务ID
    * @param {any} obj 对象
    * @param {Function} listener 返回方法
    */
    public getSysTask(taskId, obj: any, listener: Function) {
        taskId = parseInt(taskId);
        var wd: WebData = new WebData();
        wd.Msg = "getSysTask";
        var msgData = { TaskId: taskId };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh getUserTaskList 用户任务列表
    * @param {any} obj 对象
    * @param {Function} listener 返回方法
    */
    public getUserTaskList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getUserTaskList";
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh awardTask 领取任务
    * @param {any} taskId 任务ID
    * @param {any} obj 对象
    * @param {Function} listener 返回方法
    */
    public awardTask(taskId, obj: any, listener: Function) {
        taskId = parseInt(taskId);
        var wd: WebData = new WebData();
        wd.Msg = "awardTask";
        var msgData = { TaskId: taskId };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh awardAllTask 领取所有已完成任务  
    * @param {any} obj 对象
    * @param {Function} listener 返回方法
      返回，[]item.ItemPack  所有奖品列表
    */
    public awardAllTask(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "awardAllTask";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    //-游戏列表-
    public getGameList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getGameList";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh getRankList 排行榜
    * @param {any} RankType 排行榜类型
    * @ 1 财富榜
    * @ 2 每日赢金榜
    * @ 3 每周赢金榜
    * @ 4 每月赢金榜
    * @ 5 徽章成就排行榜
    * @ 6 语音房本周榜
    * @ 7 语音房下周榜
    * 20 好友房赢局数日榜     
    * 21 好友房赢局数周榜
    */
    public getRankList(RankType: number, obj: any, listener: Function, getCount: number = 50) {
        if (getCount <= 0) return;
        var wd: WebData = new WebData();
        wd.Msg = "rankList" + + new Date().getTime();
        wd.Data = JSON.stringify({
            RankType: RankType,
            Num: getCount
        });
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    public getRankAwardList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getRankAwardList";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    //获取游戏房间列表
    public getGameRooms(gameEnglishName: string, obj: any, listener: Function): void {
        var wd: WebData = new WebData();
        wd.Msg = "getGameRooms";
        wd.Data = gameEnglishName;
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh sendUserMail 用户发送邮件至系统
    * @param {any} title 主题
    * @param {any} content 内容
    * @param {any} obj 对象
    * @param {Function} listener 返回方法
    */
    public sendUserMail(title, content, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "sendUserMail";
        var msgData = { Title: title, Content: content };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh bankIn 用户发送邮件至系统
    * @param {any} Amount 主题
    * @param {any} GameID 内容
    * @param {any} ServerName 内容
    * @param {any} obj 对象
    * @param {Function} listener 返回方法
    */
    public bankIn(Amount, GameID, ServerName, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "bankIn";
        var msgData = { Amount: Amount, GameID: GameID, ServerName: ServerName };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh bankIn 用户发送邮件至系统
    * @param {any} Amount 主题
    * @param {any} GameID 内容
    * @param {any} ServerName 内容
    * @param {any} obj 对象
    * @param {Function} listener 返回方法
    */
    public bankOut(Amount, GameID, ServerName, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "bankOut";
        var msgData = { Amount: Amount, GameID: GameID, ServerName: ServerName };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh getUserMails 获取用户邮件列表
    * @param {any} mailId 邮件Id 0则为获取全部邮件 初次获取时传递0之后根据最大ID获取
    * @param {any} obj 对象
    * @param {Function} listener 返回方法
    */
    public getUserMails(mailId, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getUserMails";
        var msgData = { MailId: mailId };
        wd.Data = JSON.stringify(msgData);
        console.log("getUserMails:", wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh getSysMails 获取系统邮件列表
    * @param {any} obj 对象
    * @param {Function} listener 返回方法 Status：0未阅读 1已阅读
    */
    public getSysMails(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getSysMails";
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh updateSysMail 更新系统邮件状态
    * @param {any} sysMsgId 系统邮件ID 
    * @param {any} status 状态(0=未读 1=已读  2=领取)
    * @param {any} obj 对象
    * @param {Function} listener 返回方法
    */
    public updateSysMail(sysMsgId, status, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "updateSysMail";
        var msgData = { SysMsgId: sysMsgId, Status: status };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh updateAllSysMail 领取所有邮件
   */
    public updateAllSysMail(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "updateAllSysMail";
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh delSysMail 删除系统邮件
    * @param {any} sysMsgId 系统邮件ID 
    * @param {any} obj 对象
    * @param {Function} listener 返回方法
    */
    public delSysMail(sysMsgId, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "delSysMail";
        var msgData = { SysMsgId: sysMsgId };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 修改性别  性别 0=默认(无)  1=男   2=女
    public changeSex(Sex: number, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "changeSex";
        wd.Data = JSON.stringify({
            Sex: Sex,
        });
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    // 修改昵称费用
    public changeNameInfo(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "changeNameInfo";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 修改昵称
    public changeNickName(NickName: string, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "changeNickName";
        wd.Data = JSON.stringify({
            NewNickName: NickName,
        });
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    // 修改头像
    public changeFace(FaceID: number, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "changeFace";
        wd.Data = JSON.stringify({
            FaceID: FaceID,
        });
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    // 修改签名
    public changeUserWord(UserWords: string, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "changeUserWord";
        wd.Data = JSON.stringify({
            UserWords: UserWords,
        });
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    /** !#zh saveCountry 保存国家地区
     * @param {number} countryName 国家名称
     * @param {string} currency  币种
     * 返回：
         Data    string  // 1成功
     */
    public saveCountry(countryName: string, currency: string, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "saveCountry";
        var msgData = { CountryName: countryName, Currency: currency };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /** !#zh friendApply 好友申请
    * @param {any} targetUserId 好友用户Id 
    */
    public friendApply(targetUserId: number, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "friendApply";
        wd.Data = JSON.stringify({
            TargetUserID: targetUserId,
        });
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /** !#zh friendApply 好友数量
    * @param {any} targetUserId 好友用户Id 
    */
    public getMaxFriendCount(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getMaxFriendCount";
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /** !#zh reportUser 举报 or 反馈
    * @param {any} userId 邮箱or反馈人用户id
    * @param {any} toUserId 被举报用户ID
    * @param {any} reason 举报内容 or 反馈内容
    */
    public reportUser(userId: string, toUserId: string, reason: string, obj: any, listener: Function) {
        //RetCode: 1 = 提交成功
        var wd: WebData = new WebData();
        wd.Msg = "feedback";
        wd.Data = JSON.stringify({
            Email: userId,
            ToUserId: toUserId,
            Msg: reason
        });
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    //-世界聊天-
    /*
        MessageType_Normal = iota // 普通消息
        MessageType_Gift          // 1礼物消息
        MessageType_Room          // 2房间消息{game:baloot,roomid:54565}
    */
    public sendWorldMsg(msg: string, msgType, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "sendChatMsg";
        wd.Data = JSON.stringify({
            ChannelID: 0,
            RecvUserID: 0,
            Msg: msg,
            MsgType: msgType,
        });
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    public getWorldMsgHistory(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getChatMsg";
        wd.Data = JSON.stringify({
            ChannelID: 0,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    // 发送大喇叭
    public sendSpeaker(msg: string, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "sendChatMsg";
        wd.Data = JSON.stringify({
            ChannelID: 1,
            RecvUserID: 0,
            Msg: msg
        });
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    public getSpeakerHistory(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getChatMsg";
        wd.Data = JSON.stringify({
            ChannelID: 1,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh friendApplyList 好友申请列表
    */
    public friendApplyList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "friendApplyList";
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh friendHandleApply 好友申请处理
    * @param {any} targetUserId 好友用户Id 
    * @param {any} apply 审批标识  1：同意 2：不同意；
    */
    public friendHandleApply(targetUserId, apply, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "friendHandleApply";
        var msgData = { TargetUserID: targetUserId, Apply: apply };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh getfriendList 好友列表
    */
    public getfriendList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getfriendList";
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh delfriend 删除好友
    * @param {any} targetUserId 好友用户Id 
    */
    public delfriend(targetUserId, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "delfriend";
        var msgData = { TargetUserID: targetUserId };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh friendGiveGift 赠送礼物
    * @param {any} targetUserId 好友用户Id 
    */
    public friendGiveGift(targetUserId, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "friendGiveGift";
        var msgData = { TargetUserID: targetUserId };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh friendGetGift 领取礼物
    * @param {any} targetUserId 好友用户Id 
    */
    public friendGetGift(targetUserId, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "friendGetGift";
        var msgData = { TargetUserID: targetUserId };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh getUserInfo 获取用户信息
   * @param {any} userId 用户Id 
   */
    public getUserInfo(userId, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getUserInfo";
        var msgData = { UserId: userId };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh searchUser 搜索好友
    * @param {any} targetUserId 好友用户Id 
    * IsFriend：0=不是好友  1=好友  2=已申请(自己申请的，待对方审核)  3=待审核（对方申请的，待自己审核）
    */
    public searchUser(targetUserId, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "searchUser";
        var msgData = { TargetUserID: targetUserId };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh spreadApply 会员申请
    * @param {any} code 邀请码 
    */
    public spreadApply(code, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "spreadApply";
        var msgData = { Code: code };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh spreadMembers 会员邀请记录
    * @param {any} pageIndex 第几页 
    * @param {any} pageSize 每页页数 默认50
    */
    public spreadMembers(pageIndex, pageSize = 50, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "spreadMembers";
        var msgData = { PageIndex: pageIndex, PageSize: pageSize };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh spreadGift 领取会员邀请奖励
    * @param {any} fromUserId 用户Id 
    */
    public spreadGift(fromUserId, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "spreadGift";
        var msgData = { FromUserID: fromUserId };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /** !#zh getUserVip 获取我的VIP信息
    */
    public getUserVip(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getUserVip";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /** !#zh achievementList 获取成就奖杯列表(未设置为空)
    */
    public achievementList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "achievementList";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh achievementUpdate 设置成就奖杯
    * @param {any} taskId 任务Id(奖杯ID)
    * @param {any} status 设置状态 Status 1=设置  2=删除 
    */
    public achievementUpdate(taskId, status, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "achievementUpdate";
        var msgData = { TaskID: taskId, Status: status };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    //使用物品
    public consumeItem(item_id: number, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "consumeItem";
        wd.Data = JSON.stringify({
            ItemId: item_id,
        });
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 获取我的礼品卡兑换记录
    // cardType:礼品卡类型，如果不传则全部
    /* 
        返回:[GiftCard]
        type GiftCard struct {
            Type         int    // 类型
            Key          string // 兑换码
            UserId       int    `json:",omitempty"` // 谁兑换了,0表示未被兑换
            ActivateTime int64  `json:",omitempty"` // 兑换时间，0表示未被兑换
        }

    */
    public getGiftCardHistory(cardType, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getGiftCardHistory";
        wd.Data = cardType + ""
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 游戏记录
    public getGameCount(UserID, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getGameCount";
        var msgData = { UserID: UserID };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    //-获取游戏状态-
    public getMyGameStatus(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getMyGameStatus";
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /** !#zh trackRecord 足迹记录
    * @param {any} level_1 1级类目 例如 大厅 弹窗 传递对应名称
    * @param {any} level_2 2级类目(可以为空) 例如 弹窗子页面
    * @param {any} level_3 3级类目(可以为空) 例如 弹窗内的按钮
    */
    public trackRecord(level_1: string, level_2: string, level_3) {
        var wd: WebData = new WebData();
        wd.Msg = "trackRecord";
        var msgData = { Level_1: level_1, Level_2: level_2, Level_3: level_3 };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
    }
    /** !#zh  reviewInfo  获取评论信息
    */
    public reviewInfo(obj: any, appName: string, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "reviewInfo";
        var msgData = { AppName: appName };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh  reviewGift  领取评论奖励
    */
    public reviewGift(obj: any, appName: string, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "reviewGift";
        var msgData = { AppName: appName };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh gameHistory 游戏历史
    * @param {any} pageIndex 第几页 
    * @param {any} pageSize 每页页数 默认50
    */
    public gameHistory(pageIndex, pageSize = 50, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "gameHistory";
        var msgData = { PageIndex: pageIndex, PageSize: pageSize };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    public bindFacebook(openId: string, nickName: string, faceUrl: string, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "bindFacebook";
        var msgData = { OpenId: openId, NickName: nickName, FaceUrl: faceUrl };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    //发送私聊
    public sendChannelChat(recvUserID: number, msg: string, msgType, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "sendChannelChat";
        wd.Data = JSON.stringify({
            RecvUserID: recvUserID,
            Msg: msg,
            MsgType: msgType,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
        console.log(wd)
    }

    //接收私聊
    public getChannelChat(channelKey: string) {
        var wd: WebData = new WebData();
        wd.Msg = "getChannelChat";
        wd.Data = channelKey;
        this.sendData(JSON.stringify(wd));
        console.log(wd)
        //回调已经在 HallControl.registerWebCallback()主动监听
    }

    //获取私聊用户信息
    public getChannelInfo(channelKey, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "getChannelInfo";
        wd.Data = channelKey

        console.log('getChannelInfo', wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    // 清理聊天消息
    public clearChannelHistory(channelKey, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "clearChannelHistory";
        wd.Data = channelKey

        console.log('clearChannelHistory', wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    //获取银行信息
    public getBankInfo(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getBankInfo";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    //保存银行信息
    public saveBankInfo(realName: string, bankName: string, bankCode: string,
        bankCard: string, mobile: string, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "saveBankInfo";
        wd.Data = JSON.stringify({
            RealName: realName,
            BankName: bankName,
            BankCode: bankCode,
            BankCard: bankCard,
            Mobile: mobile
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**玩家获胜返回大厅展示收益
     */
    public getUserWinScore(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getUserWinScore";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        私人场协议
    */

    /**获取私人场信息
   * @param obj 
   * @param listener 
   * @return 返回 
       {
           RoomNo     int
           ServerAddr string
           TableId    int
           GameId     int
           Prize      int
           Owner      int
           GameName   string
           createTime int64
           RuleName   string
           RuleData   string
           UserCount  int // 限制人数
           Fee        int
           Status     int
           UserList   [{
               UserId   int
               NickName string
               ChairId  int
               Score    int
               }
           ]
       }        
   */
    public getPrivateRoomInfo(roomNo: number | string, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getPrivateRoomInfo";
        wd.Data = roomNo + ""
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
        console.log(wd)
    }


    // 输入 GameId 输出：[{Name string,Data string}] 规则名称和规则内容数组
    public getPrivateRoomRules(gameId: number, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getPrivateRoomRules";
        wd.Data = "" + gameId;
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
        console.log(wd)
    }
    /*
        createPrivateRoom 输入：{
            GameId    int    // 游戏ID
            GameRule  string // 规则名
            Target    int    // 结束目标，hand为局数，domino为胜利分数
            UserCount int    // 游戏人数
            Fee       int    // 报名费
            Prize     int    // 奖金, 奖金改成报名费，这里传0
            PlayTime  int    // 出牌时间
            IsPublic  bool   // 是否公共房间
        }
        输出：{RoomNo int,ErrMsg string}，如果RoomNo == 0 表示创建失败
    */
    public createPrivateRoom(data: any, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "createPrivateRoom";
        wd.Data = JSON.stringify(data);
        this.sendData(JSON.stringify(wd));
        console.log(wd)
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        获取房间列表
        gameId传0表示获取所有
    */
    public getPrivateRoomList(gameId: number, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getPrivateRoomsByGameId";
        wd.Data = gameId + "";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        订阅私人场变化消息
        在onNewStatus中获得通知
        Notification_PrivateRoom            // 31=私人场消息
        这个订阅有60秒时效，需要每隔30秒调用一次
    */
    public subscribePrivateRoomStatus() {
        var wd: WebData = new WebData();
        wd.Msg = "subscribePrivateRoomStatus";
        wd.Data = "";
        this.sendData(JSON.stringify(wd));
    }
    // 取消订阅
    // 退出界面后取消订阅
    public desubscribePrivateRoomStatus() {
        var wd: WebData = new WebData();
        wd.Msg = "desubscribePrivateRoomStatus";
        wd.Data = "";
        this.sendData(JSON.stringify(wd));
    }

    // 发起邀请
    public privateRoomCallForUser(roomNo) {
        var wd: WebData = new WebData();
        wd.Msg = "PrivateRoomCallForUser";
        wd.Data = roomNo + "";
        this.sendData(JSON.stringify(wd));
    }
    // 获取邀请列表
    public getPrivateRoomCallList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getPrivateRoomCallList";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        加入私人场
        输入增加chairId，如果不填则随机分配
        输出: {
            ErrMsg     string
            ServerAddr string
            TableId    int
            ChairId    int
            BlackUsers []int // 桌子内有黑名单玩家
        }
    */
    public enterPrivateRoom(roomNo: number, chairId: number, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "enterPrivateRoom";
        wd.Data = roomNo + "";
        if (chairId > -1) {
            wd.Data = wd.Data + "," + chairId
        }
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    /*
        取消加入私人场
    */
    public leavePrivateRoom(roomNo: number) {
        var wd: WebData = new WebData();
        wd.Msg = "leavePrivateRoom";
        wd.Data = roomNo + "";      
        this.sendData(JSON.stringify(wd));   
        console.log(wd)   
    }

    /*
        获取历史房间记录
        包括我创建的房间和我参与的房间记录
        目前没考虑分页
    */
    public getPrivateRoomHistory(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getPrivateRoomHistory";
        this.sendData(JSON.stringify(wd));
        console.log(wd)
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        获取我当前是否在私人场中
        返回房间号字符串，如果为空表示没有
    */
    public getPlayingPrivateRoom(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getPlayingPrivateRoom";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh getExchangeRateList 获取币种汇率列表
    * 返回：
        Currency    string  // 币种
        CountryName string  // 国家名称
        CountryCode string  // 国家编码
        Rate        float64 // 汇率
    */


    /** !#zh getShopListByCurrency 商城列表
    * @param {number} shopType 产品类型(1=金币  2=钻石  3=炮台  4=普通礼包 5=充值礼包 6=成长礼包 7=月卡 8=vip特惠)
    * @param {string} currency  币种
    * 返回：
        Currency    string  // 币种
        CountryName string  // 国家名称
        CountryCode string  // 国家编码
        Rate        float64 // 汇率
    */

    public getShopList(shopType: number, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getShopList";
        var msgData = { ShopType: shopType };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /* 
        获取汇率价格
        输入: price 10  返回："40SAR"       
    */
    public getLocalPrice(price: number, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getLocalPrice";
        var msgData = { Price: price };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /*SimpleMatch*/
    /* 
        注册赛事事件通知,内容如下:
        "userenter"      用户进入
        "userexit"       用户退出
        "statuschanged"  状态变化
        "scorechanged"   玩家分数变化通知
        "promoted"       晋级通知
        "eliminated"     淘汰通知
        "rank"           比赛结束后名次通知
        "matchroom"      分配的私人场信息
    */
    public registerSimpleMatchNotification(obj: any, listener: Function) {
        this.registerCallback("onSimpleMatchNotification", obj, listener)
    }


    /*
        获取比赛信息
    */
    public getSimpleMatchInfo(matchNo, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getSimpleMatchInfo";
        wd.Data = matchNo + ""
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
       createSimpleMatch 输入：{
           GameId    int    // 游戏ID
           GameRule  string // 规则名
           Target    int    // 结束目标，hand为局数，domino为胜利分数
           TotalUser int    // 比赛总报名人数
           TableUser int    // 每桌人数
           Fee       int    // 报名费
           Prize     int    // 奖金
           PlayTime  int    // 出牌时间
       }
       输出：{MatchNo int,ErrMsg string}，如果MatchNo == 0 表示创建失败
   */
    public createSimpleMatch(gameId, gameRule, target, totalUser, tableUser, fee, prize, obj: any, listener: Function, playTime) {
        var wd: WebData = new WebData();
        wd.Msg = "createSimpleMatch";
        var msgData = {
            GameID: gameId, GameRule: gameRule, Target: target,
            TotalUser: totalUser, TableUser: tableUser, Fee: fee, Prize: prize, PlayTime: playTime
        };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        console.log(wd)
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        closeSimpleMatch 关闭比赛
    */
    public closeSimpleMatch(matchNo, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "closeSimpleMatch";
        wd.Data = matchNo + ""
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /*getUserSngMatchId 获取我当前所处的挑战赛ID*/

    public getUserSngMatchId(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getUserSngMatchId";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        enrollSimpleMatch 加入比赛
    */
    public enrollSimpleMatch(matchNo, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "enrollSimpleMatch";
        wd.Data = matchNo + ""
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        quitSimpleMatch 退出比赛
    */
    public quitSimpleMatch(matchNo, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "quitSimpleMatch";
        wd.Data = matchNo + ""
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        getMySimpleMatches 我创建的比赛列表
    */
    public getMySimpleMatches(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getMySimpleMatches";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        getSimpleMatchConfigs 获取比赛配置列表
    */
    public getSimpleMatchConfigs(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getSimpleMatchConfigs";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        getEnrolledSimpleMatch  获取我当前参加的房间列表
    */

    public getEnrolledSimpleMatch(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getEnrolledSimpleMatch";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    public getSimpleMatchHistory(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getSimpleMatchHistory";
        this.sendData(JSON.stringify(wd));
        console.log(wd)
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
       挑战赛 SngMatch
       挑战赛为重复自建赛，玩家报名满后即开
   */

    /*getSngMatchList 获取比赛列表*/

    public getSngMatchList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getSngMatchList";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*getSngMatchHistory 获取比赛历史记录*/

    public getSngMatchHistory(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getSngMatchHistory";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        enrollSngMatch 加入比赛
    */
    public enrollSngMatch(matchId, feeIndex, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "enrollSngMatch";
        var msgData = { MatchId: matchId, FeeIndex: feeIndex };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        quitSimpleMatch 退出比赛
    */
    public quitSngMatch(matchId, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "quitSngMatch";
        wd.Data = matchId + ""
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    public giftItems(ToUserId: number, ItemId: number, Count: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "giftItems";
        var msgData = { ToUserId: ToUserId, ItemId: ItemId, Count: Count };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    public sellItem(ItemId: number, Count: number, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "sellItem";
        var msgData = { ItemId: ItemId, Count: Count };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh friendRoomInvite 好友房邀请好友
    * @param {any} targetUserId 用户Id 
    * @param {any} roomNo 房间Id 
    */
    public friendRoomInvite(targetUserId, roomNo, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "friendRoomInvite";
        var msgData = { ToUserId: targetUserId, RoomNo: roomNo };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh friendGetRoomInviteList 私人场邀请历史
    *
    */
    public friendGetRoomInviteList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "friendGetRoomInviteList";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh friendGetRoomInviteList 私人场房间邀请设置失效
   *
   */
    public friendRoomInviteInvalid(roomNo, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "friendRoomInviteInvalid";
        var msgData = { RoomNo: roomNo };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 获取潜在好友列表
    /*
        type PotentialFriend struct {
            UserId  int
            MetTime int
            Memo    string            
            NickName string
            FaceId   int
            FaceUrl  string
        }
    */
    public getPotentialFriendList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getPotentialFriendList";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*minislot*/
    public getMinislotConfig(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "minislot_config";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    public doMinislotBet(betAmount: number, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "minislot_bet";
        wd.Data = JSON.stringify({
            Amount: betAmount,
        });
        this.sendData(JSON.stringify(wd));
        console.log(wd)
        this.addCallback(wd.Msg, obj, listener);
    }

    /*point_match*/
    /*
        获取比赛信息
    */
    public getPointMatchInfo(matchNo, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getPointMatchInfo";
        wd.Data = matchNo + ""
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
       createPointMatch 输入：{
                GameId         int    // 游戏ID
        GameRule       string // 规则名
        TotalUser      int    // 比赛总报名人数
        TableUser      int    // 每桌人数
        Fee            int    // 报名费
        Prize          int    // 奖金
        PlayTime       int    // 出牌时间
        EliminateScore int    // 初始淘汰分数
        ShrinkSec      int    // 缩圈时间
        ShrinkScore    int    // 缩圈分数
        WinnerCount    int    // 最终胜利者数量
       }
       输出：{MatchNo int,ErrMsg string}，如果MatchNo == 0 表示创建失败
   */
    public createPointMatch(gameId, gameRule, totalUser, tableUser, fee, prize, eliminateScore, shrinkSec, shrinkScore, winnerCount, obj: any, listener: Function, playTime) {
        var wd: WebData = new WebData();
        wd.Msg = "createPointMatch";
        var msgData = {
            GameID: gameId, GameRule: gameRule,
            TotalUser: totalUser, TableUser: tableUser, Fee: fee, Prize: prize, PlayTime: playTime,
            EliminateScore: eliminateScore,
            ShrinkSec: shrinkSec,
            ShrinkScore: shrinkScore,
            WinnerCount: winnerCount
        };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        console.log(wd)
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        closePointMatch 关闭比赛
    */
    public closePointMatch(matchNo, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "closePointMatch";
        wd.Data = matchNo + ""
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        enrollPointMatch 加入比赛
    */
    public enrollPointMatch(matchNo, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "enrollPointMatch";
        wd.Data = matchNo + ""
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        quitPointMatch 退出比赛
    */
    public quitPointMatch(matchNo, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "quitPointMatch";
        wd.Data = matchNo + ""
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        getMyPointMatches 我创建的比赛列表
    */
    public getMyPointMatches(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getMyPointMatches";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        getPointMatchConfigs 获取比赛配置列表
    */
    public getPointMatchConfigs(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getPointMatchConfigs";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        getEnrolledPointMatch  获取我当前参加的房间列表
    */

    public getEnrolledPointMatch(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getEnrolledPointMatch";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    public getPointMatchHistory(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getPointMatchHistory";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /* 局数配置赛 */

    public getSetsMatchInfo(matchNo, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getSetsMatchInfo";
        wd.Data = matchNo + ""
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        quitSetsMatch 退出比赛
    */
    public quitSetsMatch(matchNo, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "quitSetsMatch";
        wd.Data = matchNo + ""
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        getMySetsMatches 我创建的比赛列表
    */
    public getMySetsMatches(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getMySetsMatches";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        getSetsMatchConfigs 获取比赛配置列表
    */
    public getSetsMatchConfigs(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getSetsMatchConfigs";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        getEnrolledSetsMatch  获取我当前参加的房间列表
    */

    public getEnrolledSetsMatch(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getEnrolledSetsMatch";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    public getSetsMatchHistory(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getSetsMatchHistory";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /*
        组合赛 ComboMatch
    */

    /*getSngMatchList 获取比赛列表*/

    public getComboMatchList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getComboMatchList";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*getSngMatchHistory 获取比赛历史记录*/

    public getComboMatchHistory(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getComboMatchHistory";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*getUserSngMatchId 获取我当前所处的挑战赛ID*/

    public getUserComboMatchId(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getUserComboMatchId";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    public getComboMatchInfo(MatchId: string, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getComboMatchInfo";
        wd.Data = MatchId + '';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        enrollSngMatch 加入比赛
    */
    public enrollComboMatch(matchId, feeIndex, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "enrollComboMatch";
        var msgData = { MatchId: matchId, FeeIndex: feeIndex };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        quitSimpleMatch 退出比赛
    */
    public quitComboMatch(matchId, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "quitComboMatch";
        wd.Data = matchId + ""
        this.sendData(JSON.stringify(wd));
        console.log(wd)
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
       combomatch预报名 确认参加比赛
    */
    public confirmComboMatch(matchNo, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "confirmComboMatch";
        var msgData = matchNo + ""
        wd.Data = msgData;
        this.sendData(JSON.stringify(wd));
        console.log(wd)
        this.addCallback(wd.Msg, obj, listener);
    }
    /*
       combomatch预报名 获取确认人数
    */
    public getComboMatchConfirmCount(matchNo, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getComboMatchConfirmCount";
        var msgData = matchNo + ""
        wd.Data = msgData;

        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /**语音房配置信息
    // 房间配置
    type RoomConfig struct {
        SysNotification string   // 系统公告
        Tag             []string // 标签
        Language        []string // 语言
        MicCount        []int    // 上麦数量
        JoinFee         Fee      // 入场会费
    }

    // 入场会费
    type Fee struct {
        Min int // 最低费用
        Max int // 最高费用
        Tax int // 税费(百分比)
    }
     */
    getAudioRoomConfig(obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoomConfig";
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 登录语聊房服务
     *   
     * @param obj 
     * @param listener 
     * @return  表示登录服务器成功 
     */
    loginAudioRoomServer(obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "loginAudioRoomServer";
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 退出语聊房服务
     *   
     * @param obj 
     * @param listener 
     * @return 返回 > 0 表示退出服务器成功 
     */
    logoutAudioRoomServer(obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "logoutAudioRoomServer";
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**创建房间
     * @returns
        RoomId      int    // 房间ID
        RoomName    string // 房间名称
        RoomImg     string // 房间图片
        UserId      int    // 用户ID
        Family      string // 家族
        Country     string // 国家
        Language    string // 语言
        MicCount    int    // 上麦数量
        JoinFee     int    // 入会费
        Announce    string // 公告内容
        Tag         string // 标签(游戏、闲聊、交友、家族、仅限女孩)
        MemberCount int    // 会员数
        Crdate      string // 创建时间
        OnlineCount int    // 在线用户数    
     */
    createAudioRoom(roomImg: string, roomName: string, tag: string, announce: string, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "createAudioRoom";
        console.log(wd)
        var msgData = {
            RoomName: roomName,
            RoomImg: roomImg,
            Announce: announce,
            Tag: tag,
        };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 获取预览房间封面
     * 
     * @param roomId 
     * @param obj 
     * @param listener 
     */
    getAudioRoomGetRoomImg(roomId: string, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "audioRoomGetRoomImg";
        console.log(wd)
        var msgData = {
            RoomId: roomId,
        };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**修改房间信息
    * 
    * @param RoomId  房间ID
    * @param RoomName  房间名称
    * @param RoomImg  房间图片
    * @param Family  家族
    * @param Country  国家
    * @param Language  语言
    * @param MicCount  上麦数量
    * @param JoinFee  入会费
    * @param Announce  公告内容
    * @param Tag  标签(游戏、闲聊、交友、家族、仅限女孩)
    * @param MicMode  
    * 1 表示修改成功
    */
    updateAudioRoom(RoomId, RoomName, RoomImg, Family, Country,
        Language, MicCount, JoinFee, Announce, Tag, MicMode, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "updateAudioRoom";
        var msgData = {
            RoomId: RoomId,
            RoomName: RoomName,
            RoomImg: RoomImg,
            Family: Family,
            Country: Country,
            Language: Language,
            MicCount: MicCount,
            JoinFee: JoinFee,
            Announce: Announce,
            Tag: Tag,
            MicMode: MicMode,
        };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**进入语聊房间
    * 
    * @param roomid 
    * @param obj 
    * @param listener 
    * 
        // 语音房间信息
    type RoomInfo struct {
        RoomId      int    // 房间ID
        RoomName    string // 房间名称
        RoomImg     string // 房间图片
        UserId      int    // 用户ID
        Family      string // 家族
        Country     string // 国家
        Language    string // 语言
        MicCount    int    // 上麦数量
        JoinFee     int    // 入会费
        Announce    string // 公告内容
        Tag         string // 标签(游戏、闲聊、交友、家族、仅限女孩)
        MemberCount int    // 会员数
        Crdate      string // 创建时间
        OnlineCount int    // 在线用户数
    }* 
    */
    enterAudioRoom(roomid, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "enterAudioRoom";
        var msgData = { RoomId: roomid }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 退出房间  Data = 1 表示成功退出
     * 
     * @param roomid 
     * @param obj 
     * @param listener 
     */
    exitAudioRoom(roomid, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "exitAudioRoom";
        var msgData = { RoomId: roomid }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**获取房间
     * 
     * @param roomid 
     * @param obj 
     * @param listener 
     参数说明：
    // 语音房间信息
    type RoomInfo struct {
        RoomId      int    // 房间ID
        RoomName    string // 房间名称
        RoomImg     string // 房间图片
        UserId      int    // 用户ID
        Family      string // 家族
        Country     string // 国家
        Language    string // 语言
        MicCount    int    // 上麦数量
        JoinFee     int    // 入会费
        Announce    string // 公告内容
        Tag         string // 标签(游戏、闲聊、交友、家族、仅限女孩)
        MemberCount int    // 会员数
        Crdate      string // 创建时间
        OnlineCount int    // 在线用户数
    }* 
    */
    getAudioRoom(roomid, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoom";
        var msgData = { RoomId: roomid }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**加入房间(变成成员)
     * 
     * @param roomid 
     * @param obj 
     * @param listener 
     * @return data 1=加入成功   11=房间信息无效   12=扣减钻石失败    13=已加入过房间
     */
    addAudioRoomJoin(roomid, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "addAudioRoomJoin";
        var msgData = { RoomId: roomid }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**取消加入
     * 
     * @param roomid 
     * @param obj 
     * @param listener 
     * @return Data 1=取消成功
     */
    delAudioRoomJoin(roomid, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "delAudioRoomJoin";
        var msgData = { RoomId: roomid }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 获取加入房间列表
     * 
     * @param PageIndex 页索引
     * @param PageSize 页大小
     * @param obj 
     * @param listener 
          // 语音房间信息 
        RecordCount
        List[
            type RoomInfo struct {
                RoomId      int    // 房间ID
                RoomName    string // 房间名称
                RoomImg     string // 房间图片
                UserId      int    // 用户ID
                Family      string // 家族
                Country     string // 国家
                Language    string // 语言
                MicCount    int    // 上麦数量
                JoinFee     int    // 入会费
                Announce    string // 公告内容
                Tag         string // 标签(游戏、闲聊、交友、家族、仅限女孩)
                MemberCount int    // 会员数
                Crdate      string // 创建时间
                OnlineCount int    // 在线用户数
            }
        ]
     */
    getAudioRoomJoinList(PageIndex, PageSize, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoomJoinList";
        var msgData = { PageIndex: PageIndex, PageSize: PageSize }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 获取探索列表
     * 
     * @param PageIndex 页索引
     * @param PageSize 页大小
     * @param obj 
     * @param listener 
          // 语音房间信息 
        RecordCount
        List[
            type RoomInfo struct {
                RoomId      int    // 房间ID
                RoomName    string // 房间名称
                RoomImg     string // 房间图片
                UserId      int    // 用户ID
                Family      string // 家族
                Country     string // 国家
                Language    string // 语言
                MicCount    int    // 上麦数量
                JoinFee     int    // 入会费
                Announce    string // 公告内容
                Tag         string // 标签(游戏、闲聊、交友、家族、仅限女孩)
                MemberCount int    // 会员数
                Crdate      string // 创建时间
                OnlineCount int    // 在线用户数
            }
        ] 
     */
    getAudioRoomExploreList(PageIndex, PageSize, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoomExploreList";
        var msgData = { PageIndex: PageIndex, PageSize: PageSize }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**关注房间
     * 
     * @param roomid 
     * @param obj 
     * @param listener 
     * @return Data  1=关注成功   11=已关注过
     */
    addAudioRoomAttention(roomid, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "addAudioRoomAttention";
        var msgData = { RoomId: roomid }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**取消房间关注
     * 
     * @param roomid 
     * @param obj 
     * @param listener 
     * @return Data 1=取消成功
     */
    delAudioRoomAttention(roomid, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "delAudioRoomAttention";
        var msgData = { RoomId: roomid }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 是否已经关注过房间
     * 
     * @param roomid 
     * @param obj 
     * @param listener 
     */
    AudioRoomIsAttention(roomid, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomIsAttention";
        var msgData = { RoomId: roomid }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 获取关注房间列表
     * 
     * @param PageIndex 页索引
     * @param PageSize 页大小
     * @param obj 
     * @param listener 
          // 语音房间信息 
        RecordCount
        List[
            type RoomInfo struct {
                RoomId      int    // 房间ID
                RoomName    string // 房间名称
                RoomImg     string // 房间图片
                UserId      int    // 用户ID
                Family      string // 家族
                Country     string // 国家
                Language    string // 语言
                MicCount    int    // 上麦数量
                JoinFee     int    // 入会费
                Announce    string // 公告内容
                Tag         string // 标签(游戏、闲聊、交友、家族、仅限女孩)
                MemberCount int    // 会员数
                Crdate      string // 创建时间
                OnlineCount int    // 在线用户数
            }
        ] 
     */
    getAudioRoomAttentionList(PageIndex, PageSize, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoomAttentionList";
        var msgData = { PageIndex: PageIndex, PageSize: PageSize }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**获取浏览列表
     * 
     * @param PageIndex 
     * @param PageSize 
     * @param obj 
     * @param listener 
     */
    getAudioRoomBrowseList(PageIndex, PageSize, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoomBrowseList";
        var msgData = { PageIndex: PageIndex, PageSize: PageSize }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**用户在线列表
     * 
     * @param roomid 
     * @param PageIndex 
     * @param PageSize 
     * @param obj 
     * @param listener 
    UserId    int    // 用户Id
    NickName  string // 昵称
    Vip       int    // vip等级
    FaceUrl   string // 头像url
    FaceId    int    // 头像id
    Sex       int    // 性别 0=默认(无)  1=男   2=女
    UserWords string // 个性签名
     */
    getAudioRoomOnlineUsers(roomid, PageIndex, PageSize, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoomOnlineUsers";
        var msgData = { RoomId: roomid, PageIndex: PageIndex, PageSize: PageSize }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    //获取房间人数
    getAudioRoomHotInfo(roomid, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoomHotInfo";
        var msgData = { RoomId: roomid }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**获取房间成员列表
     * 
     * @param roomid 
     * @param PageIndex 
     * @param PageSize 
     * @param obj 
     * @param listener 
    UserId    int    // 用户Id
    NickName  string // 昵称
    Vip       int    // vip等级
    FaceUrl   string // 头像url
    FaceId    int    // 头像id
    Sex       int    // 性别 0=默认(无)  1=男   2=女
    UserWords string // 个性签名
     */
    getAudioRoomMembers(roomid, PageIndex, PageSize, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoomMembers";
        var msgData = { RoomId: roomid, PageIndex: PageIndex, PageSize: PageSize }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 邀请 返回 1 表示成功
     * 
     * @param toUserId 
     * @param roomid 
     * @param obj 
     * @param listener 
     */
    AudioRoomInvite(toUserId, roomid, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomInvite";
        var msgData = { ToUserId: toUserId, RoomId: roomid }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**接收邀请
     * 
     * @param code  邀请码
     * @param obj 
     * @param listener 
     */
    AudioRoomAccept(code, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomAccept";
        var msgData = { Code: code }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**订阅（客户端上传关心的房间id，类似于我们以前做的球赛赛事列表刷新处理）
     * 
     * @param roomids  房间ID数组
     */
    AudioRoomSubscribe(roomids: number[], obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomSubscribe" + new Date().getTime();
        var msgData = { RoomIds: roomids }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 精确查找 用户
     * 
     * @param targetUserID 
     * @param targetNickName 
     * @param obj 
     * @param listener 
     *[{\"FriendID\":82331555,\"NickName\":\"Abdui Basir\",\"Sex\":1,\"FaceID\":0,\"FaceUrl\":\"https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=458954745897843\\u0026height=200\\u0026width=200\\u0026ext=1648247018\\u0026hash=AeQBUxAznbMrhy50hKc\",\"UserWords\":\"\",\"GameStatus\":\"\",\"VipLevel\":0,\"PrivateRoomCount\":0,\"IsFriend\":0}]"} 
     */
    AudioUsersearch(targetUserID: number, targetNickName: string, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "searchUserList";
        var msgData = { TargetUserID: targetUserID, targetNickName: targetNickName }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 查找房间
     * 
     * @param obj 
     * @param listener 
     * @param RoomName 
     * @param PageIndex 
     * @param PageSize 
     */
    searchAudioRoom(roomName: string, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "searchAudioRoom";
        var msgData = { RoomName: roomName, PageIndex: 0, PageSize: 20 }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**推荐玩家搜索列表
     * 
     * @param obj 
     * @param listener 
     */
    recommendAudioRoomUser(obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "recommendAudioRoomUser";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**推荐房间搜索列表
     * 
     * @param obj 
     * @param listener 
     */
    recommendAudioRoom(obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "recommendAudioRoom";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**根据标签获取房间列表
     * 
     * @param tag 
     * @param obj 
     * @param listener 
     */
    AudioRoomListByTag(tag: string, pageIndex: number, pageSize: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomListByTag";
        var msgData = { Tag: tag, PageIndex: pageIndex, PageSize: pageSize }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 上麦
     * 
     * @param roomId 
     * @param sn  麦位(0~9)
     * @param obj 
     * @param listener 
     * Data 1=上麦成功  11=无效数据  12=已经在该麦位  13=麦锁定状态，无法上麦  14=该麦已占
     */
    AudioRoomOnTheMic(roomId: number, sn: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomOnTheMic" + new Date().getTime();
        var msgData = { RoomId: roomId, SN: sn }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 下麦
     * 
     * @param roomId 
     * @param sn  麦位(0~9)
     * @param obj 
     * @param listener 
     * Data 1=下麦成功
     */
    AudioRoomOffTheMic(roomId: number, sn: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomOffTheMic";
        var msgData = { RoomId: roomId, SN: sn }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 设置上锁
     * 
     * @param roomId 
     * @param sn  麦位(0~9)
     * @param status   状态 -1=上锁  0=解锁
     * @param obj 
     * @param listener 
     * Data 1=设置成功  11=空闲时才上锁  12=无效   
     */
    AudioRoomSetMic(roomId: number, sn: number, status: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomSetMic";
        var msgData = { RoomId: roomId, SN: sn, Status: status }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 获取房间mic位信息
     * 
     * @param roomId 
     * @param obj 
     * @param listener 
     */
    getAudioRoomMicList(roomId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoomMicList";
        var msgData = { RoomId: roomId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 检测语音房挂载
     * 
     */
    getAudioRoomOnlineRoom(obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoomOnlineRoom";
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }



    /** 3、是否允许进入房间
     * 
     * @param roomId 
     * @param password 
     * @param obj 
     * @param listener 
     * 1=可以进入房间  11=只能邀请进入   12=请输入正确房间密码   13=房间黑名单，无法进入 14隐藏房间
     */
    isEnterAudioRoom(roomId: number, password: string, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "isEnterAudioRoom";
        var msgData = { RoomId: roomId, Password: password }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 4、发出成员邀请加入
     * 
     * @param roomId 
     * @param ToUserId 
     * @param obj 
     * @param listener 
     * 1=邀请成功   1=邀请失败
     * 发送通知：{"NotifyId":1,"RoomId":43933941,"Data":{"UserId":43933941,"NickName":"web_guest","Code":"1680579633"}}
     */
    AudioRoomInviteJoin(roomId: number, ToUserId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomInviteJoin";
        var msgData = { ToUserId: ToUserId, RoomId: roomId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 5、接受成员邀请
     * 
     * @param code  Code string 邀请码，通过 Notify 获得
     * @param obj 
     * @param listener 
     *  1
     */
    AudioRoomAcceptJoin(code: string, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomAcceptJoin";
        var msgData = { Code: code }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 添加管理员
     * 
     * @param roomId 
     * @param ToUserId 
     * @param obj 
     * @param listener 
     * 1=添加成功  11=添加失败,不是管理员   12=已经是管理员
     */
    AudioRoomAddAdmin(roomId: number, ToUserId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomAddAdmin";
        var msgData = { RoomId: roomId, ToUserId: ToUserId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 删除管理员
     * 
     * @param roomId 
     * @param ToUserId 
     * @param obj 
     * @param listener 
     * 1=删除成功  11=删除失败,不是管理员  12=不是管理员,无法删除
     */
    AudioRoomDelAdmin(roomId: number, ToUserId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomDelAdmin";
        var msgData = { RoomId: roomId, ToUserId: ToUserId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**AudioRoomDelMember
     * 
     * @param roomId 
     * @param ToUserId 
     * @param obj 
     * @param listener 
     */
    AudioRoomDelMember(roomId: number, ToUserId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomDelMember";
        var msgData = { RoomId: roomId, ToUserId: ToUserId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 6、邀请上麦
     * 发送通知：{"NotifyId":6,"RoomId":43933941,"Data":{"UserId":43933941,"NickName":"web_guest","Code":"1680581050"}}
     * @param roomId 
     * @param toUserId 
     * @param obj 
     * @param listener 
     * 1=邀请成功  11=邀请失败   12=已在该麦位  13=麦位已占  15=麦位黑名单，不能邀请   16=只有管理员才能邀请
     */
    AudioRoomInviteOnTheMic(roomId: number, toUserId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomInviteOnTheMic";
        var msgData = { RoomId: roomId, ToUserId: toUserId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 踢麦(强制下麦) 
     * 
     * @param roomId 
     * @param toUserId 
     * @param obj 
     * @param listener 
     * 1=踢麦成功   11=踢麦失败
     */
    AudioRoomKickMic(roomId: number, toUserId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomKickMic";
        var msgData = { RoomId: roomId, ToUserId: toUserId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 房间内部搜索 用户信息
     * 
     * @param roomId 
     * @param userId 
     * @param obj 
     * @param listener 
     * Data":"{\"UserId\":43933941,\"NickName\":\"web_guest\",\"Vip\":0,\"FaceUrl\":\"\",\"FaceId\":4,\"Sex\":1,
     * \"Button\":{\"IsWatch\":0,\"IsTrack\":0},
     * \"RoleId\":1,\"IsMicBlack\":false}"
     */
    AudioRoomSearchUser(roomId: number, toUserId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomSearchUser";
        var msgData = { ToUserId: toUserId, RoomId: roomId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 7、接收上麦
     * 
     * @param roomId 
     * @param toUserId 
     * @param sn 
     * @param obj 
     * @param listener 
     *  1=接收成功   11=邀请已失效   12=已在该麦位   13=麦位已占  
     */
    AudioRoomAcceptOnMic(roomId: number, code: string, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomAcceptOnMic";
        var msgData = { RoomId: roomId, Code: code }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 8、获取权限列表
     * 
     * @param roomId 
     * @param obj 
     * @param listener 
     * "Data":"[{\"Enabled\":1,\"PermissionType\":1}]" 是否启用 (0=关闭   1=开启 / 权限类型 (1=麦位) )
     */
    AudioRoomGetPermission(roomId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomGetPermission";
        var msgData = { RoomId: roomId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 9、设置权限
     * 
     * @param roomId 
     * @param permissionType 
     * @param enabled 
     * @param obj 
     * @param listener 
     */
    AudioRoomSetPermission(roomId: number, permissionType: number, enabled: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomSetPermission";
        var msgData = { RoomId: roomId, PermissionType: permissionType, Enabled: enabled }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 10、获取进入房间条件
     * 
     * @param roomId 
     * @param obj 
     * @param listener 
     * "Data":"{\"InviteOnly\":false,\"Password\":\"\",\"PwdExpire\":0}"
     *         InviteOnly bool   // 只能邀请加入
        Password   string // 密码
        PwdExpire  int    // 过期时间戳
        IsHide  : bool  //是否隐藏房间
     */
    AudioRoomGetEnterCondition(roomId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomGetEnterCondition";
        var msgData = { RoomId: roomId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 11、设置进入房间条件
     * 
     * @param roomId 
     * @param inviteOnly  只能邀请加入
     * @param password  密码
     * @param IsHide  密码
     * @param obj 
     * @param listener 
     * 1=设置成功   11=设置失败
     */
    AudioRoomSetEnterCondition(roomId: number, inviteOnly: boolean, password: string, isHide: boolean, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomSetEnterCondition";
        var msgData = { RoomId: roomId, InviteOnly: inviteOnly, Password: password, IsHide: isHide }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /** 12、添加黑名单
     * 
     * @param roomId 
     * @param toUserId  用户ID
     * @param blackType  黑名单类型 (1=房间黑名单  2=麦位黑名单)
     * @param seconds  拉黑时长(秒)
     * @param obj  
     * @param listener 
     * 1=添加成功  11=添加失败   12=管理员才能添加
     */
    AudioRoomAddBlack(roomId: number, toUserId: number, blackType: number, seconds: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomAddBlack";
        var msgData = { RoomId: roomId, ToUserId: toUserId, BlackType: blackType, Seconds: seconds }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /** 
     * 
     */


    /** 13、获取黑名单用户列表
     * 
     * @param roomId 
     * @param blackType  黑名单类型 (1=房间黑名单  2=麦位黑名单)
     * @param pageIndex 
     * @param pageSize 
     * @param obj 
     * @param listener 
     * "Data":"{\"RecordCount\":2,\"List\":[{\"FaceId\":0,\"FaceUrl\":\"https://dl.dominogaming.net/heads/597.jpg\",\"NickName\":\"Atma\",\"Sex\":1,\"StillSeconds\":86305,\"UserId\":1014258,\"UserWords\":\"\",\"Vip\":0},{\"FaceId\":0,\"FaceUrl\":\"https://dl.dominogaming.net/heads/597.jpg\",\"NickName\":\"Atma\",\"Sex\":1,\"StillSeconds\":86305,\"UserId\":1014258,\"UserWords\":\"\",\"Vip\":0}]
     */
    AudioRoomGetBlackList(roomId: number, blackType: number, pageIndex: number, pageSize: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomGetBlackList";
        var msgData = { RoomId: roomId, BlackType: blackType, PageIndex: pageIndex, PageSize: pageSize }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /** 14、移除黑名单
     * 
     * @param roomId 
     * @param toUserId 
     * @param blackType  // 黑名单类型 (1=房间黑名单  2=麦位黑名单)
     * @param obj 
     * @param listener 
     *  1=移除成功  11=移除失败   12=管理员才能移除  13=不在黑名单内
     */
    AudioRoomRemoveBlack(roomId: number, toUserId: number, blackType: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomRemoveBlack";
        var msgData = { RoomId: roomId, ToUserId: toUserId, BlackType: blackType }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /**
     * 
        RoomId int // 房间ID
        ToUserId int // 操作对象
        OperateType int // 
        PageIndex int // 页索引
        PageSize int // 页大小
     */
    AudioRoomGetOperateLog(roomId: number, toUserId: number, operateType: number, pageIndex: number, pageSize: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomGetOperateLog";
        var msgData = { RoomId: roomId, ToUserId: toUserId, OperateType: operateType, PageIndex: pageIndex, PageSize: pageSize }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 购买赠送礼物
     * 
     * @param roomId 
     * @param toUserId 1标识所有
     * @param productId 
     * @param num 
     * @param obj 
     * @param listener 
     * {"Msg":"AudioRoomSendGiving","Data":"{\"Ret\":true,\"Message\":\"success\"}"}
     */
    AudioRoomSendGiving(roomId: number, toUserId: number, giftId: number, num: number, obj, listener, nickName: string = "") {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomSendGiving" + new Date().getTime();
        var msgData = { RoomId: roomId, ToUserId: toUserId, ToNickName: nickName, GiftId: giftId, Num: num }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 用户等级配置
     * 
     * @param roomId 
     * @param obj 
     * @param listener
     * 
     * "Data":"{\"List\":[{\"Exps\":5000,\"Level\":1,\"Ratio\":2},]}, 
        {"Msg":"getAudioRoomUpgradeConfig","Data":"{\"List\":[{\"Exps\":10,\"Level\":1,\"Icon\":1,\"Ratio\":0},{\"Exps\":70,\"Level\":2,\"Ratio\":0， },]}"}
    */
    getAudioRoomUpgradeConfig(roomId: number, obj, listener, sysFlag: boolean = true) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoomUpgradeConfig";
        var msgData = { RoomId: roomId, SysFlag: sysFlag }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    //获取房间拓展信息 防止房间信息拥堵
    getAudioRoomExtInfo(roomId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoomExtInfo";
        var msgData = { RoomId: roomId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 系统任务
     * 返回信息：
        sysFlag = true : {"Msg":"getAudioRoomSysTask","Data":"{\"List\":[{\"Action\":1,\"Desc\":\"10分钟/300Exp\",\"Exps\":300,\"Id\":101,\"Name\":\"上麦\",\"NeedNum\":10,\"Target\":90000},{\"Action\":2,\"Desc\":\"1钻石/1Exp\",\"Exps\":1,\"Id\":201,\"Name\":\"赠  送钻石礼物\",\"NeedNum\":1,\"Target\":100000000},{\"Action\":3,\"Desc\":\"1000金币/1Exp\",\"Exps\":1,\"Id\":301,\"Name\":\"赠送金币礼物\",\"NeedNum\":1000,\"Target\":10000}]}"}
        参数说明：
        Id      int    // id
        Name    string // 名称
        Desc    string // 描述
        Action  int    // 触发动作
        Target  int    // 目标
        NeedNum int    // 数值
        Exps    int    // 经验值

        sysFlag = false : "{\"List\":[{\"Action\":1,\"Desc\":\"5分钟/40俱乐部分\",\"Exps\":[40],\"Id\":1001,\"Name\":\"麦克风聊天\",\"NeedNum\":300,\"StatValue\":40,\"Target\":320},{\"Action\":2,\"Desc\":\"1条短信/10俱乐部分\",\"Exps\":[10],\"Id\":2001,\"Name\":\"在房间内发消息\",\"NeedNum\":1,\"StatValue\":100,\"Target\":100},{\"Action\":3,\"Desc\":\"1次/30俱乐部分\",\"Exps\":[30],\"Id\":3001,\"Name\":\"分享房间\",\"NeedNum\":1,\"StatValue\":60,\"Target\":60},{\"Action\":4,\"Desc\":\"1颗钻石/1俱乐部分\",\"Exps\":[1],\"Id\":4001,\"Name\":\"收到钻石礼物\",\"NeedNum\":1,\"StatValue\":100000,\"Target\":100000},{\"Action\":5,\"Desc\":\"1颗钻石/1俱乐部分\",\"Exps\":[1,1.2],\"Id\":5001,\"Name\":\"送钻石礼 物\",\"NeedNum\":1,\"StatValue\":1,\"Target\":0}]}"}
     */
    getAudioRoomSysTask(obj, listener, sysFlag: boolean = true) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoomSysTask";
        var msgData = { SysFlag: sysFlag }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**获取房间任务列表
     * 
     * @param roomId 
     * @param obj 
     * @param listener 
     * 返回信息：
        {"Msg":"getAudioRoomTaskList","Data":"{\"List\":[{\"Schedule\":0,\"TaskId\":101,\"TimeStamp\":1684229993},{\"Schedule\":0,\"TaskId\":301,\"TimeStamp\":1684229993},{\"Schedule\":38975,\"TaskId\":201,\"TimeStamp\":1684229993}]}"}

        参数说明：
            TaskId    int // 任务id
            Schedule  int // 进度
            TimeStamp int // 时间戳
     */
    getAudioRoomTaskList(roomId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoomTaskList";
        var msgData = { RoomId: roomId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 获取昨天收集信息
     * 
     * @param roomId 
     * @param obj 
     * @param listener 
     * 
     *    //"{\"List\":{\"Award\":[{\"Count\":65,\"ItemId\":2}],\"DayIndex\":19492,\"Points\":3295,\"Ratio\":2,\"Status\":1}}"
    // DayIndex int             // 日索引
    // Points   int             // 总点数
    // Ratio    float64         // 比率
    // Award    []item.ItemPack // 奖励
    // Status   int             // 状态(0=进行中  1=完成  2=已领取奖励) 
     */
    getAudioRoomCollect(roomId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "getAudioRoomCollect";
        var msgData = { RoomId: roomId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**领取收集奖励
     * 
     * @param roomId 
     * @param obj 
     * @param listener
     * 
     * {\"RetCode\":1,\"Message\":\"success\",\"Items\":[{\"ItemId\":2,\"Count\":65}]} 
     * 参数说明：
    RetCode int             // 操作结果
    Message string          // 消息
    Items   []item.ItemPack // 奖励
	
     */
    giftAudioRoomCollect(roomId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "giftAudioRoomCollect";
        var msgData = { RoomId: roomId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 获取用户房间信息
     * 
     * @param roomId 
     * @param obj 
     * @param listener 
     * 返回信息：
        {"Msg":"AudioRoomGetUserRoomInfo","Data":"{\"Level\":1,\"Exps\":0}"}
        参数说明：
            UserId     int
            RoomId     int    // 房间id
            Level      int    // 等级
            Exps       int    // 经验
     */
    AudioRoomGetUserRoomInfo(roomId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomGetUserRoomInfo";
        var msgData = { RoomId: roomId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 用户房间任务列表
     * 
     * @param roomId 
     * @param obj 
     * @param listener
     * 返回信息：
            {"Msg":"AudioRoomGetUserTaskList","Data":"{\"List\":[{\"CurrNum\":0,\"Schedule\":0,\"Status\":0,\"TaskId\":1001,\"TimeStamp\":1684840417},{\"CurrNum\":0,\"Schedule\":0,\"Status\":0,\"TaskId\":2001,\"TimeStamp\":1684840417},{\"CurrNum\":0,\"Schedule\":0,\"Status\":0,\"TaskId\":3001,\"TimeStamp\":1684840417},{\"CurrNum\":0,\"Schedule\":0,\"Status\":0,\"TaskId\":4001,\"TimeStamp\":1684840417},{\"CurrNum\":0,\"Schedule\":0,\"Status\":0,\"TaskId\":5001,\"TimeStamp\":1684840417}]}"}
        参数说明：
            TaskId    int // 任务id
            Schedule  int // 进度
            TimeStamp int // 时间戳 
     */
    AudioRoomGetUserTaskList(roomId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomGetUserTaskList";
        var msgData = { RoomId: roomId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 分享
     * 
     * @param roomId 
     * @param obj 
     * @param listener 
     *      返回信息：
        {"Msg":"AudioRoomShare","Data":"{\"RetCode\":1,\"Data\":\"success\"}"}
     */
    AudioRoomShare(roomId: number) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomShare";
        var msgData = { RoomId: roomId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        // this.addCallback(wd.Msg, obj, listener);
    }

    /**发送l聊天消息 统计
     * 
     * @param roomId 
     * @param obj 
     * @param listener 
     * 	{"Msg":"AudioRoomSendMessage","Data":"{\"RetCode\":1,\"Data\":\"success\"}"}
     */
    AudioRoomSendMessage(roomId: number) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomSendMessage";
        var msgData = { RoomId: roomId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        //   this.addCallback(wd.Msg, obj, listener);
    }

    /** 任务统计
     * 
     * @param obj 
     * @param listener
     * 返回信息：
        {"Msg":"AudioRoomGetUserRoomTaskStat","Data":"{\"List\":[{\"FinishNum\":1,\"TaskId\":2001},{\"FinishNum\":1,\"TaskId\":3001}]}"}
        参数说明：
            TaskId    int // 任务ID
            FinishNum int // 完成数 
     */
    AudioRoomGetUserRoomTaskStat(obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomGetUserRoomTaskStat";
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    //语聊房间 小游戏列表
    getAudioRoomGameList(roomId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomGetGameList";
        var msgData = { RoomId: roomId }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**设置屏幕锁定
     * 
     * @param roomId 
     * @param status  屏幕锁（1=锁定，0=解锁）
     * @param obj 
     * @param listener 
     */
    setRoomScreenLock(roomId: number, status: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomSetScreenLock";
        var msgData = { RoomId: roomId, ScreenLock: status }
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 检测是否可以聊天 锁屏禁止
     * 
     * @param roomId 
     * @param obj 
     * @param listener 
     * @ return true 禁止
     */
    checkIsBanSpeaker(roomId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomIsBannedSpeak";
        var msgData = { RoomId: roomId }
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 小游戏开始
     * 
     * @param gameId 
     * @param playType 
     * @param scene 
     * @param message 消息结构 
     * @param obj 
     * @param listener 
     */
    audioRoomGameStart(roomId: number, gameId: number, playType: number, scene: number, message: string, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomStartGame";
        var msgData = { RoomId: roomId, GameId: gameId, PlayType: playType, Scene: scene, MessageContent: message }
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 小游戏开始
     * 
     * @param roomId 语聊房房间号 
     * @param gameId 游戏ID，找服务器要 
     * @param ruleName 玩法，找服务器要 
     输出：
     // 返回RoomNo,IP:Port,TableId
        type GameRoomInfo struct {
            RoomNo     int    // 私人场号码
            ServerAddr string // 房间地址
            TableId    int    // 桌子号
        }    
     */
    audioRoomGetGameRoomInfo(roomId: number, gameId: number, ruleName: string, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "AudioRoomGetGameRoomInfo";
        var msgData = { RoomId: roomId, GameId: gameId, RuleName: ruleName }
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 测试充值
    public testPay(ProductId: string, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "testPay";
        var msgData = ProductId;
        wd.Data = msgData;
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    public myMatchList(pageIndex, pageSize, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "myMatchList";
        var msgData = { PageIndex: pageIndex, PageSize: pageSize };

        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    public myMatchUpdateStatus(Rid, Status, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = "myMatchUpdateStatus";
        var msgData = { Rid: Rid, Status: Status };

        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
     * 兑换道具 
     * productId 想兑换的产品
     */
    public exchangeGold(productId, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "exchangeGold";
        wd.Data = "" + productId
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    /*
     * 批量兑换道具 
     * productIds 想兑换的产品列表,传入数组
     */
    public exchangeGoldInBulk(productIds: Array<string>, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "exchangeGoldInBulk";
        wd.Data = JSON.stringify(productIds)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*获取我的装扮信息*/
    public getUserDecorations(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getUserDecorations";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        设置我的装扮
        decorationType 装扮类型
        itemId 道具ID，0表示取消装扮
    */
    public setUserDecoration(decorationType, itemId, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "setUserDecoration";
        wd.Data = JSON.stringify({
            Type: decorationType,
            ItemId: itemId
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // SwitchType 由前端定义 要求必须包含前缀SwitchType_
    public sendTrackState(status: number, switchType: string, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "setSwitchInfo";
        wd.Data = JSON.stringify({
            SwitchStatus: status,
            SwitchType: switchType + ""
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    public getTrackState(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getSwitchInfo";
        wd.Data = JSON.stringify({
            Data: '',
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    //获取等级经验的梯度值
    public getLevelList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "getLevelList";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    public doSharePlatform(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "doSharePlatform";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    public doShareGame(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "doShareGame";
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 完成新手引导 isSkip是否跳过
    public finishNoviceGuidance(isSkip: boolean, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "finishNoviceGuidance";
        if (isSkip) {
            wd.Data = "skip"
        }
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 游戏新手引导
    public finishGameNoviceGuidance(isSkip: boolean, gameName: string, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "finishGameNoviceGuidance";
        var msgData = {
            GameName: gameName,
            IsSkip: isSkip
        };
        wd.Data = JSON.stringify(msgData);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 是否显示模块的新手引导 如果gameName为空，则表示大厅新手引导
    /*
        var ret struct {
            Show bool
        }
    */
    public isShowNoviceGuidance(gameName: string, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "isShowNoviceGuidance";
        wd.Data = gameName
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /** !#zh betlist 游戏投注记录
      * @param {number} gameId 游戏ID
      * @param {number} days 天数(0=今天  1=昨天  7=最近一周  all=最近30天)
      * @param {number} pageIndex 第几页
      * @param {number} pageSize 每页页数 默认20
      * @param {number} BetZone "Match"=比赛场、"PrivateRoom"=私人场
      * 
      * 返回：
      ChineseName  string //游戏名称
      BetAmount    int    //下注金额
      BetTime      string //下注时间
      ResultAmount int    //结算金额
      Tax          int    //台费
      Status       int    //状态 1=下注  2=结算  3=撤销
  
      TotalBetAmount    int64 // 总投注额
      TotalResultAmount int64 // 总结算额
      RecordCount       int // 记录数
      */
    public betlist(gameId: number, days: number, pageIndex: number, pageSize: number, betZone: string, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = "betlist";
        var msgData = {
            GameID: gameId, Days: days, PageIndex: pageIndex,
            PageSize: pageSize, BetZone: betZone
        };
        wd.Data = JSON.stringify(msgData);
        console.log(wd)
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // vip
    public getNewVipUser(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'getNewVipUser';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    //pid=1破产补助，pid=2经验加成，pid=3好友人数，pid=4 入场特效
    public getNewVipList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'getNewVipList';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    public getNewVipPurchasePackage(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'getNewVipPurchasePackage';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    //用户徽章数据
    public getUserBadgeList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'getUserBadgeList';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    //获取指定用户佩戴的徽章
    public getBadgeWearList(userId: number, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'getBadgeWearList';
        wd.Data = JSON.stringify({
            UserId: userId,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**提交佩戴徽章
     * @param badgeId 徽章Id
     * @param position 用户佩戴位置：0、1、2 ; -1为取消佩戴
     */
    public wearBadge(badgeId: number, position: number, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'wearBadge';
        wd.Data = JSON.stringify({
            BadgeId: badgeId,
            Position: position,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /*删除账号*/
    public deleteAccount(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'deleteAccount';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        拉取礼物列表 
        输出：[Gift]
        type Gift struct {
            GiftId        int             // 礼物ID
            GiftType      int             // 礼物类型
            Name          string          // 名称
            Desc          string          `json:",omitempty"` // 描述
            Price         float64         // 价格
            PayType       int             // 支付方式
            ProductId     string          `json:",omitempty"` // 现金支付产品ID
            SenderCharm   int             // 发送者魅力值
            ReceiverCharm int             // 接收方魅力值
            Icon          string          `json:",omitempty"` // 图标
            Items         []item.ItemPack // 接收者得到的物品
            AnimationType int             // 动画类型
            UserType      int             `json:",omitempty"` // 成员类型
            IsBottom      bool            `json:",omitempty"` // 是否置底
        }

    */
    public GiftService_GetGiftList(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'GiftService_GetGiftList';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        赠送礼物
        输出：{RetCode(赠送返回值),Extra(扩展信息，如果需要充值则返回ProductId)}
            SendGiftRet_Failed = iota // 0赠送失败
            SendGiftRet_OK            // 1赠送成功
            SendGiftRet_Charge        // 2需要充值
        当RetCode==SendGiftRet_Charge,用Extra字段(ProductId)调起充值即可        
    */
    public GiftService_SendGift(toUserId, giftId, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'GiftService_SendGift';
        wd.Data = JSON.stringify({
            ToUserId: toUserId,
            GiftId: giftId,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        取消充值赠送，如果选择充值礼物，取消充值或充值失败调用
        输入：toUserId，接收方  productId，产品ID
        输出：空字符串 
    */
    public GiftService_CancelChargeGift(toUserId, productId, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'GiftService_CancelChargeGift';
        wd.Data = JSON.stringify({
            ToUserId: toUserId,
            ProductId: productId,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        获取我未接收的礼物信息，登录之后调用 或者 收到礼物通知时调用        
        输出：[]UserGift
        type UserGift struct {
            Rid       int    // 流水号
            Sender    int    // 发送方
            Receiver  int    // 接收方
            GiftId    int    // 礼物ID
            SendTime  int    // 发送时间
            ClaimTime int    // 领取时间，0表示未领取
            Items     string // 物品信息，防止系统表修改后变化
        }
    */
    // public GiftService_GetUnclaimedGifts(obj: any, listener: Function) {
    //     var wd: WebData = new WebData();
    //     wd.Msg = 'GiftService_GetUnclaimedGifts';
    //     this.sendData(JSON.stringify(wd));
    //     this.addCallback(wd.Msg, obj, listener);
    // }

    /*
        领取礼物
        输入：rid，用户礼物流水号
        输出：Items,领取的礼物列表
    */
    public GiftService_ClaimUserGift(rid, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'GiftService_ClaimUserGift';
        wd.Data = JSON.stringify({
            RID: rid,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        获取我收到的礼物列表
    */
    public GiftService_GetReceivedRecord(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'GiftService_GetReceivedRecord';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** !#zh addDot 添加打点 
    * @param {string} Scene 场景 (如:游戏,赛事,商城等)
    * @param {number} Action 动作(1=点击 2=完成 3=领取)
    * @param {string} Extra 扩展信息 (商品的ID,道具ID等没有的时候为空)
    * @memberof WebControl
    */
    public addDot(scene: string, action: number, extra: string) {
        var wd: WebData = new WebData();
        wd.Msg = 'addDot';
        wd.Data = JSON.stringify({
            Scene: scene,
            Action: action,
            Extra: extra
        });
        this.sendData(JSON.stringify(wd));
        // this.addCallback(wd.Msg, obj, listener);
    }

    /*破产补助 */
    public subsidyGetInfo(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'subsidyGetInfo';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    public subsidyGift(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'subsidyGift';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // vip每日签到礼包
    public giftDailyAward(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'giftNewVipDailyAward';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    // 获取vip每日签到是否可以签到
    public checkDailyAward(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'checkNewVipDailyAward';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 获取系统新手福利
    public getSysNoviceWelfare(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'getSysNoviceWelfare';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 获取用户新手福利
    public getUserNoviceWelfare(obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'getUserNoviceWelfare';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 领取新手福利奖励
    public receiveNoviceWelfareAward(dayIndex: number, obj: any, listener: Function, isFinalPackage
        : boolean = false) {
        var wd: WebData = new WebData();
        wd.Msg = 'claimNoviceWelfareAward';
        wd.Data = JSON.stringify({
            DayIndex: dayIndex,
            IsFinalPackage: isFinalPackage
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    //领取礼包物品 {Success,ErrorMsg}
    public claimNoviceWelfareGiftPack(packageId: number, termIndex: number, obj: any, listener: Function) {
        var wd: WebData = new WebData();
        wd.Msg = 'claimNoviceWelfareGiftPack';
        wd.Data = JSON.stringify({
            PackageId: packageId,
            TermId: termIndex,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    //麦位申请列表
    public getOnMicApplyList(roomid: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'AudioRoomGetOnMicApplyList';
        wd.Data = JSON.stringify({
            RoomId: roomid,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    //麦位申请
    public applyOnMic(roomid: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'AudioRoomApplyOnMic';
        wd.Data = JSON.stringify({
            RoomId: roomid,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    //麦位申请取消
    public cancelApplyOnMic(roomid: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'AudioRoomCancelApplyOnMic';
        wd.Data = JSON.stringify({
            RoomId: roomid,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /** //麦位申请处理
     * 
     * @param roomid 
     * @param touserId 
     * @param status (1=同意  2=拒绝) 
     * @param obj 
     * @param listener 
     */
    public dealOnMicApplyUser(roomid: number, touserId: number, status: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'AudioRoomDealApplyOnMic';
        wd.Data = JSON.stringify({
            RoomId: roomid,
            ToUserId: touserId,
            Status: status,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 语聊房间收益
     * 
        RoomId     int
        profitType  int  //用户或者游戏
        FromUserId int
        ItemType   int  0 1
        BeginTime  string
        EndTime    string
        PageIndex  int
        PageSize   int
        SortType   int  
        排序类型 (11=收益降序  12=收益升序   21=礼物收益降序  22=礼物收益升序  31=游戏收益降序  32=游戏收益升序  41=下注降序  42=下注升序)
    */
    public getAudioIncomeStat(roomId: number, profitType: number, userid: number, type: number, beginTime: string, endTime: string, page: number, pageSize: number, sortType: number, obj, listener) {
        var wd: WebData = new WebData();

        if (1 == profitType) {
            //用户收益
            wd.Msg = 'AudioRoomGetUserIncomeStat';
            wd.Data = JSON.stringify({
                RoomId: roomId,
                FromUserId: userid,
                ItemType: type,
                BeginTime: beginTime,
                EndTime: endTime,
                PageIndex: page,
                PageSize: pageSize,
                SortType: sortType,
            });
        }
        else {
            //游戏收益
            wd.Msg = 'AudioRoomGetGameIncomeStat';
            wd.Data = JSON.stringify({
                RoomId: roomId,
                FromUserId: 0,
                ItemType: type,
                BeginTime: beginTime,
                EndTime: endTime,
                PageIndex: page,
                PageSize: pageSize,
                SortType: sortType,
            });
        }
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**
     * 
     * @param roomId 
     * @param gameid 
     * @param obj 
     * @param listener 
         返回信息：
      IncomeLevel     int     // 收益等级
      GoldIncomeRatio float64 // 金币流水收益比率
      ChipIncomeRatio float64 // 钻石流水收益比率* 
     */
    public audioRoomGetIncomeInfo(roomId: number, gameid: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'AudioRoomGetIncomeInfo';
        wd.Data = JSON.stringify({
            RoomId: roomId,
            GameId: gameid,
        });

        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 开启房间游戏
     * 
     * @param roomId 
     * @param userid 
     * @param gameid 
     * @param ruleName 
     * 
     * return RetMsg
     */
    public AudioRoomCreateGameRoom(roomId: number, userid: number, gameid: number, ruleName: string, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'AudioRoomCreateGameRoom';
        wd.Data = JSON.stringify({
            RoomId: roomId,
            GameId: gameid,
            RuleName: ruleName,
        });

        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 关闭房间游戏
     * 
     * @param roomId 
     * @param userid 
     * @param gameid 
     * @param roomNo 
     * @param obj 
     * @param listener 
     */
    public AudioRoomCloseGameRoom(roomId: number, userid: number, gameid: number, roomNo: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'AudioRoomCloseGameRoom';
        wd.Data = JSON.stringify({
            RoomId: roomId,
            UserId: userid,
            GameId: gameid,
            RoomNo: roomNo,
        });

        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /** 获取黑名单
    */
    public getBlackList(pageIndex: number, pageSize: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'friendGetBlackList';
        wd.Data = JSON.stringify({
            PageIndex: pageIndex,
            PageSize: pageSize,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }


    /** 加入黑名单
    */
    public addBlack(toUserId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'friendAddBlack';
        wd.Data = JSON.stringify({
            ToUserId: toUserId,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 移除黑名单
    */
    public delBlack(toUserId: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'friendDelBlack';
        wd.Data = JSON.stringify({
            ToUserId: toUserId,
        });
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /** 获取游戏房间列表 已经开启
     * 
     * @param roomid 
     * @param gameid   传入0 获取所有
     * @param obj 
     * @param listener 
     * 
     * 返回信息：
        RoomNo     int    // 私人场号码
        ServerAddr string // 房间地址
        TableId    int    // 桌子号
        GameId     int    // 游戏id
        RuleName   string // 玩法
     */
    public AudioRoomGetGameRoomList(roomid: number, gameid: number, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'AudioRoomGetGameRoomList';
        wd.Data = JSON.stringify({
            RoomId: roomid,
            GameId: gameid,
        });

        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        slots 指令
        目前只支持bambooslot
    */
    // 获取游戏配置，gamename传：EnglishName
    public getSlotsServiceConfig(gameName: string, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'getSlotsServiceConfig';
        wd.Data = gameName

        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /* 
        下注
            gameName同上
            amount:下注数量，isChip是否下钻石
            extra: 扩展下注信息，bambooslot没有
        输出：下注结果
    */
    public doSlotsServiceSpin(gameName: string, amount, isChip, extra, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'doSlotsServiceSpin';
        wd.Data = JSON.stringify({
            GameName: gameName,
            Amount: amount,
            IsChip: isChip,
            Extra: extra
        });

        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        转盘协议   
        获取转盘信息 返回：
        type WheelItem struct {
            Index    int           
            Prize    item.ItemPack           
        }

        type UserWheelInfo struct {
            Price  item.ItemPack // 转盘所需道具
            Owned  int           // 当前拥有道具数量
            IsFree bool
            Items  []WheelItem
            RefreshTaskCost item.ItemPack // 刷新任务所需道具
        }
    */
    public getDailyWheelInfo(obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'getDailyWheelInfo';
        console.log(wd);
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    /*          
       抽奖 返回：
        type WheelResult struct {
            Success  bool
            ErrorMsg string
            Index    int           // 中奖项目Index
            Items    item.ItemPack // 中奖内容
        }
    */
    public doDailyWheel(obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'doDailyWheel';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 刷新转盘任务 返回"ok"刷新成功，否则刷新失败
    public refreshDailyWheelTask(obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'refreshDailyWheelTask';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**
     * 获取等级奖励列表
     */
    public getLevelRewards(obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'getLevelRewards';
        wd.Data = '';
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /**
     * 领取等级奖励
     * @param level 等级
     * @param isPaid 是否付费
     */
    public claimLevelRewards(level: number, isAll: boolean, isPaid: boolean, obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'claimLevelRewards';
        wd.Data = JSON.stringify({
            Level: level,
            IsAll: isAll,
            IsPaid: isPaid
        });

        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /* 排位赛协议 */
    /*
        获取配置信息
        ConsecutiveWinPoints []pb.ConsecutiveWinPoint // 连胜额外点数配置
        WinPoint             int                      // 赢点数
        LosePoint            int                      // 输点数
        ConsecutiveCardPrice item.ItemPack            // 连胜卡配置
        LadderConfigs        []pb.LadderConfig        // 段位详细配置        
    */
    public getLadderSystemInfo(obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'LadderService_getSystemInfo';        
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    /*
        获取用户等级信息
        
        type UserLadderInfo struct {
            Ladder int // 所在段位
            Level  int // 段位等级
            Star   int // 星星数量
            Point     int // 分数
            MaxConWin int // 最大连胜
            WinCount  int // 赢
            LoseCount int // 输
            DrawCount int // 平
            CurConWin int // 当前累计连胜
        }
    */
    public getUserLadderInfo(gameId,obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'LadderService_getUserInfo';
        wd.Data = gameId+""        
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 获取玩家当前连胜次数，userId传0获取自己
    public getUserConsecutiveWinCount(userId,gameId,obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'LadderService_getUserConsecutiveWinCount';
        wd.Data = JSON.stringify({
            UserId: userId,
            GameId: gameId,            
        });        
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }

    // 使用连胜卡 返回 ok 或 failed
    public useConsecutiveCard(obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'LadderService_useConsecutiveCard';         
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    /*
        获取房间列表,返回数组
        type GameRoomInfo struct {
            GameId            int
            BaseScore         int    // 底分
            MinGold           int    // 最小携带
            MaxGold           int    // 最大携带 0不限制
            AdditionalPercent int    // 加成
            ServerAddr        string // 服务器地址
            RoomName          string // 坐下规则名称
            GameRule          string // 详细规则
        }        
    */
    public getLadderRoomList(obj, listener) {
        var wd: WebData = new WebData();
        wd.Msg = 'LadderService_getRoomList';         
        this.sendData(JSON.stringify(wd));
        this.addCallback(wd.Msg, obj, listener);
    }
    /* 
        通知补充
        Notification_LadderChanged // 45=排位赛等级信息变化 {Old{Ladder,Level,Star,Point},New{Ladder,Level,Star,Point}}
    */
    /* 排位赛结束 */
}

