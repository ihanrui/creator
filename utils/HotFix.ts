

import { _decorator, sys, native, game } from 'cc';

// // 热更逻辑类
// // 热更遵循creator默认规范，参考http://docs.cocos.com/creator/manual/zh/advanced-topics/assets-manager.html
// // 程序在第一个界面执行更新检查，调用HotFix.checkUpgrade
// // 热更模块本身区分web及native，如果是web，直接回调Failed
// // 热更后会自动重启游戏
// // 热更界面由上层根据消息制作
// // 生成的Project.manifest放入assets\resources目录
// // 注意发布包需要两次Build：Build->生成Project.manifest->Build
// // main.js需要添加searchPath，因为main.js每次build都会重建，所以需要修改template代码，
// //      具体位置为C:\CocosCreator\resources\static\build-templates\shares
// 

// 更新通知消息类型
export const enum UpgradeState {
    Progress = 0,	// 更新进度，后面带进度百分比参数
    Finished,		// 更新完成，自动重启应用
    Failed,         // 更新中止,或者不需要更新
    Updated,        // 已经更新过了
    StartDownload,  // 开始下载更新
};

export class HotFix {
    public static Version = "1.0.1";
    public static DownloadRoot: string = "https://cdn.cocobaloot.com/saudi_asset/assets/";
    public static ReviewUrl = "https://www.cocobaloot.com:8088/hall/isInReview";

    private static _onState: Function;
    private static _target: any;
    private static _am: any;
    private static _upgrading: boolean;
    private static _localManifest: string;
    private static _updatedModules: string[] = new Array();
    private static _moduleName: string;
    public static _localAssets;
    private static testNoUpdate = false;


    private static versionCompareHandle(versionA, versionB): number {
        console.log("JS Custom Version Compare: version A is " + versionA + ', version B is ' + versionB);
        var vA = versionA.split('.');
        var vB = versionB.split('.');
        for (var i = 0; i < vA.length; ++i) {
            var a = parseInt(vA[i]);
            var b = parseInt(vB[i] || 0);
            if (a === b) {
                continue;
            }
            else {
                return a - b;
            }
        }
        if (vB.length > vA.length) {
            return -1;
        }
        else {
            return 0;
        }
    }

    private static isModuleUpdated(moduleName: string): boolean {
        for (var i = 0; i < HotFix._updatedModules.length; i++) {
            if (HotFix._updatedModules[i] == moduleName) {
                return true;
            }
        }
        return false;
    }

    private static createModule_manifest(moduleName: string) {
        if (!moduleName || moduleName == "") {
            return
        }
        // 先看看安装包中有没有manifest
        var localManifest = "assets/" + moduleName + "/project.manifest";
        var localData = native.fileUtils.getStringFromFile(localManifest);
        if (localData == "") {
            var m = {
                packageUrl: HotFix.DownloadRoot + moduleName,
                remoteManifestUrl: HotFix.DownloadRoot + moduleName + "/project.manifest",
                remoteVersionUrl: HotFix.DownloadRoot + moduleName + "/version.manifest",
                version: "0",
                assets: {},
                searchPaths: {}
            }
            localData = JSON.stringify(m)
        }
        // 这个manifest文件不能跟下载路径同名 
        // local manifest = moduleName .. "/project.manifest"
        // if require("jni/JniBridge").isWin32() then   

        var dir = native.fileUtils.getWritablePath() + "hotfix_asset/assets/" + moduleName;
        native.fileUtils.createDirectory(dir);
        native.fileUtils.writeStringToFile(localData, dir + "/project.manifest");
    }

    public static isGameInstalled(moduleName): boolean {
        if (!sys.isNative) {
            return true
        }

        var manifest = native.fileUtils.getWritablePath() + "hotfix_asset/assets/" + moduleName + "/project.manifest";
        if (!native.fileUtils.isFileExist(manifest)) {
            HotFix.createModule_manifest(moduleName);
            return HotFix.isGameInstalled(moduleName);
        }
        var data = native.fileUtils.getStringFromFile(manifest);
        var m = JSON.parse(data);
        if (!m || !m.version || m.version == "") {
            return false;
        }
        console.log("isGameInstalled version : " + m.version);
        return m.version != "0";
    }

    private static checkModuleUpdate(moduleName: string, onState: Function, target: any) {
        HotFix._localManifest = native.fileUtils.getWritablePath() + "hotfix_asset/assets/" + moduleName + "/project.manifest";
        if (!native.fileUtils.isFileExist(HotFix._localManifest)) {
            HotFix.createModule_manifest(moduleName);
        }
        console.log('HotFix._localManifest', HotFix._localManifest)
        HotFix.checkUpdate(moduleName);
    }

    public static checkUpgrade(onState: Function, target: any, moduleName: string) {
        if (!sys.isNative || this.testNoUpdate || sys.platform == "WIN32") {
            onState.call(target, UpgradeState.Finished);
            return
        }
        if (onState == null || target == null) {
            console.log("HotFix checkUpgrade invalid args");
            return;
        }

        HotFix._onState = onState;
        HotFix._target = target;
        if (HotFix._upgrading) {
            console.log("HotFix.checkVersion upgrading");
            onState.call(target, UpgradeState.Failed);
            return;
        }

        HotFix._upgrading = true;
        HotFix._moduleName = moduleName;
        if (!moduleName) {
            HotFix._updatedModules = new Array();
        }
        console.log("checkUpgrade --- ", native.fileUtils.getSearchPaths());

        // 如果已经更新过了则直接成功
        if (moduleName) {
            if (HotFix.isModuleUpdated(moduleName)) {
                HotFix._upgrading = false;
                HotFix._am.setEventCallback(null);
                onState.call(target, UpgradeState.Updated);
                return;
            }
            HotFix.checkModuleUpdate(moduleName, onState, target);
            //HotFix.checkUpdate();
        } else {
            let localManifest = native.fileUtils.getWritablePath() + "hotfix_asset/" + "/project.manifest";
            if (native.fileUtils.isFileExist(localManifest)) {
                HotFix._localManifest = localManifest;
                console.log("checkUpgrade local updated manifest = " + HotFix._localManifest);
                HotFix.checkUpdate(null);
                return;
            }
            HotFix._localManifest = HotFix._localAssets.nativeUrl;
            console.log("checkUpgrade using local manifest = " + HotFix._localManifest);
            HotFix.checkUpdate(null);
        }
    }

    private static checkUpdate(moduleName) {
        // 加载运行路径
        var storagePath = ((native.fileUtils ? native.fileUtils.getWritablePath() : '/') + 'hotfix_asset/');
        if (moduleName) {
            storagePath = storagePath + "assets/" + moduleName + "/";
        }

        // 创建assetmanager         
        HotFix._am = new native.AssetsManager('', storagePath, HotFix.versionCompareHandle);

        HotFix._am.setVerifyCallback(function (path, asset) {
            // When asset is compressed, we don't need to check its md5, because zip file have been deleted.
            var compressed = asset.compressed;
            // Retrieve the correct md5 value.
            var expectedMD5 = asset.md5;
            // asset.path is relative path and path is absolute.
            var relativePath = asset.path;
            // The size of asset file, but this value could be absent.
            var size = asset.size;
            if (compressed) {
                console.log("Verification passed : " + relativePath);
                return true;
            }
            else {
                console.log("Verification passed : " + relativePath + ' (' + expectedMD5 + ')');
                return true;
            }
        });

        if (sys.platform === sys.Platform.ANDROID) {
            // Some Android device may slow down the download process when concurrent tasks is too much.
            // The value may not be accurate, please do more test and find what's most suitable for your game.
            HotFix._am.setMaxConcurrentTask(2);
        }

        // 检查版本
        HotFix.checkVersion();
    }

    private static startDownload() {
        if (HotFix._am) {
            HotFix._am.setEventCallback(HotFix.onUpgradeHandler);

            if (HotFix._am.getState() === native.AssetsManager.State.UNINITED) {
                // Resolve md5 url
                var url = HotFix._localManifest;

                //Todo 没找到对应接口位置
                // if (cc.loader.md5Pipe) {
                //     url = cc.loader.md5Pipe.transformURL(url);
                // }
                HotFix._am.loadLocalManifest(url);
            } else {
                console.log("startDownload", url);
                HotFix._onState.call(HotFix._target, UpgradeState.StartDownload, HotFix._am.getTotalBytes(), HotFix._am.getRemoteManifest().getVersion());
            }
            HotFix._am.update();
        }
    }

    private static onCheckHandler(event) {
        console.log('onCheckHandler Code: ' + event.getEventCode());
        var failed = false;
        var errorMsg = "";
        switch (event.getEventCode()) {
            case native.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                console.log("No local manifest file found, hot update skipped.", HotFix._localManifest);
                failed = true;
                errorMsg = "Local package is damaged,please try to re-install.";
                break;
            case native.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case native.EventAssetsManager.ERROR_PARSE_MANIFEST:
                console.log("Fail to download manifest file, hot update skipped.", HotFix._localManifest);
                //errorMsg = "Download failed,please try again later.";
                failed = true;
                break;
            case native.EventAssetsManager.ALREADY_UP_TO_DATE:
                console.log("Already up to date with the latest remote version.");
                break;
            case native.EventAssetsManager.NEW_VERSION_FOUND:
                console.log('New version found, please try to update.');
                HotFix.startDownload();
                return;
            case native.EventAssetsManager.UPDATE_PROGRESSION:
                return;
        }
        // 如果不用下载，则通知上层        
        HotFix._am.setEventCallback(null);
        HotFix._upgrading = false;
        if (failed) {
            HotFix._onState.call(HotFix._target, UpgradeState.Failed);
            return;
        }
        if (HotFix._moduleName) {
            HotFix._updatedModules.push(HotFix._moduleName);
        }
        HotFix._onState.call(HotFix._target, UpgradeState.Finished);
    }

    private static onUpgradeHandler(event) {
        console.log("onUpgradeHandler --", event.getEventCode());
        var needRestart = false;
        var failed = false;
        var errorMsg = "";
        switch (event.getEventCode()) {
            case native.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                console.log('No local manifest file found, hot update skipped.');
                failed = true;
                //"الحزمة المحلية تالفة ، يرجى محاولة إعادة التثبيت.":"Local package is damaged,please try to re-install."
                errorMsg = "الحزمة المحلية تالفة ، يرجى محاولة إعادة التثبيت.";
                break;
            case native.EventAssetsManager.UPDATE_PROGRESSION:
                HotFix._onState.call(HotFix._target, UpgradeState.Progress, event.getPercent());
                return;
            case native.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case native.EventAssetsManager.ERROR_PARSE_MANIFEST:
                console.log('Fail to download manifest file, hot update skipped.');
                //"فشل التنزيل ، يرجى المحاولة مرة أخرى لاحقًا.":"Download failed,please try again later."
                //errorMsg = "فشل التنزيل ، يرجى المحاولة مرة أخرى لاحقًا.";
                failed = true;
                break;
            case native.EventAssetsManager.ALREADY_UP_TO_DATE:
                console.log('Already up to date with the latest remote version.');
                failed = false;
                break;
            case native.EventAssetsManager.UPDATE_FINISHED:
                console.log('Update finished. ' + event.getMessage());
                needRestart = true;
                break;
            case native.EventAssetsManager.UPDATE_FAILED:
                console.log('Update failed. ' + event.getMessage());
                failed = true;
                break;
            case native.EventAssetsManager.ERROR_UPDATING:
                console.log('Asset update error: ' + event.getAssetId() + ', ' + event.getMessage());
                failed = true;
                errorMsg = "فشل التنزيل ، يرجى المحاولة مرة أخرى لاحقًا.";
                break;
            case native.EventAssetsManager.ERROR_DECOMPRESS:
                console.log(event.getMessage());
                failed = true;
                errorMsg = "فشل التنزيل ، يرجى المحاولة مرة أخرى لاحقًا.";
                break;
            case native.EventAssetsManager.NEW_VERSION_FOUND:
            case native.EventAssetsManager.ASSET_UPDATED:
                return;
            default:
                console.log("unhandled onUpgradeHandler", event.getEventCode());
                break;
        }

        if (failed) {
            HotFix._onState.call(HotFix._target, UpgradeState.Failed, errorMsg);
            HotFix._am.setEventCallback(null);
            HotFix._upgrading = false;
            return
        } else {
            HotFix._upgrading = false;
            HotFix._am.setEventCallback(null);
            if (HotFix._moduleName) {
                HotFix._updatedModules.push(HotFix._moduleName);
            }
            if(HotFix._moduleName || !needRestart){
                HotFix._onState.call(HotFix._target, UpgradeState.Finished);
            }
        }

        if (needRestart && !HotFix._moduleName) {
            console.log("restarting.........");

            //Todo: 后续处理音频
            //cc.audioEngine.stopAll();
            setTimeout(_ => {
                game.restart();
            }, 400)//不延时会报错
        }
    }

    private static checkVersion() {
        if (HotFix._am.getState() === native.AssetsManager.State.UNINITED) {
            // 加载本地manifest
            // Resolve md5 url           
            var url = HotFix._localManifest;

            //Todo 没找到对应接口位置
            //     if (cc.loader.md5Pipe) {
            //         url = cc.loader.md5Pipe.transformURL(url);
            //     }
            console.log(url);
            HotFix._am.loadLocalManifest(url);

            if (!HotFix._am.getLocalManifest() || !HotFix._am.getLocalManifest().isLoaded()) {
                console.log('Failed to load local manifest ...');
                //"فشل تحميل الموارد":"loading resources failed"
                HotFix._onState.call(HotFix._target, UpgradeState.Failed, "فشل تحميل الموارد");
                this._upgrading = false;
                return;
            }
            HotFix.Version = HotFix._am.getLocalManifest().getVersion();
            //Domain.Version = Domain.Version.replace(/\./g,"");
            HotFix._am.setEventCallback(HotFix.onCheckHandler);
            HotFix._am.checkUpdate();
        }
    }

    public static stopUpgrade() {
        if (HotFix._am) {
            HotFix._am.setEventCallback(null);
        }
        HotFix._upgrading = false;
        HotFix._onState.call(HotFix._target, UpgradeState.Failed, "");
    }

    public static removeUpdateFiles() {
        if (sys.isNative) {
            native.fileUtils.removeDirectory(native.fileUtils.getWritablePath() + "hotfix_asset/");
            HotFix._upgrading = false;
            HotFix._am.setEventCallback(null);
            console.log("restarting.........");

            //Todo 后续修改音频
            //cc.audioEngine.stopAll();
            game.restart();
        }
    }
};
