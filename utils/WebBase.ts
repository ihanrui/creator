import { _decorator, Component, sys, director } from 'cc';
import { WebData, WebCallback } from "./WebData";
import { ByteArray } from "../3rd/ByteArray";
import { Domain } from "../Domain";



export class WebBase extends Component {
    protected m_socket: WebSocket;
    protected m_callbackOnce: Map<string, WebCallback>;
    protected m_callbackPersist: Map<string, WebCallback>;
    protected m_loginData: string;
    protected m_url: string;
    protected m_socketObject;
    protected m_onConnect;
    protected m_onCloseConnect;
    protected m_onLogined;

    //0 默认  1 重连中  2 不再连了
    public reconnect_state: number;
    public reconnect_count: number;
    // protected m_bForceClose:boolean;
    // private m_bStopPing:boolean;
    // private m_bNoReconnect:boolean;
    private m_cacheData: Array<string>;
    public m_requestID = 0
    private m_pinging;

    public constructor() {
        super();
        console.log("WebBase.constructor()-------");
        this.m_callbackOnce = new Map();
        this.m_callbackPersist = new Map();
        this.m_loginData = "";
        this.m_cacheData = [];
        this.reconnect_state = 0;
        this.reconnect_count = 0;
        this.m_requestID = 0;
        this.m_pinging = false;
    }

    public initWebSocket(url: string, obj?: any,
        onConnect?: Function, onCloseConnect?: Function,
        onLogined?: Function): void {
        //this.m_callbackOnce.clear();
        //this.m_callbackPersist.clear();
        if (this.m_socket) {
            console.log("initWebSocket closing exist socket");
            this.m_socket.onclose = this.m_socket.onerror = null;
            this.m_socket.close();
            this.m_socket = null;
        }
        // this.m_bForceClose = noReconnect;
        // this.m_bNoReconnect = noReconnect;
        this.m_pinging = false;
        this.m_url = url;
        this.m_socketObject = obj;
        this.m_onConnect = onConnect;
        this.m_onCloseConnect = onCloseConnect;
        this.m_onLogined = onLogined;
        this.m_requestID = 0
        //创建 WebSocket 对象      
        this.m_socket = Domain.newWebsocket(url);
        var self = this;
        this.m_socket.onopen = function (evt) {
            self.onSocketConnected()
        };

        this.m_socket.onmessage = function (event) {
            if (sys.isNative) {
                var byte: ByteArray = new ByteArray(event.data);
                self.onReceiveMessage(byte.readUTFBytes(byte.length));
                return;
            }
            var reader = new FileReader();
            reader.readAsText(event.data, 'utf-8');

            reader.onload = function (e) {
                self.onReceiveMessage(reader.result as string);
            }
        };

        this.m_socket.onerror = function (event) {
            console.log("Send Text fired an error", event);
            self.m_socket = null;
            self.stopCheckPing();
        };

        this.m_socket.onclose = function (event) {
            console.log("onclose", event);
            self.m_socket = null;
            self.stopCheckPing();
            if (self.m_socketObject && self.m_onCloseConnect) {
                self.m_onCloseConnect.call(self.m_socketObject);
            }
        };
    }

    protected onSocketConnected() {
        console.log("websocket connected.");
        if (!this.isCanSendData()) {
            console.log("onconnect what happened ???");
            return;
        }
        this.reconnect_state = 0;
        this.startCheckPing();
        //连上即刻获取是否审核
        // this.requestInReviewGame()
        if (this.m_loginData != "") {
            console.log("WebBase.onSocketConnected sending loginData", this.m_loginData)
            this.m_socket.send(this.m_loginData);
            return;
        } else {
            this.sendCacheData();
        }
        if (this.m_socketObject && this.m_onConnect) {
            this.m_onConnect.call(this.m_socketObject);
        }
    }

    public sendCacheData() {
        if (this.m_cacheData.length > 0) {
            console.log("sending cache data begin");
            for (let i = 0, len = this.m_cacheData[i].length; i < len; i++) {
                if (this.m_cacheData[i]) this.m_socket.send(this.m_cacheData[i]);
            }
            console.log("sending cache data end");
            this.m_cacheData.length = 0;
        }
    }
    public clearCacheData() {
        this.m_cacheData.length = 0;
    }

    public closeSocketWithReconnect() {
        this.stopCheckPing();
        if (!this.m_socket)
            return;
        console.log("WebBase.closeSocket", this.m_socket);
        this.m_socket.close();
        if (this.m_socket) {
            this.m_socket.onclose = this.m_socket.onerror = this.m_socket.onopen = null;
        }
        this.m_socket = null;

        if (this.m_socketObject && this.m_onCloseConnect) {
            this.m_onCloseConnect.call(this.m_socketObject);
        }
    }
    public forceClose(){
        this.m_onCloseConnect = this.m_socket.onclose = this.m_socket.onerror = this.m_socket.onopen = null;
        this.m_socket && this.m_socket.close();
    }
    public closeSocket(): void {
        this.reconnect_state = 2;
        this.m_loginData = "";
        this.closeSocketWithReconnect()
    }

    public isConnected(): boolean {
        if (this.m_socket == null) {
            return false;
        }
        return this.m_socket.readyState == WebSocket.OPEN ||
            this.m_socket.readyState == WebSocket.CONNECTING;
    }

    public isCanSendData(): boolean {
        return this.m_socket && this.m_socket.readyState == WebSocket.OPEN;
    }

    protected onReceiveMessage(data): void {
    }

    public sendPing(): boolean {
        if (this.m_pinging) {
            return false;
        }
        if (!this.isCanSendData()) return false;
        var wd: WebData = new WebData();
        wd.Msg = "ping"
        this.m_socket.send(JSON.stringify(wd));
        this.m_pinging = true;
        var scheduler = director.getScheduler();
        scheduler.schedule(this.onPingTimeout, this, 8, 0, 0, false);
        return true;
    }

    private onPingTimeout() {
        director.getScheduler().unschedule(this.onPingTimeout, this);
        if (!this.m_pinging) {
            return
        }
        this.m_pinging = false;
        console.log("WebBase.onPingTimeout");
        this.closeSocketWithReconnect();
    }

    private startCheckPing() {
        // if(this.m_bNoReconnect) {
        //     return;
        // }
        console.log("WebBase.startCheckPing");
        this.stopCheckPing();
        var scheduler = director.getScheduler();
        scheduler.schedule(this.sendPing, this, 10, 0, 0, false);
    }

    private stopCheckPing() {
        this.m_pinging = false;
        var scheduler = director.getScheduler();
        scheduler.unschedule(this.sendPing, this);
    }

    public doConnect() {
        // if(this.m_bForceClose)return;
        if (this.isConnected()) return;
        this.initWebSocket(this.m_url, this.m_socketObject,
            this.m_onConnect, this.m_onCloseConnect,
            this.m_onLogined);
    }

    public registerCallback(msg: string, obj: any, listaner: Function) {
        var wc = new WebCallback();
        wc.Listener = listaner;
        wc.Target = obj;
        this.m_callbackPersist.set(msg, wc);
    }

    public unRegisterCallback(msg: string) {
        this.m_callbackPersist.delete(msg);
    }

    public addCallback(msg: string, obj: any, listaner: Function) {
        if (obj == null || listaner == null) {
            return;
        }
        var wc = new WebCallback();
        wc.Listener = listaner;
        wc.Target = obj;
        this.m_callbackOnce.set(msg, wc);
    }

    public sendData(data: string): boolean {
        this.m_requestID++;
        if (this.isCanSendData()) {
            this.m_socket.send(data);
            return true;
        } else {
            this.m_cacheData.push(data);
            return false;
        }
    }

    protected onPingReturn() {
        this.m_pinging = false;
    }
}
