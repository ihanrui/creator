
import { AudioClip, Button, EventHandler, Vec3, isValid } from "cc";
import { ResMgr } from "../manager/ResMgr";
import { AudioMgr } from "../manager/AudioMgr";

export class AudioCommon{
    private static btn_clip = null;
    private static func_btn_end = null;

    public static setBtnClip(clip){
        if(isValid(AudioCommon.btn_clip ))return;
        clip.addRef();
        AudioCommon.btn_clip = clip;
    }

    public static playBtnSound(){
        if(isValid(AudioCommon.btn_clip))AudioMgr.playEffect(AudioCommon.btn_clip);
    }

    public static addBtnSound(){
        if(AudioCommon.func_btn_end)return;
        //@ts-ignore
        AudioCommon.func_btn_end = Button.prototype._onTouchEnded;
        //@ts-ignore
        Button.prototype._onTouchEnded = function(event:any){
            if (!this._interactable || !this.enabledInHierarchy)return;
            if (this._pressed) {
                EventHandler.emitEvents(this.clickEvents, event);
                this.node.emit("click", this);
            }
            this._pressed = false;
            this._updateState();
            AudioCommon.playBtnSound();
            if (event)event.propagationStopped = true;
        }
        ResMgr.loadWithTag("sound",'sound/button',AudioClip,(e,a)=>{
            if(e)return;
            AudioCommon.setBtnClip(a)
        })
    }
    public static recoverBtnSound(btn: Button) {
        //@ts-ignore
        btn._onTouchEnded = AudioCommon.func_btn_end;
    }

    public static changeBtnClip(clip:AudioClip){
        let old = AudioCommon.btn_clip
        AudioCommon.btn_clip = clip
        return old
    }
}
