import { Node, Prefab, instantiate } from "cc";

const nums = [1,10,100,1000,100000];
const Limits = [10000, 1000000, 5000000, 10000000 ]; //10K, 100K, 5M, 10M
const ChipGroups = [
    [100, 500, 1000, 5000, 10000],   //最大1M——对应0~10M之间
    [100, 500, 5000, 10000, 100000],  //最大50M——对应10M——1B之间
    [100, 1000, 10000, 100000, 1000000],  //最大100M——对应1B-5B之前
    [100, 5000, 100000, 1000000, 5000000],  //最大500M——对应5B-10B之间
    [100, 10000, 500000, 5000000, 10000000],  //最大1B——对应10B-50B之间
]

const Arr_Split = new Array(10).fill(0);
const Free_Times = [0, 6, 12, 18];
export class GameConst {
    public static Free_One = 100;
    
    public static splitClip(num: number) {
        let arr = [];
        let index = nums.length - 1;
        if(num >= nums[index] * 10){
            let one = num / 10;
            for(let i = 0 ; i < 10 ; i++){
                Arr_Split[i] = one;
            }
            return Arr_Split;
        }
        let min = nums[0];
        while (num >= min) {
            if (num < nums[index]) index--;
            else {
                num = num - nums[index];
                arr.push(nums[index])
            }
        }
        // console.log(arr);
        return arr;
    }
    public static getChipGroup(gold: number) {
        let index = 0;
        for (let i = Limits.length - 1; i >= 0; i--) {
            if (gold > Limits[i]) {
                index = i + 1;
                break;
            }
        }
        return ChipGroups[index];
    }

    public static getAddArr(arr_base: Array<any>, arr_offset: Array<any>) {
        let len = arr_base.length;
        let arr = new Array(len);
        for (let i = 0; i < len; i++) {
            arr[i] = [arr_base[i][0] + arr_offset[i][0],arr_base[i][1] + arr_offset[i][1]];
        }
        return arr;
    }
    public static getPosByOffset(base_pos:Array<number>, len:number, offset_x:number, offset_y: number){
        let arr = new Array(len);
        let base_x = base_pos[0] - (len - 1) * offset_x / 2;
        let base_y = base_pos[1] - (len - 1) * offset_y / 2;
        for(let i = 0 ; i < len ; i++){
            arr[i] = [base_x + offset_x * i, base_y + offset_y * i];
        }
        return arr;
    }
    public static getOffsetArr(len:number, center:number, offset:number, out?:Array<number>){
        let arr = out || new Array(len);
        let begin = center - (len - 1) * offset / 2;
        for(let i = 0 ; i < len ; i++){
            arr[i] = begin + offset * i;
        }
        return arr;
    }
    public static geneNodes(node:Node | Prefab, len:number, parent?:Node):Array<Node>{
        let arr = new Array();
        if(node instanceof Node){
            len--;
            arr.push(node);
            if(!parent){
                parent = node.parent;
            }
        }
        for(let i = 0 ; i < len ; i++){
            let new_node = instantiate(node) as Node;
            new_node.setParent(parent);
            arr.push(new_node);
        }
        return arr;
    }
    public static geneNewArrByIndexs(arr: Array<Array<number>>, indexs: Array<number>) {
        let new_arr = new Array(indexs.length);
        for (let i = 0; i < indexs.length; i++) {
            new_arr[i] = arr[indexs[i]].slice();
        }
        return new_arr;
    }
    public static getChairMap(count:number, m_chair:number){
        if(m_chair < 0 || m_chair >= count)m_chair = 0;
        let offset = count - m_chair;
        let arr = new Array(count);
        for(let i = 0 ; i < count ; i++){
            arr[i] = (i + offset) % count;
        }
        return arr;
    }
    public static getNextTime(last:number){
        let next = Free_Times[0];
        for(let i = 0 ; i < Free_Times.length - 1; i++){
            if(Free_Times[i] == last){
                next = Free_Times[i + 1];
            }
        }
        return next;
    }

}

//baloot card
const CardPrefix = ["FP_", "MH_", "HT_", "HS_"];
export {
    CardPrefix,
}

export const PlayerState = {
    UserStatus_Free: 1,    // 已登录
    UserStatus_Sit: 2,    
    UserStatus_Ready: 3,    
    UserStatus_Play: 4,    
    UserStatus_Watch: 5,    
    UserStatus_Offline: 6,    
}

export const GameEvent = {
    Setting: 0,
    Help: 1,
    Stand: 2,
    ChangeTable: 3,
    Exit: 4,
    Reset_Game_player: 5,
    Show_Game_player: 6,
    GameEnd: 7,
}

export const BacEvent = {
    Bet: 0,
    BatchBet: 1,
    GetZonePos: 2,
    GetPlayerPos: 3,
    ResidueTime: 4,
    ShowPlayer: 5,
    AddMineBet: 6,
    AddMineBetOnly: 7,
    BacEnd: 8
}