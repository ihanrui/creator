

import { DialogMgr } from "../manager/DialogMgr"
import { Loading } from "../../../begin/scripts/Loading";
import { director, Node } from "cc";
import { MsgInfo } from "../dialog/MsgBox";
import { Dialogs } from "../UiUrl";
import { NormalInfo, SceneStatus } from "../NormalInfo";
import { BadgeTip } from "../dialog/BadgeTip";
import { GiftTip } from "../dialog/GiftTip";
import { Configuration } from "../Configuration";
import { ShopType } from "../data/ShopBag";
import { i18nMgr } from "../manager/i18nMgr";

export class DialogCommon {
    static node_persist: Node = null;

    private static loading: Loading = null;
    static badgeTip: BadgeTip = null;
    static giftTip: GiftTip = null;

    static broadcast_pre = null;

    public static init(node_persist: Node) {
        if (DialogCommon.node_persist) return;
        DialogCommon.node_persist = node_persist;
        director.addPersistRootNode(node_persist);

        let node_loading = node_persist.getChildByName("loading");
        DialogCommon.loading = node_loading.getComponent(Loading);

        // let node_badgeTip = node_persist.getChildByName("badge_tip");
        // DialogCommon.badgeTip = node_badgeTip.getComponent(BadgeTip);
        // node_badgeTip.active = false;

        // let node_giftTip = node_persist.getChildByName("gift_tip");
        // DialogCommon.giftTip = node_giftTip.getComponent(GiftTip);
        // node_giftTip.active = false;
    }

    //@show 是否显示Loadin动画(false则只有阴影层)
    public static showLoading(show: boolean = true) {
        // console.trace();
        DialogCommon.loading.node.active = true;
        DialogCommon.loading.setNeedShow(show);
        // console.log("loading",DialogCommon.node_persist)
    }
    public static removeLoading() {
        // console.trace();
        DialogCommon.loading.node.active = false;
        // console.log("loading hide",DialogCommon.node_persist)
    }

    public static showTip(desc: string) {
        DialogMgr.showByResUrl(["Tip", "common/tip"], desc);
    }

    public static showBadgeTip() {
        if (DialogCommon.badgeTip) {
            DialogCommon.badgeTip.init()
        } else {
            BadgeTip.create(() => {
                DialogCommon.badgeTip.init()
            })
        }
    }

    public static showGiftQueue() {
        if (NormalInfo.sceneStatus != SceneStatus.Hall) return;

        if (NormalInfo.gift_tip_list.length > 0) {
            let gift = NormalInfo.gift_tip_list[0]
            if (gift.GiftId < 900005 && gift.GiftId > 900000) {
                if (DialogCommon.giftTip)
                    DialogCommon.giftTip.init()
                else {
                    GiftTip.create(() => {
                        DialogCommon.giftTip.init()
                    })
                }
            } else if (gift.GiftId > 800000 && gift.GiftId < 900000) {
                DialogMgr.showByResUrl(Dialogs.GiftVip)
            } else {
                console.warn("GiftTip err", gift.GiftId);
            }
        }

        if (NormalInfo.gift_anim_list.length > 0) {
            DialogMgr.showByResUrl(Dialogs.GiftAnim)
        }
    }

    public static showMsgBox1(desc: string, func: Function = null, btn_desc: string = "") {
        let data = new MsgInfo(desc, 1);
        data.func_sure = func;
        data.txt_sure = btn_desc;
        data.show_close = true;
        DialogMgr.showByResUrl(Dialogs.MsgBox, data)
    }
    public static showMsgBox2(desc: string, func_sure: Function, func_cancel: Function = null, btn_desc: string = "") {
        let data = new MsgInfo(desc, 2);
        data.func_sure = func_sure;
        data.func_cancel = func_cancel;
        data.txt_sure = btn_desc;
        data.show_close = true;
        DialogMgr.showByResUrl(Dialogs.MsgBox, data)
    }
    public static log(data: any) {
        DialogMgr.showByResUrl(["LogPanel", "common/log"], data);
    }
    public static showGift(data: any) {
        NormalInfo.datas_gift.push(data);
        DialogMgr.showByResUrl(Dialogs.ShowGiftList);
    }

    public static addPersistNode(node: Node) {
        // console.log("addPersistNode", node)
        node.setParent(DialogCommon.node_persist);
        DialogCommon.loading.node.setSiblingIndex(-1);
    }
    public static getPersistNode(nodeName: string): Node {
        return DialogCommon.node_persist.getChildByName(nodeName);
    }
    public static showLack(data:any){
        const first_data = Configuration.getShopByType(ShopType.FirstCharge);
		if (first_data.length) {
            DialogMgr.showByResUrl(Dialogs.Recharge, data);
            return;
        }
        DialogMgr.showByResUrl(Dialogs.RechGold, data);
    }
    public static showLackTip(data:any){
        let key = data.is_gold ? "sh_gold_ins" : "sh_diamond_ins";
        DialogCommon.showTip(i18nMgr.getString(key));
    }
}
