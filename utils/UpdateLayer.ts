import { _decorator, Component, Node, Sprite, Label, director, sys, tween, native, assetManager, JsonAsset, Asset, dynamicAtlasManager, AssetManager, SpriteAtlas, Material, macro, Prefab, sp, view, ResolutionPolicy, SpriteFrame } from 'cc';
import { BeginInfo } from './BeginInfo';
import { HotFix, UpgradeState } from './HotFix';

const { ccclass, property } = _decorator;

const JsbClass = "org/cocos2dx/jsb/JsbClass";
const JsbClass_IOS = "JavaScripNative"
@ccclass
export class UpdateLayer extends Component {
    @property(Sprite)
    public sp_bg: Sprite = null;

    @property(Label)
    public lb_desc: Label = null;
    @property(Node)
    public node_progress: Node = null;
    @property(Sprite)
    public sp_progress: Sprite = null;
    @property(Label)
    public lb_progress: Label = null;
    @property(Label)
    public lb_total: Label = null;

    @property(Node)
    public node_persist: Node = null;
    // @property(Node)
    // public btnCancel: Node = null;

    @property(Asset)
    public manifest: Asset = null

    private has_review = false;
    private bg_index = 0;
    private bundle_main: AssetManager.Bundle = null;

    protected onLoad() {
        dynamicAtlasManager.enabled = false;
        let size = view.getVisibleSize();
        let des_size = view.getDesignResolutionSize();
        if (size.height / size.width < des_size.height / des_size.width) {
            BeginInfo.show_all = true;
            view.setResolutionPolicy(ResolutionPolicy.SHOW_ALL);
        }
        macro.CLEANUP_IMAGE_CACHE = true;
        this.loadBundle();
        if (!sys.isNative || sys.platform == sys.Platform.WIN32) {
            this.has_review = true;
            return;
        }
        let data = `VersionCode=${this.getVersionCode()}&PartnerId=${this.getPartnerID()}`;
        getData(HotFix.ReviewUrl, data, (content: any) => {
            this.has_review = true;
            BeginInfo.is_review = !!content.length;
            if (BeginInfo.is_review) {
                if(sys.platform == sys.Platform.IOS)this.bg_index = 1;
                this.loadRes();
            }
            else {
                this.checkUpdate();
            }
            this.showBg();
        })

    }

    public getJsbClass(): string {
        if (sys.platform == sys.Platform.IOS) {
            return JsbClass_IOS;
        } else {
            return JsbClass;
        }
    }

    private getPartnerID(): string {
        console.log("getPartnerID")
        if (sys.isNative) {
            if (sys.platform == sys.Platform.IOS) {
                let real_args = [this.getJsbClass(), "getPartnerID"]
                return native.reflection.callStaticMethod.apply(native.reflection, real_args)
            }
            return native.reflection.callStaticMethod(this.getJsbClass(), "getPartnerID", "()Ljava/lang/String;");
        }
        return "30001";
    }
    private getVersionCode() {
        if (sys.isNative) {
            if (sys.platform == sys.Platform.IOS) {
                let real_args = [this.getJsbClass(), "getVersionCode"]
                return native.reflection.callStaticMethod.apply(native.reflection, real_args)
            }
            return native.reflection.callStaticMethod(this.getJsbClass(), "getVersionCode", "()Ljava/lang/String;");
        }
        return "100";
    }

    protected checkUpdate() {
        HotFix._localAssets = this.manifest;
        HotFix.checkUpgrade(this.onState, this, "");
    }

    public onState(state:number, param:number, remoteVersion) {
        console.log("onState " + state);
        if (state == UpgradeState.Progress) {
            if (param == null || param == undefined || isNaN(param))
                param = 0;
            // console.log("onProgress " + param);
            this.refreshProgress(param);
            return;
        }
        else if (state == UpgradeState.StartDownload) {
            this.lb_desc.string = "update...";
            let m = param / 1024 / 1024;
            this.lb_total.string = `${m.toFixed(2)}M` + " تحديث "
            return;
        }
        this.loadRes();
    }
    public showBg(){
        if(this.has_review && this.bundle_main){
            this.bundle_main.load(`texture/bg/login${this.bg_index}/spriteFrame`, SpriteFrame, (err:any, sf:SpriteFrame)=>{
                if(err)return;
                this.sp_bg.spriteFrame = sf;
            })
        }
    }
    private loadBundle(){
        assetManager.loadBundle("master", (err: Error, bundle: AssetManager.Bundle) => {
            if (err) {
                console.log("loadRes err=", err.message, err.name)
            }
            this.bundle_main = bundle;
            this.showBg();
            this.loadRes();
        })
    }
    private loadRes() {
        if(!this.bundle_main || !this.has_review)return;
        if(HotFix._localAssets)return;

        // this.node_progressBar.active = true;
        this.lb_desc.string = "load resource...";
        this.loadPlists(this.bundle_main);
    }
    private refreshProgress(progress:number){
        if(this.sp_progress.fillRange > progress)return;
        this.sp_progress.fillRange = progress;
        this.lb_progress.string = Math.floor(progress * 10000) / 100 + "%";
    }
    private loadPlists(bundle: AssetManager.Bundle) {
        let paths = ["plist/head", "plist/common"];
        bundle.load(paths, SpriteAtlas, (finished: number, total: number, item: any) => {
            this.refreshProgress(finished / total * 0.3);
        }, (err, p) => {
            this.refreshProgress(0.3);
            BeginInfo.all_atlas = p;
            this.loadSpins(bundle);
        })
    }
    private loadSpins(bundle: AssetManager.Bundle) {
        bundle.load("spine/emoji2", sp.SkeletonData, (finished: number, total: number, item: any) => {
            this.refreshProgress(0.3 + finished / total * 0.1);
        }, (err, p) => {
            this.refreshProgress(0.4);
            BeginInfo.spine_brow = p;
            this.loadMats(bundle);
        })
    }
    private loadMats(bundle: AssetManager.Bundle) {
        bundle.load("shader/RoundSprite", Material, (err, p) => {
            this.refreshProgress(0.5);
            BeginInfo.mat_round = p;
            this.loadPages(bundle);
        })

        bundle.load("shader/CornerSprite", Material, (err, p) => {
            BeginInfo.mat_comer = p;
        })
    }
    private loadPages(bundle: AssetManager.Bundle) {
        let paths = ["", "prefab/lobby/page1"];
        let bg_path = "";
        if (BeginInfo.is_review && sys.platform == sys.Platform.IOS) {
            bg_path = "texture/bg/lobby_audit";
            paths[0] = "prefab/lobby/page99";
        }
        else {
            bg_path = "texture/bg/lobby_bg";
            paths[0] = "prefab/lobby/page0";
        }
        bundle.load(paths, Prefab, (finished: number, total: number, item: any) => {
            this.refreshProgress(0.5 + finished / total * 0.2);
        }, (err, p) => {
            this.refreshProgress(0.7);
            p.forEach(item => item.addRef());
            BeginInfo.pages_lobby = p;
            this.loadChatItems(bundle);
        })
        bundle.load(bg_path + "/spriteFrame", SpriteFrame, (err, p) => {
            BeginInfo.sf_lobbybg = p.addRef();
        })
    }
    private loadChatItems(bundle: AssetManager.Bundle) {
        let pix = "prefab/ui/chatWindow/item/";
        let paths = ["item_text", "item_spine", "item_gift", "roomitem", "shareRoomItem"];
        let load_count = 0;
        BeginInfo.chat_items = new Array()
        for (let i = 0; i < paths.length; i++) {
            if (i == 3) pix = "prefab/ui/customRooms/friend/";
            else if (i == 4) pix = "prefab/ui/audioRoom/item/";
            bundle.load(pix + paths[i], Prefab, (err, p) => {
                BeginInfo.chat_items.push(p.addRef());
                load_count++;
                this.refreshProgress(0.7 + load_count / paths.length * 0.3);
                if (load_count == paths.length) {
                    this.lb_desc.string = "enter game";
                    BeginInfo.func_begin(this.node_persist);
                }
            })
        }
    }
    // private loadLogin(bundle: AssetManager.Bundle) {
    //     bundle.loadScene("login", (err, scene) => {
    //         this.progressBar.fillRange = 1;
    //         director.runScene(scene);
    //     })
    // }
    // onTimeOut(){        
    //     this.btnCancel.active = true;
    // }

    // onBtnCancel() {
        // let language = localStorage.getItem('language');
        // if (!language.length) language = "en";
        // let str = "";
        // if (language == "en") {
        //     //str = `The current game version is ${Domain.Version}, and the latest game version of the game is ${this.new_version}. If skipping the update will bring a bad gaming experience, please cancel the update?`;
        //     str = `إصدار اللعبة الحالي هو ${Domain.Version} ، وأحدث إصدار للعبة هو ${this.new_version}. إذا كان تخطي التحديث سيؤدي إلى تجربة لعب سيئة ، فيرجى إلغاء التحديث؟`
        // } else {
        //     //str = `Versi gim saat ini adalah ${Domain.Version}, dan versi gim terbaru adalah ${this.new_version}. Jika melewatkan pembaruan akan membawa pengalaman bermain yang buruk, batalkan pembaruan?`
        //     str = `"الإصدار الحالي للعبة هو ${Domain.Version} ، وأحدث إصدار من اللعبة هو ${this.new_version}. إذا كان تخطي أحد التحديثات سيؤدي إلى تجربة لعب سيئة ، فهل تريد إلغاء التحديث؟"`
        // }
        // DialogCommon.showMsgBox2(str, function () {
        //     HotFix.stopUpgrade();
        // })
    // }

    // onBtnRecover() {
    //     HotFix.removeUpdateFiles();
    // }
}

function getData(url: string, data: any, callback: Function) {
    var xhr = new XMLHttpRequest();
    // Simple events
    ['loadstart', 'abort', 'error', 'load', 'loadend', 'timeout'].forEach(function (eventname) {
        xhr["on" + eventname] = function () {
            console.log("getData event = " + eventname);
            if (eventname == 'abort' || eventname == 'error' || eventname == 'timeout') {
                if (callback) callback("11");
            }
        };
    });

    // Special event
    xhr.onreadystatechange = function () {
        console.log("getData code = " + xhr.readyState + ",status = " + xhr.status);
        if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
            if (callback) callback(xhr.responseText);
        }
    };
    xhr.timeout = 5000;//5 seconds for timeout
    console.log(`getData url = " + ${url} + ",data = `, data);

    xhr.open("POST", url, true);
    //set Content-type "text/plain" to post ArrayBuffer or ArrayBufferView
    // xhr.setRequestHeader("Content-Type","text/plain");
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    // Uint8Array is an ArrayBufferView
    //xhr.send(new Uint8Array([1,2,3,4,5]));
    xhr.send(data);
}