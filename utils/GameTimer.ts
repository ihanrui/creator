import { _decorator } from 'cc';
import { GameInvoker } from "./GameInvoker";



export class GameTimer {
    public is_running = false;

    private interval = 0;
    private elapsed = 0;
    private func_index = -1;

    public init(interval: number, func_index: number) {
        this.is_running = true;
        this.interval = interval;
        this.elapsed = 0;
        this.func_index = func_index;
    }

    public add(time: number) {
        if (!this.is_running) return;
        this.elapsed += time;
        while (this.elapsed > this.interval) {
            GameInvoker.call(this.func_index);
            this.elapsed -= this.interval;
        }
    }

    public stop(force: boolean, flag: number) {
        if (force || this.func_index == flag) {
            this.elapsed = 0
            this.is_running = false;
        }
    }
}
