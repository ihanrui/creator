import { native } from "cc";
import { JSB } from "cc/env";

export class UtilSava {
    public static saveImg(name:string, width:number, height:number, array_buffer:Uint8Array){
        if(JSB){
            let new_buffer = UtilSava.flipImgBuffer(array_buffer, width, height);
            UtilSava.saveImgNative(name, width, height, new_buffer);
        }
        else UtilSava.saveImgBrowser(name, width, height, array_buffer);
    }
    public static saveImgBrowser(name:string, width:number, height:number, array_buffer:Uint8Array){
        console.time("buffer spend");
        let canvas = document.createElement("canvas");
        canvas.width = width;
        canvas.height = height;
        let ctx = canvas.getContext("2d");

        let row_bytes = width * 4;
        let read_row = 0;
        let image_data = null;
        let start = 0;
        for(let i = 0 ; i < height; i++){
            read_row = height - 1 - i;
            image_data = ctx.createImageData(width, 1);
            start = row_bytes * read_row;
            for(let j = 0 ; j < row_bytes; j++){
                image_data.data[j] = array_buffer[start + j];
            }
            ctx.putImageData(image_data, 0, i);
        }
        let img = new Image();
        img.onload = function(){
            ctx.drawImage(img, 0, 0);
        }
        console.timeEnd("buffer spend");
        // img.src = "image.jgp";
        let data_url = canvas.toDataURL();
        let link = document.createElement("a");
        link.href = data_url;
        link.download = name;
        document.body.append(link);
        link.click();
        document.body.removeChild(link);
    }
    public static flipImgBuffer(buffer:Uint8Array, width:number, height:number){
        console.time("flip buffer");
        let new_buffer = new Uint8Array(buffer.length);
        let temp_buffer = null;
        let cur = 0;
        let row_bytes = width * 4;
        for(let i = height - 1 ; i >= 0 ; i--){
            temp_buffer = buffer.subarray(i * row_bytes, i * row_bytes + row_bytes);
            new_buffer.set(temp_buffer, cur);
            cur += row_bytes;
        }
        console.timeEnd("flip buffer");
        return new_buffer;
    }
    public static saveImgNative(name:string, width:number, height:number, array_buffer:Uint8Array){
        let file_path = native.fileUtils.getWritablePath() + name;
        native.saveImageData(array_buffer, width, height, file_path).then(()=>{
            console.log(file_path);
        })
    }
}
