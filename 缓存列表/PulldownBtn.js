

cc.Class({
    extends: cc.Component,

    properties: {
        node_arrow: cc.Node,
        node_show: cc.Node,
        
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.is_show = true;
        this.node.on("click" , this.onClick , this);
        if(this.node_show.getComponent("LayoutPro")){
            this.layout_pro = this.node_show.getComponent("LayoutPro");
        }
    },

    setCallfunc(func){
        this.func = func;
    },

    setState(state , no_call){
        // if(this.is_show == state)return;
        this.is_show = state;
        if(state){
            this.node_arrow.angle = 0;
            if(!no_call){
                //有layoutpro得话就立马把坐标改到自己得节点下面,为下一步放里面填充东西做准备
                if(this.layout_pro){
                    this.node_show.y = this.node.y - this.node.height * this.node.anchorY;
                }else{
                    this.node_show.active = true;
                }
            }
        }else{
            this.node_arrow.angle = 90;
            if(!no_call){
                //有layoutpro的话就调清空
                if(this.layout_pro && this.node_show.active){
                    this.layout_pro.clear();
                }else{
                    this.node_show.active = false;
                }
            }
        }
    },

    getState(){
        return this.is_show;
    },

    onClick(){
        this.setState(!this.is_show);
        if(this.func){
            this.func(this.is_show);
        }
    },

    start () {

    },

    // update (dt) {},
});
