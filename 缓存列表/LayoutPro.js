
// const LayoutItemPro = require("LayoutItemPro");

cc.Class({
    extends: cc.Component,

    properties: {
        // scroll: require("ScrollViewPro"),
        m_type: 0,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.item_list = [];
    },

    setScroll(scroll , need_calc_parent){
        this.scroll = scroll;
        this.content = this.scroll.content;
        this.need_calc_parent = need_calc_parent;
        // if(need_calc_parent){
        //     this.parent = this.node.parent;
        // }
    },

    onEnable(){
        this.node.on(cc.Node.EventType.POSITION_CHANGED, this.onPositionChange, this)
    },

    onDisable(){
        this.node.off(cc.Node.EventType.POSITION_CHANGED, this.onPositionChange, this)
    },

    onPositionChange(){
        if(!this.have_init)return;
        this.refreshItem({x: 0,y: 1});
    },

    clear(){
        this.clearMyItems();
        this.data = [];
        this.node.height = 0;
        this.have_init = false;
    },

    initData(data,p1,force_refresh){
        this.have_init = false;
        this.data = data;
        this.p1 = p1;
        if(!this.node_height){
            let node = this.scroll.getCacheByType(this.m_type);
            this.node_height = node.height;
            this.scroll.pushCacheByType(this.m_type , node);
        }
        this.node.height = this.node_height * data.length;
        if(!this.need_calc_parent || force_refresh){
            this.refreshData();
        }
    },

    getChildByIndex(index){
        if(index >= this.begin_index && index <= this.end_index){
            let new_index = index - this.begin_index;
            return this.item_list[new_index];
        }
        return null;
    },

    refreshData(){
        if(!this.data || this.data.length <= 0)return;
        let top = this.scroll._topBoundary;
        let m_top = this.getTopPos();
        if(m_top < this.scroll._bottomBoundary){
            this.clearMyItems();
            this.have_init = true;
            return;
            // this.begin_index = 0;
            // this.end_index = this.data.length >= 2 ? 1 : 0;
            // this.end_index = 0;
        }else if(m_top - this.node.height > top){
            this.clearMyItems();
            this.have_init = true;
            return;
            // this.end_index = this.begin_index = this.data.length - 1;
            // if(this.data.length >= 2)this.begin_index--;
        }else{
            let show_top = top < m_top ? top - m_top : 0;
            if(show_top == 0){
                this.begin_index = 0;
            }else{
                this.begin_index = Math.floor(-show_top / this.node_height);
            }
            // let num = Math.ceil(top - this.scroll._bottomBoundary / this.node_height) + 1;
            let num = Math.ceil((m_top - this.scroll._bottomBoundary) / this.node_height) + 1;
            if(this.begin_index + num > this.data.length){
                num = this.data.length - this.begin_index;
            }
            this.end_index = this.begin_index + num - 1;
        }
        this.showItems();
        this.have_init = true;
    },

    showItems(){
        this.clearMyItems(true);
        // cc.log("begin:",this.begin_index);
        // cc.log("end:",this.end_index);
        for(let i = this.begin_index ; i <= this.end_index ; i++){
            let node = this.scroll.getCacheByType(this.m_type);
            node.itemComponent.init(this.data[i] , i , this.p1);
            this.item_list.push(node);
            node.parent = this.node;
            node.y = -this.node_height * i;
        }
    },
    geneOneNode(index){
        let node = this.scroll.getCacheByType(this.m_type);
        node.parent = this.node;
        if(this.data[index]){
            node.itemComponent.init(this.data[index] , index , this.p1);
        }
        node.y = -this.node_height * index;
        return node;
    },

    // refreshData(){
    //     if(!this.data || this.data.length <= 0)return;
    //     let m_top = this.getTopPos();
    //     let top = this.scroll._topBoundary;
    //     let show_top = top < m_top ? top - m_top : 0;
    //     let show_bottom = show_top -this.scroll.node.height;;
    //     if(show_bottom < -this.node.height)show_bottom = -this.node.height;
    //     this.showItems(show_top);
    //     this.have_init = true;
    // },

    // showItems(show_top , bottom_top){
    //     this.clearMyItems(true);
    //     if(show_top == 0){
    //         this.begin_index = 0;
    //     }else{
    //         this.begin_index = Math.floor(-show_top / this.node_height);
    //     }
    //     let num = Math.ceil(this.scroll.node.height / this.node_height) + 1;
    //     if(this.begin_index + num > this.data.length){
    //         num = this.data.length - this.begin_index;
    //     }
    //     this.end_index = this.begin_index + num - 1;
    //     let index = 0;
    //     this.item_list = [];
    //     for(let i = 0 ; i < num ; i++){
    //         index = this.begin_index + i;
    //         let node = this.scroll.getCacheByType(this.m_type);
    //         node.itemComponent.init(this.data[index] , index , this.p1);
    //         this.item_list.push(node);
    //         node.parent = this.node;
    //         node.y = -this.node_height * index;
    //     }
    //     cc.log("this.type:",this.m_type , ":",this.node.children.length)
    // },

    refreshItem(offset){
        if(!this.have_init)return;
        if(this.data.length <= 0)return;
        //向上
        let top_pos = this.getTopPos();
        let bottom_pos = top_pos - this.node.height;
        let show_height = 0;
        let count = 0;
        if(offset.y > 0){
            if(this.item_list.length <= 0){
                if(bottom_pos > this.scroll._topBoundary)return;
                show_height = top_pos - this.scroll._bottomBoundary;
                if(show_height > 0){
                    this.begin_index = 0;
                    count = Math.ceil(show_height / this.node_height);
                    // count = Math.max(2 , Math.ceil(show_height / this.node_height));
                    this.end_index = count < this.data.length ? count-1 : this.data.length-1;
                    // this.end_index = this.data.length > 1 ? 1 : 0;
                    this.showItems();
                }
            }else{
                if(this.end_index < this.data.length - 1){
                    let len = this.item_list.length;
                    show_height = top_pos + this.item_list[len-1].y - this.scroll._bottomBoundary;
                    if(show_height > 0){//len > 1 && 
                        count = Math.ceil(show_height / this.node_height);
                        if(this.end_index + count > this.data.length){
                            count = this.data.length - this.end_index - 1;
                        }
                        for(let i = 0 ; i < count ; i++){
                            this.end_index++;
                            let node = this.geneOneNode(this.end_index);
                            this.item_list.push(node);
                        }
                    }
                }
            }
            if(this.item_list.length > 0 && top_pos + this.item_list[0].y - this.node_height > this.scroll._topBoundary){// 
                let node = this.item_list.shift();
                this.scroll.pushCacheByType(this.m_type , node);
                this.begin_index++;
            }
        }
        //向下
        else if(offset.y < 0){
            if(this.item_list.length <= 0){
                if(this.scroll._bottomBoundary > top_pos)return;
                show_height = this.scroll._topBoundary - bottom_pos;
                if(show_height > 0){
                    this.end_index = this.data.length - 1;
                    count = Math.ceil(show_height / this.node_height);
                    this.begin_index = count < this.data.length ? this.end_index-count + 1 : 0;
                    // if(this.data.length > 1){
                    //     this.begin_index = this.data.length - 2;
                    //     this.end_index = this.begin_index + 1;
                    // }else{
                    //     this.begin_index = this.end_index = this.data.length - 1;
                    // }
                    this.showItems();
                }
            }else{
                if(this.begin_index > 0){
                    show_height = this.scroll._topBoundary - top_pos - this.item_list[0].y;// - this.node_height 
                    if(show_height > 0){
                        count =  Math.ceil(show_height / this.node_height);
                        if(this.begin_index - count < 0){
                            count = this.begin_index;
                        }
                        for(let i = 0 ; i < count ; i++){
                            this.begin_index--;
                            let node = this.geneOneNode(this.begin_index);
                            this.item_list.unshift(node);
                        }
                    }
                    // if(this.item_list.length > 0 && this.getTopPos() + this.item_list[0].y - this.node_height < this.scroll._topBoundary){
                    //     this.begin_index--;
                    //     let node = this.geneOneNode(this.begin_index);
                    //     this.item_list.unshift(node);
                    // }
                }
                let len = this.item_list.length;
                if(top_pos + this.item_list[len-1].y < this.scroll._bottomBoundary){//len > 2 && 
                    let node = this.item_list.pop();
                    this.scroll.pushCacheByType(this.m_type , node);
                    this.end_index--;
                }
            }
        }
    },


    getTopPos(){
        if(this.need_calc_parent){
            return this.content.y + this.node.y + this.node.parent.y;
        }else{
            return this.content.y + this.node.y;
        }
    },

    clearMyItems(force){
        if(force){
            if(this.node.children.length > 0){
                let childrens = this.node.children;
                for(let i = childrens.length - 1 ; i >= 0 ; i--){
                    this.scroll.pushCacheByType(this.m_type , childrens[i]);
                }
            }
        }else{
            if(this.item_list.length > 0){
                for(let i = this.item_list.length - 1 ; i >= 0 ; i--){
                    this.scroll.pushCacheByType(this.m_type , this.item_list[i]);
                }
            }
        }
        this.item_list = [];
    },

    // start () {

    // },

    // update (dt) {
        // if(!this.content)return;
        // //-198 bottom: -1058
        // let top = this.scroll._topBoundary;
        // let bottom = this.scroll.node.height;
        // let m_top = this.getTopPos();
        // let m_bottom = m_top - this.node.height;
        // let show_top = top < m_top ? top - m_top : 0;
        // let show_bottom = show_top -bottom;
        // if(show_bottom < -this.node.height)show_bottom = -this.node.height;
        // cc.log("top:",show_top , "bottom:",show_bottom);
    // },
});
