import { App } from "../App";
import { I18Lb } from "../components/I18Lb";



export class PanelHelper {

    public static getCom(node: cc.Node, path: string[], type = null): any {
        for (let i = 0, len = path.length; i < len; i++) {
            node = node.getChildByName(path[i]);
        }
        return type ? node.getComponent(type) : node;
    }
    public static getAllNeedCom(sc: any, node: cc.Node, recursive: boolean = false) {
        let children = node.children;
        for (let i = 0, len = children.length; i < len; i++) {
            getComByName(sc, children[i]);
            if (recursive) {
                PanelHelper.getAllNeedCom(sc, children[i], true)
            }
        }
    }
    public static getArrCom(node: cc.Node, name: string, count: number, type = null) {
        let arr = new Array(count);
        let children = node.children;
        let cur_connt = 0;
        for (let i = 0; i < node.childrenCount; i++) {
            if (children[i].name.startsWith(name)) {
                cur_connt++;
                let index = parseInt(children[i].name.slice(name.length));
                arr[index] = type ? children[i].getComponent(type) : children[i];
                if (cur_connt >= count) break;
            }
        }
        return arr;
    }
    public static addBtnEvent(btn_node:cc.Node , func:Function, target:any){
        let btn = btn_node.getComponent(cc.Button);
        if(!btn){
            btn = btn_node.addComponent(cc.Button);
            btn.transition = cc.Button.Transition.SCALE;
            btn.target = btn_node;
            btn.zoomScale=1.05;
        }
        btn_node.on("click", func, target);
        return btn;
    }
    public static addBtnEvent2(btn_node:cc.Node, func:string, target:any, param:string){
        let btn = btn_node.getComponent(cc.Button);
        if(!btn){
            btn = btn_node.addComponent(cc.Button);
            btn.transition = cc.Button.Transition.SCALE;
            btn.target = btn_node;
            btn.zoomScale=1.05;
        }
        let handler = new cc.Component.EventHandler();
        handler.target = target.node;
        handler.component = cc.js.getClassName(target);
        handler.handler = func;
        handler.customEventData = param;
        btn.clickEvents.push(handler);
        return btn;
    }
    public static clearBtnEvent(node:cc.Node, remove_btn:boolean){
        node.off("click");
        let btn = node.getComponent(cc.Button);
        if(btn){
            btn.clickEvents.length = 0;
            if(remove_btn)node.removeComponent(cc.Button);
        }
    }
    public static loadSpine(spine:sp.Skeleton, path:string, name:string, loop = false){
        App.main_bundle.load(path, sp.SkeletonData, function(error:Error, data:sp.SkeletonData){
            if(error){
                console.log(error.message);
                return;
            }
            if(!cc.isValid(spine))return;
            spine.skeletonData = data;
            if(name.length)spine.setAnimation(0, name, loop);
            else spine.node.active = false;
        })
    }
    public static setNodeGray(node_or_btn:cc.Node | cc.Button, state:boolean, recursive = false){
        let node = node_or_btn;
        let btn = null;
        if(node_or_btn instanceof cc.Button){
            btn = node_or_btn;
            node = node_or_btn.node;
        }
        else btn = node.getComponent(cc.Button);
        if(btn)btn.interactable = state;
        let mat_name = state ? "2d-sprite" : "2d-gray-sprite";
        let mat = cc.Material.getBuiltinMaterial(mat_name);
        let func = function(node0:any, recu:boolean){
            let com_rendener = node0.getComponent(cc.RenderComponent);
            if(com_rendener && !com_rendener.setAnimation)com_rendener.setMaterial(0, mat);
            if(recu){
                let children = node0.children;
                for(let i = 0 ; i < children.length ; i++)func(children[i], true);
            }
        }
        func(node, recursive);
    }
    
}
function getComByName(sc: any, node: cc.Node) {
    let name = node.name;
    switch (name[0]) {
        case "a":
            if (name.startsWith("auto_btn_")){
                let click_name = name.slice(10);
                let func_name = "on"+name[9].toUpperCase()+click_name+"Click";
                if(sc[func_name])PanelHelper.addBtnEvent(node, sc[func_name], sc);
            }
            break;
        case "b":
            if (name.startsWith("btn_"))sc[name] = node.getComponent(cc.Button);
            break;
        case "e":
            if (name.startsWith("edit_"))sc[name] = node.getComponent(cc.EditBox);
            
            break;
        case "i":
            if (name.startsWith("ilb_"))sc[name] = node.getComponent(I18Lb);
            break;
        case "l":
            if (name.startsWith("lb_")) sc[name] = node.getComponent(cc.Label);
            break;
        case "n":
            if (name.startsWith("node_")) sc[name] = node;
            break;
        case "r":
            if (name.startsWith("rich_"))sc[name] = node.getComponent(cc.RichText);
            break;
        case "s":
            if (name.startsWith("sp_")) sc[name] = node.getComponent(cc.Sprite);
            else if (name.startsWith("sc_")) {
                name = name.slice(3);
                //@ts-ignore
                sc[name] = node._components[0];
            }
            else if (name.startsWith("spine_")) sc[name] = node.getComponent(sp.Skeleton);
            break;
        case "t":
            if (name.startsWith("toggle_")) sc[name] = node.getComponent(cc.Toggle);
            break;
    }


}
