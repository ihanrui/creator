import { Game, System, director, game, isValid, js } from "cc";

export class TimeInfo{
    public cur_time = 0;
    public func: Function;
    public target: any;
    constructor(time:number, func:Function, target:any){
        this.cur_time = time;
        this.func = func;
        this.target = target;
    }
    public minusTime(second:number){
        if(!isValid(this.target))return true;
        this.cur_time -= second;
        return this.func.call(this.target, this.cur_time);
    }
}
export class TimeInfoPrecise extends TimeInfo{
    private int_time = 0;
    constructor(time:number, func:Function, target:any){
        super(time, func, target);
        this.int_time = time;
    }
    public minusTime(dt:number){
        if(!isValid(this.target))return true;
        this.cur_time -= dt;
        if(this.int_time > 1 + this.cur_time){
            this.int_time = Math.ceil(this.cur_time);
            return this.func.call(this.target, this.int_time);
        }
        return false;
    }
    public updateTime(time:number){
        this.int_time = this.cur_time = time;
        return this.func.call(this.target, this.cur_time);
    }
}


class TimeMgr extends System {
    private enable = false;
    private hide_time = 0;
    private cur_time = 0;
    private infos: Array<TimeInfo> = [];
    private infos_precise: Array<TimeInfoPrecise> = [];

    constructor(){
        super();
        game.on(Game.EVENT_HIDE, this.onHide, this);
        game.on(Game.EVENT_SHOW, this.onShow, this);
    }
    public addTimeInfo(time:number, func:Function, target:any){
        let info = this.getInfo(func, target);
        if(info){
            info.cur_time = time;
            info.minusTime(0);
        }
        info = new TimeInfo(time, func, target);
        if(!info.minusTime(0))this.infos.push(info);
        if(!this.enable){
            this.enable = true;
            director.registerSystem("timemgr", this, 0);
        }
    }
    public addInfo(info:TimeInfo, is_precise: boolean){
        let arr = is_precise ? this.infos_precise : this.infos;
        arr.push(info);
        if(!this.enable){
            this.enable = true;
            director.registerSystem("timemgr", this, 0);
        }
    }
    public getInfo(func:Function, target: any){
        for(let i = this.infos.length - 1; i >= 0 ; i--){
            if(this.infos[i].func == func && this.infos[i].target == target)return this.infos[i];
        }
        return null;
    }
    public removeByTarget(target:any, is_precise: boolean){
        let arr = is_precise ? this.infos_precise : this.infos;
        for(let i = arr.length - 1; i >= 0 ; i--){
            if(arr[i].target == target)arr.splice(i, 1);
        }
    }
    public removeInfo(info:TimeInfo, is_precise: boolean){
        let arr = is_precise ? this.infos_precise : this.infos;
        let index = arr.indexOf(info);
        if(index > -1)arr.splice(index, 1);
    }
    public update(dt: number): void {
        this.cur_time += dt;
        for(let i = 0; i < this.infos_precise.length; i++){
            if(this.infos_precise[i].minusTime(dt))this.infos_precise.splice(i, 1);
        }
        if(this.cur_time < 1)return;
        let mul = Math.floor(this.cur_time);
        this.cur_time -= mul;
        for(let i = this.infos.length - 1; i >= 0 ; i--){
            if(this.infos[i].minusTime(mul))this.infos.splice(i, 1);
        }
        if(this.infos.length + this.infos_precise.length == 0){
            this.enable = false;
            director.unregisterSystem(this);
        }
    }
    private onHide(){
        this.hide_time = new Date().getTime();
    }
    private onShow(){
        if(this.hide_time == 0)return;
        let offset = new Date().getTime() - this.hide_time;
        if(offset > 0){
            console.log("进入后台时长(ms):", offset);
            for(let i = 0; i < this.infos_precise.length; i++){
                if(this.infos_precise[i].minusTime(offset))this.infos_precise.splice(i, 1);
            }
            this.cur_time += (offset / 1000);
        }
        this.hide_time = 0;
    }
    public log(){
        console.log("times:",this.infos);
        console.log(this.infos_precise);
    }
    
}

export const timeMgr = new TimeMgr();
