import { sys , Node, ScrollView, Vec2, Vec3, Intersection2D, PolygonCollider2D, view} from "cc";

const Empty_Func = function(){};
var old_dispatch = null;
var startTime=0;
var intervalTime=200;

var touch_fun = null;
function changeDispatch(){
    old_dispatch = Node.prototype.dispatchEvent;
    Node.prototype.dispatchEvent = function (event) {
        if(touch_fun)touch_fun(event);
        if(event.type == "touch-start"){
            let nowTime = sys.now();
            if(startTime > 0 && (nowTime-startTime) < intervalTime){
                console.log("%cclick too fast", "color:red");
                console.log(this);
                return;
            }
            startTime = nowTime;
        }
        old_dispatch.call(this, event);
    };
}

function changeSafeRect(){
    let old_func = sys.getSafeAreaRect;
    // console.log(old_func());
    sys.getSafeAreaRect = function(){
        let rect = old_func();
        // rect.height -= 200;
        // rect.y += 100;
        let y = rect.y;
        if(y > 0){
            rect.height += y;
            rect.y = 0;
        }
        ExtraMgr.fill_height = rect.height;
        let all_height = view.getVisibleSize().height;
        ExtraMgr.fill_bottom = rect.y - (all_height - rect.height) / 2;
        return rect;
    }
}
const V3_World = new Vec3(0);
const V3_Local = new Vec3(0);
const V2_Local = new Vec2(0);
function hitTest(screenPoint: Vec2){
    V3_World.x = screenPoint.x;
    V3_World.y = screenPoint.y;
    const cameras = this._getRenderScene().cameras;
    for (let i = 0; i < cameras.length; i++) {
        const camera = cameras[i];
        if (!(camera.visibility & this.node.layer) || (camera.window && !camera.window.swapchain)) continue;
        Vec3.set(V3_World, screenPoint.x, screenPoint.y, 0);  // vec3 screen pos
        camera.screenToWorld(V3_World, V3_World);
        this.convertToNodeSpaceAR(V3_World, V3_Local);
        V2_Local.x = V3_Local.x;
        V2_Local.y = V3_Local.y;
        return Intersection2D.pointInPolygon(V2_Local, this.node.getComponent(PolygonCollider2D).points);
    }
    return false;
}
export class ExtraMgr{
    public static fill_height = 1280;
    public static fill_bottom = 0;
    public static init(){
        ExtraMgr.fill_height = view.getVisibleSize().height;
        changeDispatch();
        changeSafeRect();
    }
    public static resetDispatch(node:Node){
        node.dispatchEvent = old_dispatch;
    }
    public static resetMoveTopLeft(scroll:ScrollView){
        //@ts-ignore
        scroll._moveContentToTopLeft = Empty_Func;
    }
    public static changeHitTest(node:Node){
        node._uiProps.uiTransformComp.hitTest = hitTest;
    }
    public static setTouchFunc(func:Function){
        touch_fun = func;
    }
}
