
import { _decorator, game, input, Input, KeyCode, log, macro, sys,screen, assetManager, Node, Label, find, UITransform, view, Sprite, SpriteFrame, Vec3, isValid, Layers, Prefab, Texture2D, AudioClip, Asset, AssetManager } from 'cc';
import { AudioCommon } from '../common/AudioCommon';
import { DialogCommon } from '../common/DialogCommon';
import { JsbCommon } from '../JsbCommon';
import { AudioMgr } from './AudioMgr';
import { DialogMgr } from './DialogMgr';
import { ExtraMgr } from './ExtraMgr';
import { i18nMgr } from './i18nMgr';
import { RecordMgr } from './RecordMgr';
import { ResMgr } from './ResMgr';
import { timeMgr } from './TimeMgr';
import { info } from 'console';

export class SetMgr {
    private static have_init: boolean = false;

    public static init(node_persist: Node) {
        if (SetMgr.have_init) return;
        ResMgr.init();
        AudioMgr.init(node_persist);
        i18nMgr.init();
        DialogMgr.init();
        AudioCommon.addBtnSound();
        ExtraMgr.init();
        // redPointMgr.init();
        // WidgetPro.refreshInfo();
        //关掉多点触摸
        macro.ENABLE_MULTI_TOUCH = false;
        if (sys.isBrowser) {
            if(sys.isMobile)screen.requestFullScreen()
            else input.on(Input.EventType.KEY_DOWN, onKeyDownTest, this);
        }
        else {
            input.on(Input.EventType.KEY_DOWN, onKeyDown, this);
        }
        SetMgr.have_init = true;
    }
}

function onExit() {
    let ret: any = DialogMgr.hideTop();
    if (ret) {//第一次点击返回                
        return;
    }
    if (JsbCommon.exitSdk()) {
        return;
    }
    //"تأكيد الخروج؟"："comfirm exit?"
    DialogCommon.showMsgBox2("تأكيد الخروج؟", function () {
        game.end();
    });
}
function onKeyDown(e) {
    switch (e.keyCode) {
        case KeyCode.NONE:
            onExit();
            break;
    }
}
/*
a 资源数
s resmanager
d dialogmgr
f 资源引用打印
g 界面打印
c 关闭z或x
b 打印eventManager的监听
*/
function onKeyDownTest(e) {
    switch (e.keyCode) {
        case KeyCode.NONE:
            onExit();
            break;
        case KeyCode.KEY_A:
            logCache();
            break;
        case KeyCode.KEY_S:
            ResMgr.log();
            break;
        case KeyCode.KEY_D:
            DialogMgr.log();
            break;
        case KeyCode.KEY_F:
            i18nMgr.log();
            break;
        case KeyCode.KEY_G:
            timeMgr.log();
            break;
        case KeyCode.KEY_Z:
            showLbAtlas();
            break;
        case KeyCode.KEY_X:
            closeDebugNode();
            break;
        case KeyCode.KEY_R:
            RecordMgr.getInstance().save()
            break;
        case KeyCode.KEY_T:
            RecordMgr.getInstance().clear()
            break;
    }
}

var node_debug = null;
function closeDebugNode() {
    if (isValid(node_debug)) node_debug.setParent(null);
}
function showLbAtlas() {
    if (!isValid(node_debug)) {
        node_debug = new Node("debug");
        node_debug._layer = Layers.Enum.UI_2D;
        node_debug.addComponent(Sprite).spriteFrame = ResMgr.atlas_common.getSpriteFrame("white");
        node_debug.getComponent(UITransform).setContentSize(view.getVisibleSize());
        let new_node = new Node("tex");
        new_node.setParent(node_debug);
        new_node.addComponent(Sprite);
        new_node.setScale(new Vec3(0.5, 0.5, 1))
    }
    node_debug.setParent(find("node_persist"));
    //@ts-ignore
    let tex = Label.Assembler.getAssembler({ cacheMode: Label.CacheMode.CHAR }).getAssemblerData();
    console.log(tex);
    let sf = new SpriteFrame();
    sf.texture = tex;
    node_debug.children[0].getComponent(Sprite).spriteFrame = sf;
    console.log(node_debug)
    // let texture = cc.Label._shareAtlas._fontDefDictionary._texture;
    // AtlasManager.showDebugNode(1280, 720);


    // let bg = AtlasManager.node_debug.children[0];
    // bg.width = 2048;
    // bg.height = 2048;

    // content = bg.children[0];
    // let spriteFrame = new cc.SpriteFrame();
    // spriteFrame.setTexture(texture);
    // content.getComponent(cc.Sprite).spriteFrame = spriteFrame;
}
class ResInfo{
    public bundle:AssetManager.Bundle = null;
    public arr_pre = [];
    public arr_tex = [];
    public arr_sound = [];
    constructor(bundle: AssetManager.Bundle){
        this.bundle = bundle;
    }
    public addRes(res:Asset){
        let info = this.bundle.getAssetInfo(res._uuid);
        if(!info)return false;
        //@ts-ignore
        let path = info.path;
        if(!path){
            if(info.packs)path = "|||" + info.uuid;
            else path = "___" + res.name;
        }
        if(res instanceof Prefab)this.arr_pre.push(path);
        else if(res instanceof Texture2D){
            this.arr_tex.push(path);
        }
        else if(res instanceof AudioClip)this.arr_sound.push(path);
        return true;
    }
    public log(){
        if(this.arr_pre.length + this.arr_sound.length + this.arr_tex.length == 0)return;
        console.log("%c"+this.bundle.name+":" , "color:green");
        if(this.arr_pre.length > 0)console.log("Prefab:", this.arr_pre);
        if(this.arr_tex.length > 0)console.log("Texture2D:", this.arr_tex);
        if(this.arr_sound.length > 0)console.log("AudioClip:", this.arr_sound);
    }
}
function logCache(){
    // let assets = assetManager.assets;//._count);
    let infos:Array<ResInfo> = [];
    assetManager.bundles.forEach((item)=>{
        infos.push(new ResInfo(item));
    });
    assetManager.assets.forEach((item, key)=>{
        for(let i = 0; i < infos.length; i++){
            if(infos[i].addRes(item))break;
        };
    })
    for(let i = 0; i < infos.length; i++){
        infos[i].log();
    }
}