import { AudioClip, AudioSource, Node } from "cc";
import { TSCommon } from "../TSCommon";
import { BeginInfo } from "../../../begin/scripts/BeginInfo";

const Extra_Count = 2;
const Effect_Count = 5;
const Use_Index = [0, 0, 0];
export class AudioMgr {
    private static music_on = true;
    private static effect_on = true;
    private static audio: AudioSource = null;
    private static audios_loop: Array<AudioSource> = null;
    private static audios_func: Array<AudioSource> = null;
    private static audios_effect: Array<AudioSource> = null;

    private static cache = [false, false];

    public static init(node: Node) {
        AudioMgr.audio = node.addComponent(AudioSource);
        AudioMgr.audio.playOnAwake = false;
        let node_func = new Node("node_audio_func");
        node_func.setParent(node);
        AudioMgr.audios_loop = new Array(Extra_Count);
        AudioMgr.audios_func = new Array(Extra_Count);
        let audio = null;
        for (let i = 0; i < Extra_Count; i++) {
            audio = AudioMgr.audios_loop[i] = node.addComponent(AudioSource);
            audio.loop = true;
            audio.playOnAwake = false;
            audio = AudioMgr.audios_func[i] = node_func.addComponent(AudioSource);
            audio.loop = false;
            audio.playOnAwake = false;
        }
        AudioMgr.audios_effect = new Array(Effect_Count);
        for (let i = 0; i < Effect_Count; i++) {
            audio = AudioMgr.audios_effect[i] = node.addComponent(AudioSource);
            audio.loop = false;
            audio.playOnAwake = false;
        }
        node_func.on("ended", function (source: AudioSource) {
            source.clip.emit("ended");
        })
        AudioMgr.getSaveSet();
    }
    public static onHide() {
        AudioMgr.cache[0] = AudioMgr.music_on;
        AudioMgr.cache[1] = AudioMgr.effect_on;
        AudioMgr.music_on = AudioMgr.effect_on = false;
        AudioMgr.pause();
    }
    public static onShow() {
        AudioMgr.music_on = AudioMgr.cache[0];
        AudioMgr.effect_on = AudioMgr.cache[1];
        AudioMgr.play();
    }

    public static playMusic(clip: AudioClip, loop: boolean) {
        AudioMgr.audio.stop();
        AudioMgr.audio.loop = loop;
        AudioMgr.audio.clip = clip;
        if (BeginInfo.is_review) {
            AudioMgr.audio.stop();
            return
        }
        if (AudioMgr.music_on) {
            AudioMgr.audio.play();
        }
    }

    public static playEffect(clip: AudioClip, loop = false, need_stop = false) {
        if (AudioMgr.effect_on) {
            if (loop) {
                let audio_source = AudioMgr.audios_loop[Use_Index[0]];
                audio_source.stop();
                audio_source.clip = clip;
                audio_source.play();
                Use_Index[0] = (Use_Index[0] + 1) % Extra_Count;
            }
            else if (need_stop) {
                let index = Use_Index[2];
                let audio_source = AudioMgr.audios_effect[index];
                audio_source.stop();
                audio_source.clip = clip;
                audio_source.play();
                Use_Index[2] = (index + 1) % 4;
            }
            else {
                AudioMgr.audio.playOneShot(clip, 1);
            }
        }
    }
    public static playEffectWithFunc(clip: AudioClip, func: Function) {
        if (!func) {
            AudioMgr.playEffect(clip);
            return;
        }
        let audio_source = AudioMgr.audios_func[Use_Index[1]];
        audio_source.stop();
        audio_source.clip = clip;
        audio_source.play();
        clip.once("ended", func as any);
        Use_Index[1] = (Use_Index[1] + 1) % Extra_Count;
    }
    public static setVolume(vol: number) {
        AudioMgr.audio.volume = vol;
    }
    public static getVolume() {
        return AudioMgr.audio.volume
    }
    public static getMusicOn() {
        return AudioMgr.music_on;
    }

    public static setMusicOn(state: boolean) {
        if (AudioMgr.music_on == state) return;
        AudioMgr.music_on = state;
        if (state) {
            AudioMgr.play()
        } else {
            AudioMgr.pause();
        }
        AudioMgr.saveSet();
    }

    public static getEffectOn() {
        return AudioMgr.effect_on;
    }

    public static setEffectOn(state: boolean) {
        if (AudioMgr.effect_on == state) return;
        AudioMgr.effect_on = state;
        AudioMgr.saveSet();
    }

    public static play() {
        if (BeginInfo.is_review) {
            AudioMgr.audio.stop();
            return
        }
        AudioMgr.audio.play();
    }
    public static pause() {
        AudioMgr.audio.pause();
        for (let i = 0; i < Extra_Count; i++) {
            AudioMgr.audios_loop[i].pause();
            AudioMgr.audios_func[i].pause();
        }
        for (let i = 0; i < Effect_Count; i++) {
            AudioMgr.audios_effect[i].stop();
            AudioMgr.audios_effect[i].clip = null;
        }
    }
    public static stopSound(clip: AudioClip) {
        for (let i = 0; i < Effect_Count; i++) {
            if (this.audios_effect[i].clip == clip) {
                this.audios_effect[i].stop();
                this.audios_effect[i].clip = null;
            }
        }
    }
    public static stop() {
        AudioMgr.audio.stop();
        AudioMgr.audio.clip = null;
        for (let i = 0; i < Extra_Count; i++) {
            AudioMgr.audios_loop[i].stop();
            AudioMgr.audios_loop[i].clip = null;
            AudioMgr.audios_func[i].stop();
            AudioMgr.audios_func[i].clip = null;
        }
        for (let i = 0; i < Effect_Count; i++) {
            AudioMgr.audios_effect[i].stop();
            AudioMgr.audios_effect[i].clip = null;
        }
    }
    public static saveSet() {
        let str = "0", str1 = "0";
        if (AudioMgr.music_on) str = "1";
        if (AudioMgr.effect_on) str1 = "1";
        TSCommon.setCookie("music_switch", str + str1);
    }
    public static getSaveSet() {
        let str = TSCommon.getCookie("music_switch");
        if (!str) return;
        AudioMgr.music_on = str[0] == "1";
        AudioMgr.effect_on = str[1] == "1";
    }
}
