
import { JsonAsset, SpriteFrame, sp, SpriteAtlas, AssetManager, js, isValid, Color } from "cc";
import { i18Base } from "../components/i18Base";
import { i18nSprite } from "../components/i18nSprite";
import { TSCommon } from "../TSCommon";
import { ResMgr } from "./ResMgr";
/*
使用说明
在label或richtext组件添加多语言组件i18nLabel

修改内容：
1在编辑器设置字符串索引
2动态修改
i18nLabel.setString(index,[可选参数])
参数会替换字符串中#0，#1

3动态获取字符串
i18nMgr.getString(12,[可选参数])

*/

const Path_Lb = "i18n/label/";
const Path_Sf = "i18n/sprite/";
const Path_Atlas = "i18n/atlas/";
const Path_Spine = "i18n/spine/";

export const i18Type = {
    Label: 0,
    Sprite: 1,
    Spine: 2,
}
const SupportLanguage = ["ara", "en", "zh"];
type LanguageType = "ara" | "en" | "zh";

var Language = "ara";     // 当前语言
const Empty = Object.create(null);
class VersionRes {
    ver: string;
    res: SpriteFrame | SpriteAtlas | sp.SkeletonData;
    public init(ver: string, res: any) {
        this.ver = ver;
        this.res = res;
    }
}
// const resInfoPool = new Pool(() => new VersionRes(), 32);

class i18Bundle {
    public m_bundle: AssetManager.Bundle = null;

    private arr_com: Array<Array<i18Base>> = [];

    private data_txt = Empty;
    private map_sf: Map<string, VersionRes> = new Map();
    private map_atlas: Map<string, VersionRes> = new Map();
    private map_spinedata: Map<string, VersionRes> = new Map();

    constructor(bundle: AssetManager.Bundle) {
        this.m_bundle = bundle;
        this.arr_com = new Array(3);
        for (let i = 0; i < 3; i++)this.arr_com[i] = new Array();
        this.loadString();
    }
    public log() {
        console.log("所有组件:");
        for (let i = 0; i < 2; i++) {
            let arr = this.arr_com[i];
            let str = "";
            arr.forEach(ele => {
                str += ele.node.name;
                if (isValid(ele)) str += " vv ";
                else str += " xx "
            })
            console.log(str);
        }
        console.log("所有资源:");
        console.log(this.map_sf);
        console.log(this.map_atlas);
        // console.log(this.map_spinedata);
    }
    public change() {
        this.loadString();
        let arr = null;
        for (let i = 1; i < this.arr_com.length; i++) {
            arr = this.arr_com[i];
            for (let j = 0, l = arr.length; j < l; j++) {
                arr[j].refresh();
            }
        }
    }
    public addCom(index: number, com: i18Base) {
        this.arr_com[index].push(com);
    }
    public removeCom(index: number, com: i18Base) {
        js.array.fastRemove(this.arr_com[index], com);
    }
    public releaseSfByPrefix(prefix: string) {
        this.map_sf.forEach((v, k: string) => {
            if (!k.startsWith(prefix)) return;
            v.res.decRef()
            this.map_sf.delete(k);
        })
    }
    public clear() {
        this.data_txt = Empty;
        let func = function (item: VersionRes) { if(isValid(item.res))item.res.decRef() };
        this.map_sf.forEach(func);
        this.map_atlas.forEach(func);
        this.map_spinedata.forEach(func);
        this.map_sf.clear();
        this.map_atlas.clear();
        this.map_spinedata.clear();
    }
    public getString(key: string, opts?: any) {
        let str = this.data_txt[key];
        if (!str) return Language + " no txt:" + key;
        if (opts) {
            for (let i = 0; i < opts.length; i++) {
                str = str.replace('##' + i, opts[i]);
            }
        }
        return str;
    }
    public getRichStr(key: string, opts?: any, hexes?: any, defHex = "#FFFFFF") {
        let str = this.data_txt[key];
        if (!str) return Language + " no txt:" + key;
        if (opts && hexes) {
            const def_hexInd = 0;
            for (let i = 0; i < opts.length; i++) {
                let hexInd = hexes.length == opts.length ? i : def_hexInd;
                // console.log('##' + i, `<color=${hexes[hexInd]}>${opts[i]}</color>`);
                str = str.replace('##' + i, `<color=${hexes[hexInd]}>${opts[i]}</color>`);
            }
            str = `<color=${defHex}>${str}</color>`
        }
        return str;
    }
    public loadString() {
        this.m_bundle.load(Path_Lb + Language, JsonAsset, (err: any, data: JsonAsset) => {
            if (err) {
                console.log(err);
                return;
            }
            this.data_txt = data.json;
            let lbs = this.arr_com[0];
            for (let i = 0, l = lbs.length; i < l; i++) {
                lbs[i].refresh();
            }
        })
    }
    public getSf(key: string, atlas?: string) {
        if (atlas) {
            let atlas_info = this.map_atlas.get(atlas);
            if (atlas_info?.ver == Language && isValid(atlas_info.res)) return (atlas_info.res as SpriteAtlas).getSpriteFrame(key);
            else this.loadAtlas(atlas);
        }
        else {
            let sf_info = this.map_sf.get(key);
            if (sf_info?.ver == Language && isValid(sf_info.res)) return sf_info.res;
            else this.loadSf(key);
        }
        return null;
    }
    public loadSf(key: string) {
        let info = this.map_sf.get(key);
        if (info) {
            if (info.ver == Language) return;
        }
        else {
            let info = new VersionRes();
            info.init(Language, null);
            this.map_sf.set(key, info);
        }
        let final_path = Path_Sf + Language + "/" + key + "/spriteFrame";
        this.m_bundle.load(final_path, SpriteFrame, (err: any, sf: SpriteFrame) => {
            if (err) {
                console.log(err);
                return;
            }
            let info = this.map_sf.get(key);
            let old_res = null;
            if (info.ver != Language) {
                if (isValid(info.res)) {
                    old_res = info.res;
                    info.ver = Language;
                }
                else return;
            }
            else if (isValid(info.res)) return;

            info.res = sf.addRef() as SpriteFrame;
            let sps = this.arr_com[1];
            for (let i = 0, l = sps.length; i < l; i++) {
                if (sps[i].index == key) sps[i].refresh();
            }
            if (old_res) old_res.decRef();
        })
    }
    public loadAtlas(key: string) {
        let info = this.map_atlas.get(key);
        if (info) {
            if (info.ver == Language) return;
        }
        else {
            let info = new VersionRes();
            info.init(Language, null);
            this.map_atlas.set(key, info);
        }
        let final_path = Path_Atlas + key + "_" + Language;
        this.m_bundle.load(final_path, SpriteAtlas, (err: any, sf: SpriteAtlas) => {
            if (err) {
                console.log(err);
                return;
            }
            let info = this.map_atlas.get(key);
            let old_res = null;
            if (info.ver != Language) {
                if (isValid(info.res)) {
                    old_res = info.res;
                    info.ver = Language;
                }
                else return;
            }
            else if (isValid(info.res)) return;

            info.res = sf.addRef() as SpriteAtlas;
            let sps = this.arr_com[1];
            for (let i = 0, l = sps.length; i < l; i++) {
                if ((sps[i] as i18nSprite).atlas == key) sps[i].refresh();
            }
            if (old_res) old_res.decRef();
        })
    }

    public getSpineData(key: string) {
        let spine_info = this.map_spinedata.get(key);
        if (spine_info?.ver == Language && isValid(spine_info.res)) return spine_info.res;
        else this.loadSpine(key);
    }
    public loadSpine(key: string) {
        let info = this.map_spinedata.get(key);
        if (info) {
            if (info.ver == Language) return;
        }
        else {
            let info = new VersionRes();
            info.init(Language, null);
            this.map_spinedata.set(key, info);
        }
        let final_path = Path_Spine + key + "_" + Language;
        this.m_bundle.load(final_path, sp.SkeletonData, (err: any, spine: sp.SkeletonData) => {
            if (err) {
                console.log(err);
                return;
            }
            let info = this.map_spinedata.get(key);
            let old_res = null;
            if (info.ver != Language) {
                if (isValid(info.res)) {
                    old_res = info.res;
                    info.ver = Language;
                }
                else return;
            }
            else if (isValid(info.res)) return;

            info.res = spine.addRef() as SpriteAtlas;
            let sps = this.arr_com[2];
            for (let i = 0, l = sps.length; i < l; i++) {
                if (sps[i].index == key) sps[i].refresh();
            }
            if (old_res) old_res.decRef();
        })
    }

}
export class i18nMgr {
    private static main_i18: i18Bundle = null;
    private static game_i18: i18Bundle = null;
    // private static other_i18: Map<string, i18Bundle> = null;

    public static getLanguage() {
        return Language;
    }
    public static addCom(bundle: string, index: number, com: i18Base) {
        if (bundle.length) this.game_i18.addCom(index, com);
        else this.main_i18.addCom(index, com);

    }
    public static removeCom(bundle: string, index: number, com: i18Base) {
        if (bundle.length) this.game_i18.removeCom(index, com);
        else this.main_i18.removeCom(index, com);
    }
    public static log() {
        this.main_i18.log();
        this.game_i18?.log();
    }
    public static init() {
       // Language = TSCommon.getCookie('language');
       // if (SupportLanguage.indexOf(Language) == -1) Language = SupportLanguage[0];
        let bundle = ResMgr.getBundle(false);
        this.main_i18 = new i18Bundle(bundle);
    }
    public static addGameBundle() {
        let game_bundle = ResMgr.getBundle(true);
        if (!this.game_i18) {
            this.game_i18 = new i18Bundle(game_bundle);
        }
        else if (this.game_i18.m_bundle != game_bundle) {
            this.game_i18.clear();
            this.game_i18.m_bundle = game_bundle;
            this.game_i18.loadString();
        }
    }
    public static change(language: LanguageType) {
        return;
        Language = language;
        this.main_i18.change();
        this.game_i18?.change();
        TSCommon.setCookie('language', language)
    }
    public static getString(key: string, opts?: Array<string>) {
        return this.main_i18.getString(key, opts);
    }
    public static getRichStr(key: string, opts?: Array<string>, hexes?: Array<string>, defHex?) {
        return this.main_i18.getRichStr(key, opts, hexes, defHex);
    }
    public static getBundleString(key: string, opts?: Array<any>) {
        return this.game_i18.getString(key, opts);
    }
    public static getSf(key: string, atlas?: string) {
        return this.main_i18.getSf(key, atlas);
    }
    public static getGameSf(key: string, atlas?: string) {
        return this.game_i18.getSf(key, atlas);
    }
    public static getSpine(key: string) {
        return this.main_i18.getSpineData(key);
    }
    public static getGameSpine(key: string) {
        return this.game_i18.getSpineData(key);
    }
    public static releaseSfByPrefix(prefix: string, in_game: boolean) {
        if (in_game) this.game_i18.releaseSfByPrefix(prefix);
        else this.main_i18.releaseSfByPrefix(prefix);
    }
}
