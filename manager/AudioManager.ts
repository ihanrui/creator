import {TSCommon} from "../TSCommon"

export class AudioManager{
    private static music_on: boolean = true;
    private static effect_on: boolean = true;
    private static temp_music: cc.AudioClip = null;
    private static music_id: number = -1;

    public static playMusic(clip:cc.AudioClip , loop:boolean = true){
        if(AudioManager.music_on){
           AudioManager.music_id = cc.audioEngine.playMusic(clip , loop);
           return AudioManager.music_id
        }else{
            AudioManager.temp_music = clip;
        }
        return null
    }

    public static playEffect(clip:cc.AudioClip , loop:boolean = false , func: Function = null){
        if(AudioManager.effect_on){
            let sid = cc.audioEngine.playEffect(clip , loop);
            if(func){
                if(sid == -1){
                    func();
                    return;
                }else{
                    cc.audioEngine.setFinishCallback(sid,func);
                    
                }
            }
            return sid
        }else{
            func && func();
        }
    }

    public static stopMusic(music:number){
        cc.audioEngine.stop(music)
    }

    public static stopEffect(audio:number){
        cc.audioEngine.stop(audio);
    }

    public static getMusicOn(){
        return AudioManager.music_on;
    }
    public static isSoundOn() {
        var isOn = TSCommon.getCookie("setting_sound");
        return isOn != "0";
    }

    public static setMusicOn(state:boolean){
        if(AudioManager.music_on == state)return;
        AudioManager.music_on = state;
        if(state){
            if(AudioManager.music_id > -1){
                cc.audioEngine.resumeMusic();
            }
            if(cc.audioEngine.getState(AudioManager.music_id) != cc.audioEngine.AudioState.PLAYING){
                if(AudioManager.temp_music)AudioManager.playMusic(AudioManager.temp_music , true);
            }
        }else{
            cc.audioEngine.pauseMusic();
        }
        TSCommon.setCookie("music_on",AudioManager.music_on?"1":"0");
    }

    public static getEffectOn(){
        return AudioManager.effect_on;
    }

    public static setEffectOn(state:boolean){
        if(AudioManager.effect_on == state)return;
        AudioManager.effect_on = state;
        if(!state){
            cc.audioEngine.stopAllEffects();
        }
        TSCommon.setCookie("effect_on",AudioManager.effect_on?"1":"0");
    }

    public static getSaveSet(){
        let str = TSCommon.getCookie("music_on");
        if(str != "" && str == "0"){
            AudioManager.music_on = false;
        }
        str = TSCommon.getCookie("effect_on");
        if(str != "" && str == "0"){
            AudioManager.effect_on = false;
        }
    }

    public static stopAll(){
        cc.audioEngine.stopAll();
    }
}
