import { _decorator, Component, AudioClip, isValid } from "cc";
import { AudioCommon } from "../common/AudioCommon";
import { AudioMgr } from "../manager/AudioMgr";
import { ResMgr } from "../manager/ResMgr";

const { ccclass, property } = _decorator;

@ccclass("GameSound")
export class GameSound extends Component {

    public static instance: GameSound = null;

    @property([AudioClip])
    public sounds: AudioClip[] = [];

    public sounds_extra = [];

    private lobby_click = null;

    public playMusic(index: number = 0) {
        if(isValid(this.sounds[index]))AudioMgr.playMusic(this.sounds[index], true);
    }
    public playSound(index: number, loop = false, use_extra = false) {
        let sounds = use_extra ? this.sounds_extra : this.sounds;
        if(!isValid(sounds[index]))return;
        console.log(`effect:${index}::${use_extra}`)
        AudioMgr.playEffect(sounds[index], loop);
    }
    public playSoundWithFunc(index: number, func: Function, use_extra: boolean){
        let sounds = use_extra ? this.sounds_extra : this.sounds;
        if(!isValid(sounds[index]))return;
        AudioMgr.playEffectWithFunc(sounds[index], func);
    }
    public playStopSound(index:number){
        let sound = this.sounds[index];
        if(!isValid(sound))return -1;
        AudioMgr.playEffect(sound, false, true);
    }
    public stopSound(index:number){
        AudioMgr.stopSound(this.sounds[index]);
    }
    public setGameClick(index: number) {
        this.lobby_click = AudioCommon.changeBtnClip(this.sounds[index]);
    }
    public loadGameClick(path:string){
        ResMgr.gameLoadWithTag("gamesound",path,AudioClip,(err,clip:AudioClip)=>{
            if(!isValid(this) || err)return;
            this.lobby_click = AudioCommon.changeBtnClip(clip);
        })
    }
    public loadMusic( path:string){
        ResMgr.gameLoadWithTag("gamesound",path,AudioClip,(err,clip:AudioClip)=>{
            if(!isValid(this) || err)return;
            AudioMgr.playMusic(clip, true);
        })
    }
    public loadSounds(prefix:string, paths:Array<string>){
        this.sounds.length = paths.length;
        for(let i = 0 ; i < paths.length ; i++){
            if(isValid(this.sounds[i]) || !paths[i])continue;
            ResMgr.gameLoadWithTag("gamesound",prefix+paths[i],AudioClip,(err,clip:AudioClip)=>{
                if(!isValid(this) || err)return;
                this.sounds[i] = clip;
            })
        }
    }
    public loadSoundsExtra(prefix:string, paths:Array<string>){
        this.sounds_extra.length = paths.length;
        for(let i = 0 ; i < paths.length ; i++){
            if(!paths[i]?.length)continue;
            ResMgr.gameLoadWithTag("gamesound",prefix+paths[i],AudioClip,(err,clip:AudioClip)=>{
                if(!isValid(this))return;
                if(err){
                    console.log(err)
                    return;
                }
                this.sounds_extra[i] = clip;
            })
        }
    }
    public getSound(index:number, use_extra: boolean){
        let sounds = use_extra ? this.sounds_extra : this.sounds;
        return sounds[index];
    }

    public onLoad() {
        GameSound.instance = this;
    }
    public onDestroy() {
        if (this.lobby_click)AudioCommon.changeBtnClip(this.lobby_click);
        ResMgr.gameRealeaseByTag("gamesound");
        GameSound.instance = null;
    }

}
