//@ts-nocheck

const funcs_calc_pos = new Array<Function>(1);
funcs_calc_pos[0] = function(index:number){
    let row = index % 2;
    let col = (index - row) / 2;
    return new cc.Vec2(col * 232 + 2 , row * 232 + 2);
}

class Atlas{
    private _dirty = false;
    public _texture: cc.RenderTexture;
    private _width = 0;
    private _height = 0;
    //纹理得偏移和Spriteframe对应得纹理
    private _innerTextureInfos = new Map<string , any>();
    //每个index目前存在的数量
    private all_count: Array<number>;
    //func(index)根据可以p数据计算位置得函数
    private func_indexs: Array<number>;

    constructor(func_indexs:Array<number>, width:number=512 , height: number=512){
        this.func_indexs = func_indexs;
        this.all_count = new Array(func_indexs.length).fill(0);
        
        let texture = new cc.RenderTexture();
        texture.initWithSize(width, height);
        texture.update();

        this._texture = texture;
        this._width = width;
        this._height = height;
    }

    public containName(name:string){
        return this._innerTextureInfos.has(name);
    }

    public insertSpriteFrame(index:number, p:any, spriteFrame:cc.SpriteFrame){
        if(spriteFrame._original && spriteFrame._original._texture != this._texture){
            spriteFrame._resetDynamicAtlasFrame();
        }
        let info = this._innerTextureInfos[p];
        if (info) {
            sx += info.x;
            sy += info.y;
        }else{
            let texture = spriteFrame._texture;
            if(!texture._packable)return null;
            let rect = spriteFrame._rect;
            let sx = rect.x, sy = rect.y;
            let pos = funcs_calc_pos[this.func_indexs[index]](this.all_count[index]);
            cc.log(pos.x + "  " + pos.y);
            if(pos.x + texture.width > this._width || pos.y + texture.height > this._height){
                cc.warn("atlas size if beyond!!!!");    
                return null;
            }
            this._texture.drawTextureAt(texture, pos.x-1, pos.y);
            this._texture.drawTextureAt(texture, pos.x+1, pos.y);
            this._texture.drawTextureAt(texture, pos.x, pos.y-1);
            this._texture.drawTextureAt(texture, pos.x, pos.y+1);
            this._texture.drawTextureAt(texture, pos.x, pos.y);
            spriteFrame.releaseTexImg();
            this._innerTextureInfos[p] = {
                x: pos.x,
                y: pos.y,
                texture: texture
            };
            this.all_count[index]++;
            sx += pos.x;
            sy += pos.y;
            this._dirty = true;
        }
        let frame = {
            x: sx,
            y: sy,
            texture: this._texture
        }
        return frame;
    }

    public update () {
        if (!this._dirty) return;
        this._texture.update();
        this._dirty = false;
    }

    public reset () {
        this._innerTextureInfos.clear();
    }

    public destroy () {
        this.reset();
        this._texture.destroy();
    }
}

export class AtlasManager extends cc.Component {
    private static all_atlas:Map<string , Atlas> = new Map<string , Atlas>();
    private static node_debug: cc.Node = null;

    //增加一个纹理需传入唯一名字   一个根据插入图片索引计算位置得函数
    public static addAtlas(name: string, func_indexs: Array<number>,  width: number, height: number){
        if(AtlasManager.all_atlas.has(name)){
            cc.warn(name+"atlas is has!!!");
            return;
        }
        let atlas = new Atlas(func_indexs , width, height);
        AtlasManager.all_atlas.set(name , atlas);
    }
    //将一张精灵插入到指定纹理中
    public static insertSpriteFrame(atlas_name:string, index:number, p:any, spriteFrame:cc.SpriteFrame){
        let atlas = AtlasManager.all_atlas.get(atlas_name);
        if(atlas){
            return atlas.insertSpriteFrame(index, p, spriteFrame);
        }else{
            cc.warn("cant find: " + atlas_name);
        }
        return null;
    }
    //移除纹理
    public static removeAtlasByName(atlas_name: string){
        let atlas = AtlasManager.all_atlas.get(atlas_name);
        if(atlas){
            atlas.destroy();
            AtlasManager.all_atlas.delete(atlas_name);
        }
    }
    public static removelAll(){
        AtlasManager.all_atlas.forEach(atlas=>{
            atlas.destroy();  
        })
        AtlasManager.all_atlas.clear();
    }
    public static logAtlas(){
        cc.log("dynamic atlas");
        cc.log(AtlasManager.all_atlas);
    }
    public static showAtlas(atlas_name: string , show: boolean){
        if (show) {
            if (!AtlasManager.node_debug || !AtlasManager.node_debug.isValid) {
                if(!AtlasManager.all_atlas.has(atlas_name))return;
                let width = cc.visibleRect.width;
                let height = cc.visibleRect.height;

                AtlasManager.node_debug = new cc.Node('ATLAS_DEBUG');
                AtlasManager.node_debug.width = width;
                AtlasManager.node_debug.height = height;
                AtlasManager.node_debug.zIndex = 10;
                AtlasManager.node_debug.parent = cc.Canvas.instance.node;
                AtlasManager.node_debug.x = 0;
                AtlasManager.node_debug.y = 0;
                
                let texture = AtlasManager.all_atlas.get(atlas_name)._texture;
                // let texture = cc.Label._shareAtlas._fontDefDictionary._texture;

                let bg = new cc.Node("BG");
                bg.parent = AtlasManager.node_debug;
                let bg_texture = new cc.Texture2D;
                let bg_spriteFrame = new cc.SpriteFrame;
                bg_texture.initWithData(new Uint8Array([0, 0, 0]), cc.Texture2D.PixelFormat.RGB888, 1, 1);
                bg_spriteFrame.setTexture(bg_texture);
                bg_spriteFrame.setRect(cc.rect(0, 0, texture.width, texture.height));
                let bg_sprite = bg.addComponent(cc.Sprite);
                bg_sprite.spriteFrame = bg_spriteFrame;
                bg.scale = 0.5;
                
                let content = new cc.Node('CONTENT');
                content.parent = AtlasManager.node_debug;
                let spriteFrame = new cc.SpriteFrame();
                spriteFrame.setTexture(texture);
                let sprite = content.addComponent(cc.Sprite);
                sprite.spriteFrame = spriteFrame;
                content.scale = 0.5;
            }
            return AtlasManager.node_debug;
        }
        else {
            if (AtlasManager.node_debug) {
                AtlasManager.node_debug.parent = null;
                AtlasManager.node_debug = null;
            }
        }
    }
}
