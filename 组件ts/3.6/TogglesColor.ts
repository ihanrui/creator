import { _decorator, Toggle, Node, LabelOutline, Component, Color, CCBoolean, ToggleContainer, Label, Sprite } from 'cc';
import { UtilsCommon } from '../utils/UtilsCommon';
const { ccclass, property, menu, executionOrder } = _decorator;

// /**
//  * 切换Toggles时改变颜色
//  * @author chenxj 2021.04.27
//  * 
//  * 挂载togglecontainer的node
//
//  * 命名规则：
//  * 背景toggle_name
//  * 文字name
//  * 选中图checkmark_name
//  * 
//  * 有描边则在选中的时候才显示
//  */
class ToggleInfo {
    public toggle: Toggle | null = null;
    public sp_bg: Sprite | null = null;
    public lb_text: Label | null = null;
    public outline: LabelOutline | null = null;
    public constructor(t: Toggle, l: Label) {
        this.toggle = t;
        this.lb_text = l;
    }
}

@ccclass('TogglesColor')
@menu("新组件/TogglesColor")
export class TogglesColor extends Component {
    //    //#region property 资源
    //    /** !#zh selectedColor 选中颜色 */
    @property(Color)
    public selectedColor: Color = new Color(255, 251, 129, 255);
    //    /** !#zh unselectedColor 未选中颜色 */
    @property(Color)
    public unselectedColor: Color = new Color(83, 23, 16, 255);

    @property({ tooltip: "获取其他组件方式\n1:当前节点下面\n2:对应toggle下面" })
    public type = 2;
    //'选中的时候是否隐藏toggle背景'
    @property
    public hideToggleBg = false


    private infos: Array<ToggleInfo> = null;
    public onLoad() {
        this.updateInfos();
        this.onToggleChange();
    }
    start() {
        let handler = UtilsCommon.createEventHandler(this.node, "TogglesColor", "onToggleChange");
        let container = this.node.getComponent(ToggleContainer);
        container.checkEvents.push(handler);
    }
    public updateInfos() {
        let container = this.node.getComponent(ToggleContainer);
        if (!container) return;
        let toggles = container.toggleItems;
        let l = toggles.length;
        this.infos = new Array(l);
        let out = null;
        let lb = null;
        let node_toggle = null;
        if (this.type == 2) {
            for (let i = 0; i < l; i++) {
                node_toggle = toggles[i].node;
                lb = node_toggle.getComponentInChildren(Label);
                this.infos[i] = new ToggleInfo(toggles[i], lb);
                out = lb.node.getComponent(LabelOutline);
                if (out) this.infos[i].outline = out;
                if (this.hideToggleBg) {
                    this.infos[i].sp_bg = node_toggle.children[0].getComponent(Sprite);
                }
            }
        }
        else {
            let parent = this.node;
            let lb_index = "";
            for (let i = 0; i < l; i++) {
                node_toggle = toggles[i].node;
                lb_index = node_toggle.name.slice(6);
                lb = parent.getChildByName("lb" + lb_index).getComponent(Label);
                this.infos[i] = new ToggleInfo(toggles[i], lb);
                out = lb.node.getComponent(LabelOutline);
                if (out) this.infos[i].outline = out;
            }
        }
    }
    public onToggleChange() {
        if (!this.infos) return;
       
        let info: ToggleInfo = null;
        for (let i = 0, len = this.infos.length; i < len; i++) {
            info = this.infos[i];
            if (info.toggle.enabled && info.toggle.isChecked) {
                info.lb_text.color = this.selectedColor;
                if (info.outline) info.outline.enabled = true;
                if (this.hideToggleBg) {
                    info.sp_bg.enabled = false
                }
            }
            else {
                info.lb_text.color = this.unselectedColor;
                if (info.outline) info.outline.enabled = false;
                if (this.hideToggleBg) {
                    info.sp_bg.enabled = true
                }
            }
        }
    }
    setActivateByIndex(index: number, func = null) {
        if (index < 0) return;
        let toggleInfo = this.infos[index]

        if (!toggleInfo) return;

        toggleInfo.toggle.enabled = true;
        toggleInfo.toggle.isChecked = true;

        this.onToggleChange();
        func && func();
    }
}
