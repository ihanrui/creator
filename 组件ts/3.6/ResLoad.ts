
import { _decorator, Asset, AudioClip, ccenum, Component, sp, Sprite, SpriteFrame } from 'cc';
import { ResMgr } from '../manager/ResMgr';
import { AudioMgr } from '../manager/AudioMgr';
const { ccclass, menu, property, type } = _decorator;

const LoadType = [SpriteFrame, AudioClip, sp.SkeletonData];
enum ResType{
    SpriteFrame,
    Sound,
    Spine
}
ccenum(ResType);

@ccclass('ResLoad')
@menu("新组件/ResLoad")
export class ResLoad extends Component {
    @type(ResType)
    @property
    private type: ResType = ResType.SpriteFrame;
    @property
    private is_game = false;
    @property
    private path = "";

    private m_com: Component = null;
    private m_res: Asset = null;
    
    protected onLoad(): void {
        switch(this.type){
            case ResType.SpriteFrame:
                this.m_com = this.node.getComponent(Sprite);
                break;
            case ResType.Sound:
                break;
            case ResType.Spine:
                this.m_com = this.node.getComponent(sp.Skeleton);
                break;
        }
    }
    protected start(): void {
        this.loadRes();
    }
    public setPath(path:string){
        this.path = path;
        this.loadRes();
    }
    public loadRes(){
        if(this.path.length == 0)return;
        let bundle = ResMgr.getBundle(this.is_game);
        if(!bundle)return;
        let final_path = this.path;
        if(this.type == ResType.SpriteFrame)final_path += "/spriteFrame";
        bundle.load(final_path, LoadType[this.type] as any, (err, sf)=>{
            if(!this.isValid)return;
            if(err){
                console.log(err);
                return;
            }
            let old = this.m_res;
            this.m_res = sf.addRef();
            switch(this.type){
                case ResType.SpriteFrame:
                    (this.m_com as Sprite).spriteFrame = sf as SpriteFrame;
                    break;
                case ResType.Sound:
                    AudioMgr.playMusic(this.m_res as AudioClip, true);
                    break;
                case ResType.Spine:
                    (this.m_com as sp.Skeleton).skeletonData = sf as sp.SkeletonData;
                    (this.m_com as sp.Skeleton).setAnimation(0, "animation", true);
                    break;
            }
            if(old)old.decRef();
        })
    }
    protected onDestroy(): void {
        if(this.m_res)this.m_res.decRef();
    }

}
