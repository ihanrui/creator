
import { _decorator, AudioClip, AudioSource, Component, Node } from 'cc';


const map_clips = new Map<string, any>();
function playNext(source:AudioSource){
    source.clip = null;
    let clips = map_clips.get(source.uuid);
    clips.shift();
    if(clips.length == 0)return;
    source.clip = clips[0];
    console.log("sound3:",clips[0]);
    source.play();
}

export class ContinueSound {

    private source: AudioSource = null;
    
    private clips: Array<AudioClip> = null;
    constructor(node:Node){
        this.source = node.addComponent(AudioSource);
        this.source.loop = false;
        this.source.playOnAwake = false;
        this.clips = new Array();
        map_clips.set(this.source.uuid, this.clips);
        if(!node.hasEventListener("ended")){
            node.on("ended", playNext);
        }
    }
    public play(clip:AudioClip){
        console.log("sound1:",clip);
        this.clips.length = 0;
        this.clips.push(clip);
        this.source.clip = clip;
        this.source.play();
    }
    public addSound(clip:AudioClip){
        if(this.clips.length == 0){
            this.clips.push(clip);
            this.source.clip = clip;
            console.log("sound2:",clip);
            this.source.play();
        }
        else{
            this.clips.push(clip);
        }
    }
    public remove(){
        map_clips.delete(this.source.uuid);
        this.source.destroy();
    }

}
