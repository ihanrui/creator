import { _decorator, Label, RichText, assetManager } from 'cc';
const { ccclass, property, executeInEditMode, disallowMultiple, menu } = _decorator;

import { i18nMgr, i18Type } from "../manager/i18nMgr";
import { i18Base } from './i18Base';

@ccclass("i18nLabel")
@executeInEditMode
@disallowMultiple
@menu("多语言/i18nLabel")
export class i18nLabel extends i18Base {
    public static cache_json: Map<string, any> = new Map;

    protected i18_type = i18Type.Label;

    private i18n_params: string[] = [];

    private m_lb: Label | RichText = null;

    onLoad(){
        this.m_lb = this.node.getComponent(Label);
        if(!this.m_lb)this.m_lb = this.node.getComponent(RichText);
        if(!this.m_lb){
            console.warn("no lb:",this.node.name);
            this.m_lb = this.node.addComponent(Label);
        }
    }
    public refresh(){
        if(!this.i18n_index.length)return;
        let str = null;
        if(this.bundle.length)str = i18nMgr.getBundleString(this.i18n_index, this.i18n_params)
        else str = i18nMgr.getString(this.i18n_index, this.i18n_params);
        if(str)this.m_lb.string = str;
    }
    public setString(index:string, opts?:Array<any>){
        this.i18n_index = index;
        this.i18n_params = opts;
        this.refresh();
    }
    public async refreshInEditor(){
        if(!this.i18n_index.length){
            // this.m_lb.string = "";
            return;
        }
        let bundle = this.bundle;
        if(!bundle.length)bundle = "master";
        let json_data = i18nLabel.cache_json.get(bundle);
        if(json_data){
            if(json_data[this.i18n_index])this.m_lb.string = json_data[this.i18n_index];
        }
        else{
            let assetPath = "";
            if(bundle == "master")assetPath = `db://assets/master/i18n/label/${i18nMgr.getLanguage()}.json`;
            else assetPath = `db://assets/games/${this.bundle}/i18n/label/${i18nMgr.getLanguage()}.json`;
            const info = await Editor.Message.request('asset-db', 'query-assets', {pattern:assetPath});
            assetManager.loadAny({ uuid: info[0].uuid }, (err, json) => {
                if(err){
                    console.log(err)
                    return;
                }
                i18nLabel.cache_json[bundle] = json.json;
                if(json.json[this.i18n_index])this.m_lb.string = json.json[this.i18n_index];
            });
        }
    }

}
