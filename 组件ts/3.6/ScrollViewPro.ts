import { _decorator, Node,Enum, EventHandler, ScrollView, Prefab, instantiate, UITransform, Vec3, find, NodeEventType } from 'cc';
const { ccclass, property, menu } = _decorator;

import { ViewItemPro } from "./ViewItemPro"
import { CircleArray } from '../extra/ExClass';

const _tempVec3 = new Vec3();
const EPSILON = 1e-4;

@ccclass("ScrollViewPro")
@menu("新组件/ScrollViewPro")
export class ScrollViewPro extends ScrollView {
    @property(Prefab)
    public item_prefab: Prefab = null;
    @property
    public item_num = 0;
    @property
    public item_offset = 0;
    @property
    public show_action = false;

    private uitrans: UITransform = null;
    
    private have_init = false;
    private begin_index = 0;
    private end_index = 0;
    private all_item: Array<ViewItemPro> = [];
    private circle_items: CircleArray<ViewItemPro> = null;
    private data: any = null;
    private p: any = null;

    protected onLoad(){
        this.uitrans = this._content.getComponent(UITransform);
    }

    private initParam(){
        if(this.have_init)return;
        this.have_init = true;
        // this.uitrans = this._content.getComponent(UITransform);
        let node = instantiate(this.item_prefab);
        node.setParent(this.content);
        this.all_item.push(node.getComponent(ViewItemPro));
        if(this.item_offset == 0){
            this.item_offset = node.getComponent(UITransform).height;
        }
        let my_uitrans = this.node.getComponent(UITransform);
        if(this.item_num == 0){
            let height = my_uitrans.height;
            this.item_num = Math.ceil(height / this.item_offset) + 2;
        }       
    }
    protected getItem(index: number) {
        if (index < this.all_item.length) return this.all_item[index];
        else {
            if (index == this.all_item.length) {
                let node = instantiate(this.item_prefab);
                node.setParent(this._content);
                this.all_item.push(node.getComponent(ViewItemPro));
                return this.all_item[index];
            }
            else {
                console.warn("getItem index not generate")
            }
        }
    }
    protected refreshContentSize(len:number){
        let height = len * this.item_offset;
        if(height < this.view.contentSize.height)height = this.view.contentSize.height;
        this.uitrans.height = height;
        let begin_y = (1 - this.uitrans.anchorY) * this.uitrans.height;
        this.content.toY(this._topBoundary - begin_y);
        return begin_y;
    }
    public autoSize(){
        if(this.view){
            this.view.node.on(NodeEventType.SIZE_CHANGED, function(){
                let height = this.view.height;
                if(this.uitrans.height < height)this.uitrans.height = height;
                if((this.item_num - 1) * this.item_offset < height && this.item_offset > 0){
                    this.item_num = Math.ceil(height / this.item_offset) + 2;
                }
            }, this);
        }
    }

    public initData(data: Array<any>, p?:any) {
        this.stopAutoScroll();
        this.initParam();
        let begin_y = this.refreshContentSize(data.length);
        this.data = data;
        this.p = p;
        let num = Math.min(data.length, this.item_num);
        this.initItems(begin_y, num);
    }

    protected initItems(begin_y: number, num: number) {
        this.begin_index = 0;
        this.end_index = num - 1;
        let i = 0;
        for (; i < num; i++) {
            let item = this.getItem(i);
            let node = item.node;
            node.active = true;
            item.init(this.data[i], i, this.p);
            if(this.show_action)item.showAction(i);
            let y = begin_y - this.item_offset * (1 - node._uiProps.uiTransformComp.anchorY);
            node.toY(y);
            begin_y -= this.item_offset;
        }
        //超出数据的隐藏掉
        for (; i < this.all_item.length; i++) {
            this.all_item[i].node.active = false;
        }
        this.circle_items = new CircleArray(this.all_item, num);
    }
    public getItemByIndex(index: number) {
        let new_index = index - this.begin_index;
        return this.circle_items.get(new_index);
    }

    protected _moveContent(deltaMove: Vec3, canStartBounceBack: boolean) {
        let adjustedMove = this._flattenVectorByDirection(deltaMove);

        Vec3.add(_tempVec3, this._content.position, adjustedMove); 
        //@ts-ignore
        this._setContentPosition(_tempVec3);

        if (this.all_item.length == this.circle_items?.getLen() && Math.abs(adjustedMove.y) > EPSILON) {
            this._checkNeedRefresh(adjustedMove);
        }
        // let outOfBoundary = this._getHowMuchOutOfBoundary();
        // log(outOfBoundary.x , outOfBoundary.y)
        // this._updateScrollBar(outOfBoundary);
        if (this.elastic && canStartBounceBack) {
            this._startBounceBackIfNeeded();
        }
    }

    protected _checkNeedRefresh(offset: Vec3) {
        if (this.circle_items.getLen() < 2) return;

       // console.log("滚动 检查刷新", offset)

        //元素向上滚动
        if (offset.y > 0 && this.end_index < this.data.length - 1 && this._content.position.y + this.circle_items.get(1).node.position.y > this._topBoundary) {
            
            this.begin_index++;
            this.end_index++;//数据索引增加

            let first_info = this.circle_items.get(0);
            let node_end = this.circle_items.getLast(1).node;
            // log("tobottom:",this.data[this.end_index])
            this.circle_items.addOffset(1);
            //更新最下面元素的数据
            first_info.init(this.data[this.end_index], this.end_index, this.p);
            //最上面的元素移动到最下面
            let y = node_end.position.y - this.item_offset;
            first_info.node.toY(y);
        }
        if (offset.y < 0 && this.begin_index > 0 && this._content.position.y + this.circle_items.getLast(2).node.position.y < this._bottomBoundary) {
            this.begin_index--;
            this.end_index--;
            let end_info = this.circle_items.getLast(1);
            let node_first = this.circle_items.get(0).node;
            // log("tobottom:",this.data[this.end_index])
            this.circle_items.addOffset(-1);
            //更新最下面元素的数据
            end_info.init(this.data[this.begin_index], this.begin_index, this.p);
            //最上面的元素移动到最下面
            let y = node_first.position.y + this.item_offset;
            end_info.node.toY(y);
        }
    }
    public resetInEditor(){
        if(this.horizontal && this.vertical)this.horizontal = false;
        if(!this._content){
            let node = this.node.children[0];
            if(!node)return;
            node = node.children[0];
            if(node)this._content = node;
        }
    }
}
