
import { _decorator, Component, Node, sys, view, NodeEventType } from 'cc';
import { EDITOR } from 'cc/env';
import { ExtraMgr } from '../manager/ExtraMgr';

const { ccclass, property, menu, executeInEditMode } = _decorator;

@ccclass('WidgetPro')
@menu("新组件/WidgetPro")
@executeInEditMode
export class WidgetPro extends Component {
    // public static half_height = 0;
    // public static half_width = 0;

    // public static refreshInfo(){
    //     console.log("getSafeAreaRect", sys.getSafeAreaRect())
    //     let safe_rect = sys.getSafeAreaRect();
    //     WidgetPro.half_height = safe_rect.height / 2;
    //     WidgetPro.half_width = safe_rect.width / 2;
    // }
    // public static getBottomOffset(){
    //     return view.getVisibleSize().height / 2 - 640;
    // }
    public static getFitArr(arr_pos: Array<any>, indexs:Array<number>){
        let arr = new Array(arr_pos.length);
        for(let i = 0 ; i < arr_pos.length ; i++){
            arr[i] = arr_pos[i].slice();
        }
        let half_height = ExtraMgr.fill_height / 2;
        for(let i = 0 ; i < indexs.length ; i++){
            let index = indexs[i];
            let offset = half_height - 640;
            if(arr[index][1] > 0)arr[index][1] += (offset + ExtraMgr.fill_bottom);
            else arr[index][1] -= (offset - ExtraMgr.fill_bottom);
        }
        return arr;
    }

    @property({serializable: true})
    private _fitHeight= false;
    @property({serializable: true})
    private _dis_height = 0;
    @property({serializable: true})
    private _fit_width = false;
    @property({serializable: true})
    private _dis_width = 0;

    @property({group:"height"})
    get fitHeight(){
        return this._fitHeight;
    }
    set fitHeight(v:boolean){
        if(this._fitHeight == v)return;
        this._fitHeight = v;
        if(EDITOR)this.refreshProp();
    }
    @property({group:"height"})
    get dis_height(){
        return this._dis_height;
    }
    set dis_height(v:number){
        if(this._dis_height == v)return;
        this._dis_height = v;
        if(EDITOR)this.refreshPos();
    }
    @property({group:"width"})
    get fitWidth(){
        return this._fit_width;
    }
    set fitWidth(v:boolean){
        if(this._fit_width == v)return;
        this._fit_width = v;
        if(EDITOR)this.refreshProp();
    }
    @property({group:"width"})
    get disWidth(){
        return this._dis_width;
    }
    set disWidth(v:number){
        if(this._dis_width == v)return;
        this._dis_width = v;
        if(EDITOR)this.refreshPos();
    }

    onLoad(){
        this.refreshPos();
        if(EDITOR)this.node.on(NodeEventType.TRANSFORM_CHANGED, this.refreshProp, this);
    }

    private refreshProp(){
        if(this._fitHeight){
            let y = this.node.position.y;
            this._dis_height = y > 0 ? y - 640 : 640 + y;
        }
        if(this._fit_width){
            let x = this.node.position.x;
            this._dis_width = x > 0 ? x - 360 : 360 + x;
        }
    }
    public refreshPos(){
        if(this._fitHeight){
            // let half_height = view.getVisibleSize().height / 2;
            let half_height = ExtraMgr.fill_height / 2;
            // console.log("half_height",half_height)
            if(this._dis_height > 0)this.node.toY(this._dis_height - half_height + ExtraMgr.fill_bottom);
            else this.node.toY(this._dis_height + half_height - ExtraMgr.fill_bottom);
        }
        if(this._fit_width){
            let half_width = view.getVisibleSize().width / 2;
            if(this._dis_width > 0)this.node.toX(this._dis_width - half_width);
            else this.node.toX(this._dis_width + half_width);
        }
    }
}
