
import { _decorator, animation, Component, js, Label, Node, Skeleton, sp, Toggle, ToggleContainer } from 'cc';
import { SplitInfo } from '../extra/ExClass';
import { GameConst } from '../game/GameConst';
import { UtilsCommon } from '../utils/UtilsCommon';
import { TogglesColor } from './TogglesColor';

const { ccclass, property, executionOrder } = _decorator;

@ccclass('ToggleBottom')
@executionOrder(99)
export class ToggleBottom extends Component {
    @property
    private need_split = true;

    private node_light: Node = null;
    private toggles: Array<Toggle> = null;
    private split_info: Array<SplitInfo> = null;

    private offsets_pos = null;
    private index_show: Array<boolean> = null;

    public target = null;
    public func: Function = null;

    protected onLoad(): void {
        this.node_light = this.node.getChildByName("node_light");
        // let index = this.node_light.getSiblingIndex();
        let nodes = new Array();
        let children = this.node.children;
        for(let i = 0 ; i < children.length ; i++){
            if(children[i].getComponent(Toggle))nodes.push(children[i]);
        }
        // let nodes = this.node.children.slice(0, index);
        this.index_show = new Array(nodes.length);
        this.toggles = new Array(nodes.length);
        let hander = UtilsCommon.createEventHandler(this.node, js.getClassName(this), "onToggles");
        this.node.getComponent(ToggleContainer).checkEvents.push(hander);
        for(let i = 0 ; i < nodes.length ; i++){
            this.index_show[i] = nodes[i].active;
            this.toggles[i] = nodes[i].getComponent(Toggle);
            if(this.toggles[i].isChecked)this.node_light.toX(nodes[i].position.x)
        }
        if(this.need_split){
            this.offsets_pos = SplitInfo.getOffsetByNode(nodes[0]);
            this.split_info = new Array(nodes.length);
            let offset = nodes[0].getSiblingIndex();
            for(let i = 0 ; i < nodes.length ; i++){
                this.split_info[i] = new SplitInfo(nodes[i]);
                this.split_info[i].offset = offset;
                nodes[i].setSiblingIndex(i + offset);
                this.split_info[i].setSlisByIndex(i);
                this.split_info[i].moveByOffset(this.offsets_pos);
                if(!nodes[i].active){
                    this.split_info[i].setActive(false);
                }
            }
        }
    }
    public init(func:Function, target:any){
        this.func = func;
        this.target = target;
    }
    public showIndexs(arr:Array<number>){
        for(let i = 0 ; i < this.index_show.length ; i++)this.index_show[i] = false;
        for(let i = 0 ; i < arr.length ; i++)this.index_show[arr[i]] = true;
        this.refresh();
    }
    public closeIndexs(arr:Array<number>){
        for(let i = 0 ; i < arr.length ; i++)this.index_show[arr[i]] = false;
        this.refresh();
    }
    private refresh(){
        let count = 0;
        for(let i = 0 ; i < this.index_show.length ; i++){if(this.index_show[i])count++;}
        let offset = 720 / count;
        let pos = GameConst.getOffsetArr(count, 0, offset);
        let cur_index = 0;
        let ani_name = "animation2";
        if(count == 2)ani_name = "animation1";
        else if(count == 3)ani_name = "animation3";
        this.node_light.getComponent(sp.Skeleton).setAnimation(0, ani_name, true);
        this.node_light._uiProps.uiTransformComp.width = offset;
        for(let i = 0 ; i < this.index_show.length ; i++){
            this.toggles[i].node.active = this.index_show[i];
            this.toggles[i].node._uiProps.uiTransformComp.width = offset;
            this.toggles[i].node.toX(pos[cur_index]);
            if(this.need_split){
                this.split_info[i].setActive(this.index_show[i]);
                this.split_info[i].moveByOffset(this.offsets_pos);
            }
            if(!this.index_show[i])continue;
            if(this.toggles[i].isChecked)this.node_light.toX(this.toggles[i].node.position.x)
            cur_index++;
        }
    }
    private onToggles(e:any, str:string){
        this.node_light.toX(e.node.position.x);
        if(this.func) this.func.call(this.target, e, str);
    }
    public selectToggle(index:number){
        this.toggles[index].isChecked = true;
        let toggle_color = this.node.getComponent(TogglesColor);
        if(toggle_color)toggle_color.onToggleChange();
    }
    public refreshRedPos(index:number, node:Node){
        let pos = this.toggles[index].node.position;
        node.toXY(pos.x + 30, 34);
    }
}
