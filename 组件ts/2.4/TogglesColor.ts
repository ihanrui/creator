const { ccclass, property } = cc._decorator;

/**
 * 切换Toggles时改变颜色
 * @author chenxj 2021.04.27
 */
@ccclass
export default class TogglesColor extends cc.Component {
    //#region property 资源

    /** !#zh selectedColor 选中颜色 */
    @property(cc.Color)
    public selectedColor: cc.Color = new cc.Color(255, 251, 129, 255);

    /** !#zh unselectedColor 未选中颜色 */
    @property(cc.Color)
    public unselectedColor: cc.Color = new cc.Color(83, 23, 16, 255);

    @property({ tooltip: "区分checkmark节点和描述节点是在同一层 还是子层 类型1 同一层 类型2子层" })
    public type = 1;
    //#endregion

    //#region 定义变量

    private toggle_container = null;

    //#endregion
    onLoad() {
        this.toggle_container = this.node.getComponent(cc.ToggleContainer);
    }

    start() {
        let node = this.node;
        if (this.type == 1) {
            this.toggle_container.toggleItems.forEach((item) => {
                let lb_name = item.name.substring(7, item.name.length - 8);
                item.node_lb = node.getChildByName(lb_name);
                item.lineout = item.node_lb.getComponent(cc.LabelOutline);
            });
        }
        else {
            this.toggle_container.toggleItems.forEach((item) => {
                let lb_name = item.name.substring(7, item.name.length - 8);
                item.node_lb = item.node.getChildByName(lb_name);
                item.lineout = item.node_lb.getComponent(cc.LabelOutline);
            });
        }
        let clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node; // 这个 node 节点是你的事件处理代码组件所属的节点
        clickEventHandler.component = "TogglesColor"; // 这个是代码文件名
        clickEventHandler.handler = "onToggleChange";
        this.toggle_container.checkEvents.push(clickEventHandler);
        this.onToggleChange();
    }

    private onToggleChange() {
        for (
            let i = 0, len = this.toggle_container.toggleItems.length;
            i < len;
            i++
        ) {
            let item = this.toggle_container.toggleItems[i];
            if (item.enabled && item.isChecked) {
                item.node_lb.color = this.selectedColor;
                if (item.lineout) item.lineout.enabled = true;
            } else {
                item.node_lb.color = this.unselectedColor;
                if (item.lineout) item.lineout.enabled = false;
            }
        }
    }
    // update (dt) {},
}
