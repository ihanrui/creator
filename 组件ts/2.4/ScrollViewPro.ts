// @ts-nocheck

const {ccclass, property} = cc._decorator;

import { ViewItemPro } from "./ViewItemPro"

const EPSILON = 1e-4;

const DO_NO = 1;
const PRE_CLEAR_OUT_BOUNDARY_FLAG = 1 << 1;
const CLEAR_OUT_BOUNDARY_FLAG = 1 << 2;

class ViewItemInfo {
    public node: cc.Node;
    public itemComponent: any;
    // public index: number;
}

function getTopHeight(node:cc.Node){
    return node.height * (1 - node.anchorY);
}
function getBottomHeight(node:cc.Node) {
    return node.height * node.anchorY;
}

@ccclass
export class ScrollViewPro extends cc.ScrollView {
    @property(cc.Prefab)
    public item_prefab: cc.Prefab = null;
    @property
    public item_num:number = 0;
    @property
    public item_height:number = 10;
    @property
    public fit_height:boolean = false;

    private state_flag:number = DO_NO;
    private begin_index:number = 0;
    private end_index:number = 0;
    private item_list:Array<ViewItemInfo> = [];
    private index_arr:Array<number> = [];
    private is_init = false;
    private data:any = null;
    private p1:any = null;

    // protected onLoad(){
    //     this.state_flag = DO_NO;
    //     this.begin_index = 0;
    //     this.end_index = 0;
    //     this.item_list = [];
    //     this.index_arr = [];
    //     this.is_init = false;
    // }

    private getItem(index:number){
        if(index < this.item_list.length)return this.item_list[index];
        else{
            if(index == this.item_list.length){
                let item = new ViewItemInfo();
                let node = cc.instantiate(this.item_prefab);
                item.node = node;
                item.itemComponent = node.getComponent(ViewItemPro);
                // item.index = index;
                if(this.fit_height){
                    let self = this;
                    node.on(cc.Node.EventType.SIZE_CHANGED, _ => {
                        self._onItemSizeChange(index);
                    });
                }
                node.parent = this.content;
                this.item_list.push(item);
                return this.item_list[index];
            }else{
                console.warn("getItem index not generate")
            }
        }
    }

    private _onItemSizeChange(item_index:number){
        if(this.is_init)return;
        let index = 0;
        for (let i = 0; i < this.index_arr.length; ++i) {
            if (this.index_arr[i] == item_index) {
                //找到元素在list孩子的索引
                index = i;
                break
            }
        }
        if(index > 0){
            index--;
        }
        let node = this.item_list[this.index_arr[index]].node;
        let empty_y = node.y - node.height * node.anchorY;
        for(let i = index + 1 ; i < this.index_arr.length ; i++){
            node = this.item_list[this.index_arr[i]].node;
            node.y = empty_y - node.height * (1 - node.anchorY);
            empty_y -= node.height;
        }
        // this._quickAllayOuterBoundary();
    }

    private _quickAllayOuterBoundary(){
        this._outOfBoundaryAmountDirty = true;
        let offset = this._getHowMuchOutOfBoundary();
        this.stopAutoScroll();
        if(!offset.fuzzyEquals(cc.v2(0, 0), EPSILON)){
            // this._moveContent(offset);
            let newPosition = this.getContentPosition().add(offset);
            this.setContentPosition(newPosition);
        }
    }

    public initData(data:Array<any> , p1:any=null){
        this.is_init = true;
        this.data = data;
        this.p1 = p1;
        let begin_y = 0;
        let num = Math.min(data.length , this.item_num);
        this.end_index = this.begin_index + num - 1;
        if(this.end_index > data.length - 1){
            let offset = this.end_index + 1 - data.length;
            this.end_index = data.length - 1;
            this.begin_index -= offset;
            let y = this.begin_index * this.item_height + this._topBoundary;
            if(this.begin_index < 0){
                this.begin_index = 0;
                this.content.y = y;
            }
            begin_y = -this.begin_index * this.item_height;
            if(!this.fit_height && this.begin_index > 0){
                let node = this.item_list[this.index_arr[0]].node;
                begin_y += node.height * node.anchorY;
            }

        }else{
            if(this.item_list.length > 0 && this.index_arr.length > 0){
                let node = this.item_list[this.index_arr[0]].node;
                begin_y = node.y + node.height * (1 - node.anchorY);
            }
        }
        this.initItems(begin_y , num);
        this.state_flag = PRE_CLEAR_OUT_BOUNDARY_FLAG;
        // this._calculateBoundary();
        this.is_init = false;
    }

    private initItems(begin_y:number , num:number){
        this.index_arr = [];
        let i = 0;
        let layout = null;
        for(; i < num ; i++){
            let item = this.getItem(i);
            let node = item.node;
            let index = this.begin_index + i;
            node.active = true;
            item.itemComponent.init(this.data[index] , index , this.p1);
            layout = node.getComponent(cc.Layout)
            if (layout) {
                layout.updateLayout();
            }
            node.y = begin_y - node.height * (1 - node.anchorY);
            begin_y -= node.height;
            this.index_arr.push(i);
        }
        for(; i < this.item_list.length ; i++){
            this.item_list[i].node.active = false;
        }
    }

    public getChildInfoByIndex(index:number){
        if(index >= this.begin_index && index <= this.end_index){
            let new_index = index - this.begin_index;
            return this.item_list[this.index_arr[new_index]];
        }
        return null;
    }

    protected _moveContent (deltaMove:cc.Vec2, canStartBounceBack:boolean) {
        let adjustedMove = this._flattenVectorByDirection(deltaMove);
      
        let newPosition = this.getContentPosition().add(adjustedMove);
        this.setContentPosition(newPosition);

        if(!adjustedMove.fuzzyEquals(cc.v2(0, 0), EPSILON)){
            this._checkNeedRefresh(adjustedMove);
        }

        // let outOfBoundary = this._getHowMuchOutOfBoundary();
        // cc.log(outOfBoundary.x , outOfBoundary.y)
       // this._updateScrollBar(outOfBoundary);
        if (this.elastic && canStartBounceBack) {
            this._startBounceBackIfNeeded();
        }
    }

    protected _checkNeedRefresh(offset:cc.Vec2){
        let len = this.index_arr.length;
        if(len < 2)return;
        if(offset.y > 0 && this.end_index < this.data.length - 1 && this.content.y + this.item_list[this.index_arr[1]].node.y > this._topBoundary){
            this.is_init = true;
            this.begin_index++;
            this.end_index++;
            let end_index = this.index_arr[len - 1];
            let first_index = this.index_arr.shift();
            this.index_arr.push(first_index);
            let first_info = this.item_list[first_index];
            let node_end = this.item_list[end_index].node;
            // cc.log("tobottom:",this.data[this.end_index])
            first_info.itemComponent.init(this.data[this.end_index] , this.end_index , this.p1);
            let layout = first_info.node.getComponent(cc.Layout)
            if (layout) {
                layout.updateLayout();
            }
            first_info.node.y = node_end.y - node_end.height * node_end.anchorY - first_info.node.height * (1 - first_info.node.anchorY); 
            this.is_init = false;
        }
        if(offset.y < 0 && this.begin_index > 0 && this.content.y + this.item_list[this.index_arr[len - 2]].node.y < this._bottomBoundary){
            this.is_init = true;
            this.begin_index--;
            this.end_index--;
            let first_index = this.index_arr[0];
            let end_index = this.index_arr.pop();
            let end_info = this.item_list[end_index];
            let node_first = this.item_list[first_index].node;
            // cc.log("totop:",this.data[this.begin_index])
            end_info.itemComponent.init(this.data[this.begin_index] , this.begin_index , this.p1);
            let layout = end_info.node.getComponent(cc.Layout)
            if (layout) {
                layout.updateLayout();
            }
            end_info.node.y = node_first.y + node_first.height * (1 - node_first.anchorY) + end_info.node.height * end_info.node.anchorY;
            this.index_arr.unshift(end_index);
            this.is_init = false;
        }
    }

    protected _getContentTopBoundary () {
        if(this.index_arr.length > 0){
            let node = this.item_list[this.index_arr[0]].node;
            let y = this.content.y + node.y + node.height * (1 - node.anchorY);
            if(this.begin_index > 0){
                y += this.item_height;
            }
            return y;
        }else{
            let contentSize = this.content.getContentSize();
            return this._getContentBottomBoundary() + contentSize.height;
        }
    }

    protected _getContentBottomBoundary () {
        if(this.index_arr.length > 0){
            let node = this.item_list[this.index_arr[this.index_arr.length - 1]].node;
            let y = this.content.y + node.y - node.height * node.anchorY;
            y = Math.min(y , this.content.y - this.content.height);
            return y;
        }else{
            let contentPos = this.getContentPosition();
            return contentPos.y - this.content.getAnchorPoint().y * this.content.getContentSize().height;
        }
    }

    protected _moveContentToTopLeft(viewSize:cc.Size){
        cc.log("_moveContentToTopLeft");
        if(this.index_arr.length == 0){
            cc.ScrollView.prototype._moveContentToTopLeft.call(this,viewSize);
        }
    }

    protected update (dt:number) {
        cc.ScrollView.prototype.update.call(this,dt);
        if(this.state_flag == DO_NO)return;
        if(this.state_flag == PRE_CLEAR_OUT_BOUNDARY_FLAG){
            this.state_flag = CLEAR_OUT_BOUNDARY_FLAG;
        }else if(this.state_flag == CLEAR_OUT_BOUNDARY_FLAG){
            // this._calculateBoundary();
            this._quickAllayOuterBoundary();
            this.state_flag = DO_NO;
        }
    }
}
