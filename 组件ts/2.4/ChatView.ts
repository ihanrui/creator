// @ts-nocheck

const {ccclass, property} = cc._decorator;

import { ViewItemPro } from "./ViewItemPro"

const EPSILON = 1e-4;

const DO_NO = 1;
const PRE_CLEAR_OUT_BOUNDARY_FLAG = 1 << 1;
const CLEAR_OUT_BOUNDARY_FLAG = 1 << 2;

class ViewItemInfo {
    public node: cc.Node;
    public itemComponent: any;
    public type: number;
    public _data_index: number;
}

function getTopHeight(node:cc.Node){
    return node.height * (1 - node.anchorY);
}
function getBottomHeight(node:cc.Node) {
    return node.height * node.anchorY;
}

@ccclass
export class ChatView extends cc.ScrollView {
    @property([cc.Prefab])
    public pres_item: cc.Prefab[] = [];
    @property
    public item_height = 10;

    private cache_pools: Array<Array<ViewItemInfo>> = [];
    private begin_index:number = 0;
    private end_index:number = 0;
    private item_list:Array<ViewItemInfo> = [];
    private data:Array<any> = [];
    private p1:any = null;
    private func2type: Function = null;
    private state: number = 0;
    private wait_frames = 0;

    private state_flag:number = DO_NO;

    protected onLoad(){
        this._calculateBoundary();
        for(let i = 0 , len = this.pres_item.length ; i < len ; i++){
            this.cache_pools.push([]);
        }
     
        // this.state_flag = DO_NO;
        // this.begin_index = 0;
        // this.end_index = 0;
        // this.item_list = [];
        // this.index_arr = [];
    }

    protected start () {
        // this._calculateBoundary();
        //Because widget component will adjust content position and scrollview position is correct after visit
        //So this event could make sure the content is on the correct position after loading.
        if (this.content) {
            cc.director.once(cc.Director.EVENT_BEFORE_DRAW, this._adjustContentOutOfBoundary, this);
        }
    }


    public setTypeFunc(func:Function){
        this.func2type = func;
    }

    private getItemByType(type:number){
        if(type < 0 || type >= this.cache_pools.length)return;
        if(type < this.cache_pools.length){
            let pool = this.cache_pools[type];
            if(pool.length <= 0){
                let item = new ViewItemInfo();
                let node = cc.instantiate(this.pres_item[type]);
                item.node = node;
                item.type = type;
                item.itemComponent = node.getComponent(ViewItemPro);
                node.parent = this.content;
                var self = this;
                node.on(cc.Node.EventType.SIZE_CHANGED, () => {
                    self._onItemSizeChange(item._data_index);
                });
                return item;
            }
            let item = pool.pop();
            item.node.active = true;
            return item;
        }
        return null;
    }

    private pushItemByType(type: number, item: ViewItemInfo){
        item.node.active = false;
        if(type > -1 && type < this.cache_pools.length){
            this.cache_pools[type].push(item);
        }
    }

    private recycleItems(){
        for(let i = 0 ; i < this.item_list.length ; i++){
            this.pushItemByType(this.item_list[i].type , this.item_list[i]);
        }
        this.item_list = [];
    }

    public getChildInfoByIndex(index: number){
        if(index >= this.begin_index && index <= this.end_index){
            var new_index = index - this.begin_index;
            return this.item_list[new_index];
        }
        return null;
    }

    private _onItemSizeChange(data_index:number){
        if(this.state != 1)return;
        let index = 0;
        for (let i = 0; i < this.item_list.length; ++i) {
            if (this.item_list[i]._data_index == data_index) {
                //找到元素在list孩子的索引
                index = i;
                break
            }
        }

        if(index > 0){
            index--;
        }
        let node = this.item_list[index].node;
        let empty_y = node.y - node.height * node.anchorY;
        for(let i = index + 1 ; i < this.item_list.length ; i++){
            node = this.item_list[i].node;
            node.y = empty_y - node.height * (1 - node.anchorY);
            empty_y -= node.height;
        }
    
        // this._quickAllayOuterBoundary();
    }

    private align2Top(){
        if(this.state != 2)return;
        let index = this.item_list.length - 1;
        let node = this.item_list[index].node;
        let bottom_empty = this.content.y + node.y - node.height * node.anchorY - this._bottomBoundary;
        let empty_y = node.y + node.height * (1 - node.anchorY);
        for(let i = index - 1 ; i >= 0 ; i--){
            node = this.item_list[i].node;
            node.y = empty_y + node.height * node.anchorY;
            empty_y += node.height;
        }
        let first_node = this.item_list[0].node;
        let top_over = this.content.y + first_node.y + first_node.height * (1 - first_node.anchorY) - this._topBoundary;
        if(bottom_empty > 0 && top_over > 0){
            let drop = Math.min(bottom_empty , top_over);
            this.item_list.forEach((item:ViewItemInfo)=>{
                item.node.y -= drop;
            })
        }    
        // cc.log(top_over);
        // cc.log(bottom_empty);
        // if(data_index == this.data.length - 1)
        this.state = 1;
    }

    private _moveContent (deltaMove, canStartBounceBack) {
        let adjustedMove = this._flattenVectorByDirection(deltaMove);
      
        let newPosition = this.getContentPosition().add(adjustedMove);
        this.setContentPosition(newPosition);

        this._checkNeedRefresh(adjustedMove);

        let outOfBoundary = this._getHowMuchOutOfBoundary();
        // cc.log(outOfBoundary.x , outOfBoundary.y)
        // this._updateScrollBar(outOfBoundary);
        if (this.elastic && canStartBounceBack) {
            this._startBounceBackIfNeeded();
        }
    }

    private geneOneNode(index: number){
        let type = this.func2type ? this.func2type(this.data[index]) : 0;
        var item = this.getItemByType(type);
        item._data_index = index;
        item.itemComponent.init(this.data[index] , index , this.p1);
        return item;
    }

    private _checkNeedRefresh(offset){
        this.state = 0;
        var list = this.item_list;
        var content_y = this.content.y;
        if(offset.y > 0 && this.end_index < this.data.length - 1){
            if(list.length > 1 && list[1].node.y + content_y > this._topBoundary){
                var item = list.shift();
                this.pushItemByType(item.type , item);
                this.begin_index++;
            }
            let len = list.length;
            if(len > 1 && list[len-2].node.y + content_y > this._bottomBoundary){
                this.end_index++;
                var item = this.geneOneNode(this.end_index);
                let end_node = list[len - 1].node;
                let y = end_node.y - end_node.height *(1 - end_node.anchorY);
                item.node.y = y;
                list.push(item);
            }
        }
        else if(offset.y < 0 && this.begin_index > 0){
            if(list.length > 1 && list[1].node.y + content_y < this._topBoundary){
                this.begin_index--;
                var item = this.geneOneNode(this.begin_index);
                let layout = item.node.getComponent(cc.Layout);
                if (layout) {
                    layout.updateLayout();
                }
                let first_node = list[0].node;
                let y = first_node.y + first_node.height;
                item.node.y = y;
                this.item_list.unshift(item);
            }
            let len = this.item_list.length;
            if(len > 1 && list[len-2].node.y + content_y < this._bottomBoundary){
                var item = this.item_list.pop();
                this.pushItemByType(item.type , item);
                this.end_index--;
            }
        }
        this.state = 1;
    }

    private _quickAllayOuterBoundary(){
        this._outOfBoundaryAmountDirty = true;
        let offset = this._getHowMuchOutOfBoundary();
        this.stopAutoScroll();
        if(!offset.fuzzyEquals(cc.v2(0, 0), EPSILON)){
            // this._moveContent(offset);
            let newPosition = this.getContentPosition().add(offset);
            this.setContentPosition(newPosition);
        }
    }

    public initData(data:Array<any> , p1:any=null){
        this.state = 0;
        this.recycleItems();
        this.data = data;
        this.p1 = p1;
        this.begin_index = this.end_index = data.length - 1;;
        // this.content.y = this._topBoundary;
        this.initItems();
        // this._quickAllayOuterBoundary();
    }

    public changeData(data:any){
        if(this.end_index < this.data.length - 1){
            this.data = data;
        }else{
            this.initData(data);
        }
    }

    private initItems(){
        let begin_y = this.data.length * this.item_height;
        if(begin_y > this.content.height){
            this.content.y = this._topBoundary + begin_y - this.content.height;
        }else{
            this.content.y = this._topBoundary;
        }
        begin_y = -begin_y;
        let total_height = 0;
        let is_full = false;
        for(let i = this.data.length - 1; i >= 0 ; i--){
            let item = this.geneOneNode(i);
            let node = item.node;
            this.item_list.unshift(item);
            // item._data_index = i;
            // item.itemComponent.init(this.data[i] , i , this.p1);
            node.y = begin_y + node.height * node.anchorY;
            begin_y += node.height;
            if(is_full)break;
            total_height += node.height;
            this.begin_index--;
            if(total_height > this.content.height){
                is_full = true;
            }
        }
        this.wait_frames = 0;
        this.state = 2;
    }

    protected _getContentTopBoundary () {
        if(this.item_list.length > 0){
            let node = this.item_list[0].node;
            let y = this.content.y + node.y + node.height * (1 - node.anchorY);
            if(this.begin_index > 0){
                y += this.item_height;
            }
            return y;
        }else{
            let contentSize = this.content.getContentSize();
            return this._getContentBottomBoundary() + contentSize.height;
        }
    }

    protected _getContentBottomBoundary () {
        if(this.item_list.length > 0){
            var node = this.item_list[this.item_list.length - 1].node;
            var y = Math.min(this.content.y + node.y - node.height * node.anchorY , this.content.y - this.content.height)
            return y;
        }else{
            let contentPos = this.getContentPosition();
            return contentPos.y - this.content.getAnchorPoint().y * this.content.getContentSize().height;
        }
    }

    protected _moveContentToTopLeft(viewSize:cc.Size){
        cc.log("_moveContentToTopLeft");
        // if(this.index_arr.length == 0){
        //     cc.ScrollView.prototype._moveContentToTopLeft.call(this,viewSize);
        // }
    }

    protected update (dt:number) {
        cc.ScrollView.prototype.update.call(this,dt);
        if(this.state == 2){
            this.wait_frames++;
            if(this.wait_frames >= 3){
                this.align2Top();
                this.state = 1;
            }
        }
        // if(this.state_flag == DO_NO)return;
        // if(this.state_flag == PRE_CLEAR_OUT_BOUNDARY_FLAG){
        //     this.state_flag = CLEAR_OUT_BOUNDARY_FLAG;
        // }else if(this.state_flag == CLEAR_OUT_BOUNDARY_FLAG){
        //     // this._calculateBoundary();
        //     this._quickAllayOuterBoundary();
        //     this.state_flag = DO_NO;
        // }
    }
}
