

import { ViewItemPro } from "./ViewItemPro";

const Dir = cc.Enum({
    ToTop: 0,
    ToBottom: 1,
    ToLeft: 2,
    ToRight: 3,
})

const {ccclass, property} = cc._decorator;

@ccclass
export default class LoopPlay extends cc.Component {

    @property(cc.Prefab)
    public pre_show: cc.Prefab = null;

    @property({type:Dir})
    public m_dir = Dir.ToTop;

    @property
    public speed: number = 0;

    private m_data = [];
    private m_list: Array<ViewItemPro> = [];
    private max_count: number = 0;
    private boundary: number = 0;
    private change_pos: number = 0;
    private state: number = 0;
    private cur_index: number = 0;


    public initData(data:Array<any>){
        if(data.length == 0)return;
        this.m_data = data;
        let node = cc.instantiate(this.pre_show);
        this._calculateBoundary(node);
        this.cur_index = 0;
        this.setNode(node);
        this.state = 1;
    }

    private _calculateBoundary(node:cc.Node){        
        let count = 0
        switch(this.m_dir){
            case Dir.ToTop:
                count = Math.ceil(this.node.height / node.height);
                this.boundary = this.node.height * (1 - this.node.anchorY);
                this.change_pos = this.boundary + node.height * node.anchorY;
                break;
            case Dir.ToBottom:
                count = Math.ceil(this.node.height / node.height);
                this.boundary = -this.node.height * this.node.anchorY;
                this.change_pos = this.boundary - node.height * (1 - node.anchorY); 
                break;
            case Dir.ToLeft:
                count = Math.ceil(this.node.width / node.width);
                this.boundary = -this.node.width * this.node.anchorX;
                this.change_pos = this.boundary - node.width * (1 - node.anchorX); 
                break;
            case Dir.ToRight:
                count = Math.ceil(this.node.width / node.width);
                this.boundary = this.node.width * (1 - this.node.anchorX);
                this.change_pos = this.boundary + node.width * node.anchorX; 
                break;
        }
        this.max_count = count + 1;
        // cc.log("bounday:",this.boundary);
        // cc.log("changePos:",this.change_pos);
        // cc.log("count:",this.max_count);
    }

    private setNode(node: cc.Node){
        node.parent = this.node;
        this.m_list.push(node.getComponent(ViewItemPro));
        this.m_list[this.cur_index].init(this.m_data[this.cur_index], this.cur_index);
        switch(this.m_dir){
            case Dir.ToTop:
                node.x = 0;
                node.y = this.boundary - node.height * this.cur_index - node.height * (1 - node.anchorY);
                break;
            case Dir.ToBottom:
                node.x = 0;
                node.y = this.boundary + node.height * this.cur_index + node.height * node.anchorY;
                break;
            case Dir.ToLeft:
                node.y = 0;
                node.x = this.boundary + node.width * this.cur_index + node.width * node.anchorX;
                break;
            case Dir.ToRight:
                node.y = 0;
                node.x = this.boundary - node.width * this.cur_index - node.width * (1 - node.anchorX);
                break;
    
        }
        this.cur_index++;
    }

    private dealNewNode(i:number){
        let new_index = (i + this.max_count - 1) % this.m_list.length;
        let node = this.m_list[new_index].node;
        this.m_list[i].init(this.m_data[this.cur_index] , this.cur_index);
        this.cur_index++;
        if(this.cur_index >= this.m_data.length)this.cur_index = 0;
        return node;
    }

    protected update(dt:number){
        if(this.state == 1){
            if(this.m_list.length < this.max_count && this.cur_index < this.m_data.length){
                let node = cc.instantiate(this.pre_show);
                this.setNode(node);
                if(this.m_list.length >= this.max_count){
                    this.state = 2;
                }
            }
        }else if(this.state == 2){
            let dis = this.speed * dt;
            switch(this.m_dir){
                case Dir.ToTop:
                    for(let i = 0 , len = this.m_list.length ; i < len ; i++){
                        this.m_list[i].node.y += dis;
                        if(this.m_list[i].node.y > this.change_pos){
                            let front_node = this.dealNewNode(i);
                            this.m_list[i].node.y = front_node.y - front_node.height;
                        }
                    }
                break;
                case Dir.ToBottom:
                    for(let i = 0 , len = this.m_list.length ; i < len ; i++){
                        this.m_list[i].node.y -= dis;
                        if(this.m_list[i].node.y < this.change_pos){
                            let front_node = this.dealNewNode(i);
                            this.m_list[i].node.y = front_node.y + front_node.height;
                        }
                    }
                break;
                case Dir.ToRight:
                    for(let i = 0 , len = this.m_list.length ; i < len ; i++){
                        this.m_list[i].node.x += dis;
                        if(this.m_list[i].node.x > this.change_pos){
                            let front_node = this.dealNewNode(i);
                            this.m_list[i].node.x = front_node.x - front_node.width;
                        }
                    }
                break;
                case Dir.ToLeft:
                    for(let i = 0 , len = this.m_list.length ; i < len ; i++){
                        this.m_list[i].node.x -= dis;
                        if(this.m_list[i].node.x < this.change_pos){
                            let front_node = this.dealNewNode(i);
                            this.m_list[i].node.x = front_node.x - front_node.width;
                        }
                    }
                break;
            }
        }

        
    }
}
