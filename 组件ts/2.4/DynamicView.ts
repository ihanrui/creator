
import { ResPool } from "../games/ResPool";
import { DynamicItem } from "./DynamicItem";


const { ccclass, property } = cc._decorator;

@ccclass
export class DynamicView extends cc.Component {
    @property(cc.Prefab)
    public pre_item: cc.Prefab = null;

    @property({ tooltip: "最大数量" })
    public max_num = 10;
    @property({ tooltip: "方向0123: 上下左右" })
    public direction = 3;

    @property
    public item_width = 0;
    @property
    public item_height = 0;
    @property
    public item_anchor = 0.5;
    @property
    public space = 0;

    public m_items: Array<DynamicItem> = [];
    public m_pool: ResPool<DynamicItem> = null;
    private m_data: any = null;

    public onLoad() {
        this.m_pool = new ResPool<DynamicItem>("DynamicItem", this.pre_item, 2);
    }

    public initData(data: Array<any>) {
        this.m_data = data;
        let items_len = this.m_items.length;
        let len = Math.min(data.length, this.max_num);
        this.updateSize(len);
        let need_add = len - this.m_items.length;
        if (need_add > 0) {
            for (let i = 0; i < need_add; i++) {
                let item = this.m_pool.alloc();
                item.node.setParent(this.node);
                this.m_items.push(item);
            }
            this.updateItems(0, len);
        }
        else {
            need_add = -need_add;
            for (let i = items_len - 1; i >= items_len + need_add; i--) {
                this.m_pool.recycle(this.m_items[i]);
            }
            this.m_items.length -= need_add;
            this.updateItems(0, this.m_data.length);
        }
    }

    public addData(data: Array<any>) {
        this.m_data = data;
        let item_len = this.m_items.length;
        if (item_len < this.max_num) {
            let len = Math.min(data.length, this.max_num);
            this.updateSize(len);
        }
        let is_max = item_len == this.max_num;
        for (let i = 0; i < item_len; i++) {
            let front = is_max ? i - 1 : i;
            let pos = this.getPosByIndex(front);
            if (this.direction > 1) {
                let y = this.m_items[i].node.y;
                if (i < item_len - 1)
                    this.m_items[i].move(pos, y, null, null);
                else
                    this.m_items[i].move(pos, y, this.addActionItem, this);
            }
            else {
                let x = this.m_items[i].node.x;
                if (i < item_len - 1)
                    this.m_items[i].move(x, pos, null, null);
                else
                    this.m_items[i].move(x, pos, this.addActionItem, this);
            }
        }
    }
    public addActionItem() {
        if (this.m_items.length == this.max_num) {
            this.m_pool.recycle(this.m_items[0]);
            this.m_items.splice(0, 1);
        }
        let item = this.m_pool.alloc();
        item.node.setParent(this.node);
        item.init(this.m_data[this.m_data.length - 1]);
        let pos = this.getPosByIndex(this.m_data.length - 1);
        if (this.direction > 1) {
            item.action(pos, item.node.y);
        }
        else {

            item.action(item.node.x, pos);
        }
        this.m_items.push(item);
    }
    public updateSize(len: number) {
        if (this.direction > 1) {
            this.node.width = this.item_width * len + this.space * (len - 1);
        }
        else {
            this.node.height = this.item_height * len + this.space * (len - 1);
        }
    }

    private updateItems(begin: number, end: number) {
        for (let i = begin; i < end; i++) {
            this.m_items[i].init(this.m_data[i], i);
            let node = this.m_items[i].node;
            node.active = true;
            if (this.direction > 1) {
                node.x = this.getPosByIndex(i);
            }
            else {
                node.y = this.getPosByIndex(i);
            }
        }
    }

    public getPosByIndex(index: number) {
        let space = this.space * index;
        let pos = 0;
        switch (this.direction) {
            case 0:
                pos = (1 - this.node.anchorY) * this.node.height + (index + this.item_anchor) * this.item_height;
            case 1:
                pos = (1 - this.node.anchorY) * this.node.height - (index + this.item_anchor) * this.item_height;
            case 2:
                pos = (1 - this.node.anchorX) * this.node.width - (index + this.item_anchor) * this.item_width;
            case 3:
                pos = -this.node.anchorX * this.node.width + (index + this.item_anchor) * this.item_width;
        }
        return pos + space;
    }
    // private _getLeftBoundary () {
    //     return -this.node.anchorX * this.node.width;
    // }
    // private _getBottomBoundary () {
    //     return -this.node.anchorY * this.node.height;
    // }
    // private _getRightBoundary () {
    //     return (1 - this.node.anchorX) * this.node.width;
    // }
    // private _getTopBoundary () {
    //     return (1 - this.node.anchorY) * this.node.height;
    // }
}
