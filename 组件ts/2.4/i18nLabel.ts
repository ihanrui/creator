import { i18nMgr } from "../manager/i18nMgr";

const { ccclass, property, executeInEditMode, disallowMultiple, requireComponent, menu } = cc._decorator;

@ccclass
@executeInEditMode
// @requireComponent(cc.Label)
@disallowMultiple
@menu("多语言/i18nLabel")
export class i18nLabel extends cc.Component {

    @property({ visible: false })
    private i18n_index: number = 1;

    @property({ visible: false })
    private i18n_params: string[] = [];
 
    start() {
        if (this.bundle) {
            i18nMgr.addBundleLabel(this, this.bundle, true)
        } else {
            i18nMgr._addOrDelLabel(this, true);
        }

        this._resetValue();
    }

    @property()
    bundle = ""

    stringFunc=null;

    @property()
    get index(){
        return this.i18n_index
    }
    set index(i){
        this.i18n_index=i
        this.setEndValue()
    }

    setStringFunc(func){
        this.stringFunc=func
    }

    setString(i,params){
        this.i18n_index=i
        this.i18n_params=params
        this.setEndValue()
    }

    // init(string: string, params: string[]) {
    //     this.i18n_params = params;

    //     this.setEndValue()
    // }

    private setEndValue() {//有参数的时候，要替换处理 
            if (this.bundle) {
                if(CC_EDITOR){
                    let p=i18nMgr.getBundleString(this.i18n_index, this.bundle, this.i18n_params)
                    if(typeof p === 'string' || p instanceof String){
                        this.changeLabel(p)
                    }else{
                        p.then(_=>{
                            let s=i18nMgr.getBundleString(this.i18n_index, this.bundle, this.i18n_params)
                            this.changeLabel(s)
                        })
                    }
                }else{
                    let s = i18nMgr.getBundleString(this.i18n_index, this.bundle, this.i18n_params)
                    this.changeLabel(s)
                }
            } else {
                let s = i18nMgr.getString(this.i18n_index, this.i18n_params);
                this.changeLabel(s)
            }
    }

    changeLabel(str){
        if(this.stringFunc){
            str=this.stringFunc(str)
        }

        let com=this.node.getComponent(cc.Label)
        if(!com){
            com=this.node.getComponent(cc.RichText)
        }
        com.string=str
    }

    _resetValue(){
        this.setEndValue()
    }

    onDestroy() {
        if (this.bundle) {
            i18nMgr.addBundleLabel(this, this.bundle, false)
        }else{
            i18nMgr._addOrDelLabel(this, false);
        }
    }
}

