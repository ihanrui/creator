
const {ccclass, property} = cc._decorator;

@ccclass
export default class ToggleSide extends cc.Component {

    editor: {
        requireComponent: cc.Button,
    }


    @property(cc.Node)
    checkmark: cc.Node = null;
    @property(cc.Sprite)
    slide: cc.Sprite = null 
    @property
    slide_width: number = 0
    @property
    is_checked: boolean = false
    @property([cc.Component.EventHandler])
    clickEvents: cc.Component.EventHandler[] = []

    @property([cc.SpriteFrame])
    imgArr: cc.SpriteFrame[] = []

    cur_tween = null
    call_func = null

    onLoad () {
        if(!this.node.getComponent(cc.Button))this.node.addComponent(cc.Button);
    }
    
    start(){
        this.checkmark.active = this.is_checked;
        this.slide.node.x = this.is_checked ? this.slide_width : -this.slide_width;

        this.slide.spriteFrame = this.is_checked ? this.imgArr[1] : this.imgArr[0]
    }

    onEnable() {
        this.node.on('click', this.toggle, this);
    }

    onDisable() {
        this.node.off('click', this.toggle, this);
    }

    setFunc(func){
        this.call_func = func;
    }

    setCheck(state , need_call){
        this.is_checked = state;
        this._showAction();
        if(need_call){
            this._emitToggleEvents();
        }
    }

    toggle(){
        this.is_checked = !this.is_checked;
        this._emitToggleEvents();
        this._showAction();
       
    }

    _emitToggleEvents() {
        if(this.call_func)this.call_func(this.is_checked);
        if (this.clickEvents) {
            cc.Component.EventHandler.emitEvents(this.clickEvents, this);
        }
    }
    _showAction(){
        this.checkmark.active = this.is_checked;
        if(this.cur_tween)this.cur_tween.stop();
        if(this.is_checked){
            this.cur_tween = cc.tween(this.slide.node)
                                .to(0.1 , {x: this.slide_width})
                                .call(()=>{
                                    this.cur_tween = null;
                                    this.slide.spriteFrame = this.imgArr[1]
                                })
                                .start();
        }else{
            this.cur_tween = cc.tween(this.slide.node)
                                .to(0.1 , {x: -this.slide_width})
                                .call(()=>{
                                    this.cur_tween = null;
                                    this.slide.spriteFrame = this.imgArr[0]
                                })
                                .start();
        }
    }
}
