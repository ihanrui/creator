


const {ccclass, property} = cc._decorator;

// @Editor({
//     menu: 'i18n:MAIN_MENU.component.ui/Generator',
//     executeInEditMode: false,
// })
@ccclass
export class Generator extends cc.Component {

    @property(cc.Prefab)
    public pre_item: cc.Prefab = null;

    @property({tooltip:"第一帧加载个数"})
    public first_add_num = 1;
    @property({tooltip:"每帧加载个个数"})
    public every_frame_add = 1;
    @property({tooltip:"每行最多个数"})
    public horizontal_num = 1;
    @property({tooltip:"是否需要计算子节点位置"})
    public auto_align_child = true;
    @property({tooltip:"1 高度=item行数*高度\n2:1的基础上宽度=列数*宽度"})
    public change_size_mode = 1;
    @property
    public split_child = false;

    private m_data:any = null;
    private item_list: Array<any> = [];
    private state:number = 0;
    private have_init = false;
    private first_node: cc.Node = null;
    private p: any = null;
    private item_width: number = 0;
    private item_height: number = 0;
    private offset_x: number = 0;
    private offset_y: number = 0;

    private offsets: Array<cc.Vec2> = [];
    private all_childs: Array<Array<cc.Node>> = [];
    // protected onLoad () {
    //     this.m_data = null;
    //     this.item_list = [];
    //     this.state = 0;
    //     this.have_init = false;
    //     this.first_node = null;
    // }

    public initData(data:any , p:any=null){
        this.m_data = data;
        this.p = p;
        if(!data || data.length == 0){
            this.item_list.forEach(element => {
                element.node.active = false;
            });
            return;
        }
        this.initParams();
        this.updateSize();
        if(this.change_size_mode > 0 && this.auto_align_child){
            this.updateItemsPos();
        }
        let items_len = this.item_list.length;
        let need_add = data.length - items_len;
        if(need_add > 0){
            let need_add = data.length - items_len;
            need_add = Math.min(need_add , this.first_add_num);
            this.addItems(items_len , items_len + need_add);
            this.updateItems(0 , this.item_list.length);
            this.state = 1;
        }else{
            if(need_add < 0)
                for(let i = items_len - 1 ; i >= items_len + need_add ; i--){
                    this.item_list[i].node.active = false;
                }
            this.updateItems(0 , this.m_data.length);
        }
    }

    private initParams(){
        if(!this.have_init){
            let node = cc.instantiate(this.pre_item);
            this.item_width = node.width;
            this.item_height = node.height;
            this.offset_x = node.anchorX * node.width;
            this.offset_y = (1 - node.anchorY) * node.height;
            this.first_node = node;
            if(this.split_child){
                this.offsets.length = this.first_node.childrenCount;
                let childrens = this.first_node.children;
                childrens.forEach((node:cc.Node,index:number)=>{
                    this.offsets[index] = node.getPosition();
                })
            }
            this.have_init = true;
        }
    }

    private updateSize(){
        let len = this.m_data.length;
        let row = Math.ceil(len / this.horizontal_num);
        if(this.change_size_mode == 2){
            let col = len > this.horizontal_num ? this.horizontal_num : len;
            this.node.setContentSize(col * this.item_width , row * this.item_height);
        }else if(this.change_size_mode == 1){
            this.node.height = row * this.item_height;
        }
    }

    private addItems(begin:number , end:number){
        let begin_x = this._getLeftBoundary();
        let begin_y = this._getTopBoundary();
        let row = 0 , col = 0;
        for(let i = begin ; i < end ; i++){
            let node = i == 0 ? this.first_node : cc.instantiate(this.pre_item);
            this.item_list.push(node.getComponent("ViewItemPro"));
            node.parent = this.node;
            if(this.auto_align_child){
                row = Math.floor(i / this.horizontal_num);
                col = i % this.horizontal_num; 
                node.x = begin_x + col * this.item_width + this.offset_x;
                node.y = begin_y - row * this.item_height - this.offset_y;
            }
        }
        if(this.split_child){
            for(let i = begin ; i < end ; i++){
                let count = this.item_list[i].node.childrenCount;
                this.all_childs.push(new Array(count));
                let com_node = this.item_list[i].node;
                let children = com_node.children;
                let base_pos = com_node.getPosition();
                com_node.setSiblingIndex(i);
                let j = 0;
                while(children.length > 0){
                    let node = children[0];
                    this.all_childs[i][j] = node;
                    let new_index = (j + 1) * (i+1) + i;
                    this.node.insertChild(node , new_index);
                    node.setPosition(base_pos.add(this.offsets[j]));
                    j++;
                }
            }
            // cc.log(this.node.children)
        }
    }

    private updateItemsPos(){
        if(this.item_list.length == 0)return;
        let begin_x = this._getLeftBoundary();
        let begin_y = this._getTopBoundary();
        let row = 0 , col = 0;
        for(let i = 0 , len = this.item_list.length ; i < len ; i++){
            let node = this.item_list[i].node;
            row = Math.floor(i / this.horizontal_num);
            col = i % this.horizontal_num; 
            node.x = begin_x + col * this.item_width + this.offset_x;
            node.y = begin_y - row * this.item_height - this.offset_y;
        }
    }

    private updateItems(begin:number , end:number){
        for(let i = begin ; i < end ; i++){
            this.item_list[i].node.active = true;
            this.item_list[i].reset();
            this.item_list[i].init(this.m_data[i] , i , this.p);
        }
    }

    private _getLeftBoundary () {
        return -this.node.anchorX * this.node.width;
    }
    private _getBottomBoundary () {
        return -this.node.anchorY * this.node.height;
    }
    private _getRightBoundary () {
        return (1 - this.node.anchorX) * this.node.width;
    }
    private _getTopBoundary () {
        return (1 - this.node.anchorY) * this.node.height;
    }

    public getItems(){
        return this.item_list;
    }
    public getItemById(index:number){
        return this.item_list[index];
    }

    protected update (dt:number) {
        if(this.state == 0)return;
        if(this.item_list.length >= this.m_data.length){
            this.state = 0;
            // cc.log(this.node.children)
            this.node.emit("load_end");
        }else{
            let add = Math.min(this.every_frame_add , this.m_data.length - this.item_list.length);
            let items_len = this.item_list.length;
            this.addItems(items_len , items_len + add);
            this.updateItems(items_len , items_len + add);
        }
    }

}
