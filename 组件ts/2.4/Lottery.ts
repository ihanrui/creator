
const {ccclass, property} = cc._decorator;

@ccclass
export default class Lottery extends cc.Component {

    cur_select: number;

    begin_x: number = -408;
    begin_y: number = 162;
    offset_x: number = 204.5;
    offset_y: number = 130;
    col: number = 5;
    row: number = 4;
    //加速时间
    begin_times: number[];
    //减速时间
    end_times: number[];
    //所有路径点
    all_pos: number[][];

    //减速地方得索引
    cut_speed_index: number;

    cur_time_index: number;
    action_stage: number;

    call_func: Function;
    onLoad () {
        this.cur_select = 0;
        this.begin_times = [0.3,0.25,0.2,0.1];
        this.end_times = [0.15,0.2,0.3];
        this.all_pos = [];
        let right_x = this.begin_x + this.offset_x * (this.col - 1);
        let bottom_y = this.begin_y - this.offset_y * (this.row - 1);
        for(let i = 0 ; i < this.col ; i++){
            this.all_pos.push([this.begin_x + this.offset_x * i , this.begin_y]);
        }
        for(let i = 1 ; i < this.row - 1 ; i++){
            this.all_pos.push([right_x , this.begin_y - i * this.offset_y]);
        }
        for(let i = this.col - 1 ; i >= 0 ; i--){
            this.all_pos.push([this.begin_x + this.offset_x * i , bottom_y]);
        }
        for(let i = this.row - 2 ; i >= 0 ; i--){
            this.all_pos.push([this.begin_x , this.begin_y - this.offset_y * i]);
        }
    }

    runToPos(index , func){
        this.call_func = func;
        this.cut_speed_index = index - this.end_times.length;
        if(this.cut_speed_index < 0)this.cut_speed_index = this.cut_speed_index + this.all_pos.length - 1;
        this.cur_time_index = 0;
        this.action_stage = 1;
        this.showAction();
    }

    showAction(){
        this.cur_select++;
        if(this.cur_select > this.all_pos.length - 2)this.cur_select = 0;
        this.node.x = this.all_pos[this.cur_select][0];
        this.node.y = this.all_pos[this.cur_select][1];

        let time = 0;
        if(this.action_stage == 1){
            time = this.begin_times[this.cur_time_index];
            this.cur_time_index++;
            if(this.cur_time_index == this.begin_times.length){
                this.action_stage = 2;
                this.cur_time_index = 0;
            }
        }else if(this.action_stage == 2){
            time = 0.05;
            this.cur_time_index++;
            if(this.cur_time_index > 15){
                if(this.cur_select == this.cut_speed_index){
                    this.action_stage = 3;
                    this.cur_time_index = 0;
                }
            }
        }else if(this.action_stage == 3){
            if(this.cur_time_index == this.end_times.length - 1){
                this.action_stage = 0;
                if(this.call_func)this.call_func();
                return;
            }
            time = this.end_times[this.cur_time_index];
            this.cur_time_index++;
        }
        cc.tween(this.node)
            .delay(time)
            .call(this.showAction.bind(this))
            .start();
    }

    start () {

    }

    // update (dt) {}
}
