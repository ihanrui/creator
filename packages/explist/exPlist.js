
const { throws } = require("assert");
const fs = require("fs");
const path = require("path");

const PlistPath = "../../assets/master/texture/ui/bag/bag.plist.meta";
const TexturesPath = "../../assets/master/texture/ui/bag/";
const PrefabsPath = "../../assets/master/prefab/ui/bag/";

const CurMapPath = "bag.txt";
const TotalMapPath = "allmap.txt";

const NeedDel = true;
const Cmd = 1;

const need_path = new Array();
function getAllPath(parent, suffixs, need_name){
    let all_path = fs.readdirSync(parent);
    for(let i = 0 ; i < all_path.length ; i++){
        let final_path = path.join(parent, all_path[i]);
        if(fs.statSync(final_path).isDirectory()){
            getAllPath(final_path, suffixs, need_name);
        }
        else{
            let pass = false;
            for(let j = 0 ; j < suffixs.length ; j++){
                if(all_path[i].endsWith(suffixs[j])){
                    pass = true;
                    // break;
                }
            }
            if(pass){
                if(need_name)need_path.push(all_path[i].split(".")[0]);
                need_path.push(final_path);
            }
        }
    }
}
function getMap(data_plist){
    let name_map = new Map();
    for(let k in data_plist.subMetas){
        let ele = data_plist.subMetas[k];
        name_map.set(ele.name, ele);
    }
    // console.log(name_map);
    let result = new Map();
    for(let i = 0 ; i < need_path.length ; i += 2){
        let item = name_map.get(need_path[i]);
        if(!item)continue;
        let data = JSON.parse(fs.readFileSync(need_path[i+1], {encoding: "utf-8"}));
        let ele_need = null;
        for(let k in data.subMetas){
            let ele = data.subMetas[k];
            if(ele.importer == "sprite-frame")ele_need = ele;
        }
        if(ele_need){
            result.set(ele_need.uuid, item.uuid);
            item.borderTop = ele_need.borderTop;
            item.borderBottom = ele_need.borderBottom;
            item.borderLeft = ele_need.borderLeft;
            item.borderRight = ele_need.borderRight;
        }
        if(NeedDel){
            console.log("del:",need_path[i+1].slice(0, -5));
            fs.rmSync(need_path[i+1], {force:true});
            fs.rmSync(need_path[i+1].slice(0, -5), {force:true});
        }
    }
    return result;
}
function getMapByTxt(){
    let str = fs.readFileSync(CurMapPath, {encoding: "utf-8"});
    if(!str.length){
        throws(null, "没有映射文件");
    }
    let strs = str.split("\n");
    let result = new Map();
    for(let i = 0 ; i < strs.length ; i++){
        let ones = strs[i].split("=>");
        if(ones.length == 2){
            let key = ones[0].trim();
            let v = ones[1].trim();
            if(key && v)result.set(key, v);
        }
    }
    return result;
}

function changePrefab(uuid_map, path_pre){
    let str = fs.readFileSync(path_pre, {encoding: "utf-8"});
    if(!str.length){
        console.warn("没有内容?:",path_pre);
        return;
    }
    let data = JSON.parse(str);
    let count = 0;
    for(let i = 0 ; i < data.length ; i++){
        switch(data[i].__type__){
            case "cc.Sprite":
                if(data[i]._spriteFrame){
                    let uuid = data[i]._spriteFrame.__uuid__;
                    let ex = uuid_map.get(uuid);
                    if(ex){
                        count++;
                        data[i]._spriteFrame.__uuid__ = ex;
                    }
                }
                break;
            case "CCPropertyOverrideInfo":
                if(data[i].value && data[i].value.__expectedType__ == "cc.SpriteFrame"){
                    let uuid = data[i].value.__uuid__;
                    let ex = uuid_map.get(uuid);
                    if(ex){
                        count++;
                        data[i].value.__uuid__ = ex;
                    }
                }
                break;
            case "cc.Button":
                if(data[i]._normalSprite){
                    let uuid = data[i]._normalSprite.__uuid__;
                    let ex = uuid_map.get(uuid);
                    if(ex){
                        count++;
                        data[i]._normalSprite.__uuid__ = ex;
                    }
                }
                break;
        }
    }
    if(count > 0){
        writeFile(path_pre, JSON.stringify(data), true);
    }
    else{
        console.log("没有变化:",path_pre);
    }
}

var write_count = 0;
function writeFile(fileName, data, calc_count) {
    if(calc_count)write_count++;
	function complete(err) {
        if (!err) {
            console.log(fileName + " 生成成功");
		}
		else{
            console.log(fileName + " 生成失败");
		}
        if(calc_count){
            write_count--;
            if(write_count == 0){
                console.log("替换完毕☠")
            }
        }
	}
	fs.writeFile(fileName, data, 'utf-8', complete);
}

function exchange(){
    let data_plist = JSON.parse(fs.readFileSync(PlistPath, {encoding: "utf-8"}));
    // console.log(data_plist);
    getAllPath(TexturesPath, [".png.meta"], true);
    // console.log(need_path);
    let uuid_map = null;
    if(Cmd == 1){
        uuid_map = getMap(data_plist);
        // console.log(uuid_map);
        let str = "";
        uuid_map.forEach((uuid_new, uuid_old)=>{
            str += `${uuid_old} => ${uuid_new}\n`;
        })
        writeFile(CurMapPath, str, false);
    }
    else if(Cmd == 3){
        uuid_map = getMapByTxt();
    }

    need_path.length = 0;
    getAllPath(PrefabsPath, [".prefab", ".scene"], false);
    console.log(need_path)
    for(let i = 0 ; i < need_path.length ; i++){
        changePrefab(uuid_map, need_path[i]);
    }
    if(write_count == 0){
        console.log("没有需要替换的uuid");
    }
    else{
        writeFile(PlistPath, JSON.stringify(data_plist), false);
    }
}

function mergeMap(){
    getAllPath("./", [".txt"], false);
    let str = "";
    for(let i = 0 ; i < need_path.length ; i++){
        if(need_path[i].endsWith(TotalMapPath))continue;
        str += need_path[i];
        str += "\n";
        str += fs.readFileSync(need_path[i], {encoding: "utf-8"});
        str += "\n";
    }
    writeFile(TotalMapPath, str, false);
}

function run(){
    if(Cmd == 1 || Cmd == 3){
        exchange();
    }
    else if(Cmd == 2){
        mergeMap();
    }
}

run();