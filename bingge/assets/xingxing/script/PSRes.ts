import { _decorator, Component, Node, Prefab, SpriteAtlas } from 'cc';
import { ResPool } from '../../master/script/game/ResPool';
import { PSEle } from './PSEle';
const { ccclass, property } = _decorator;

@ccclass('PSRes')
export class PSRes extends Component {
    public static instance: PSRes = null;
    
    @property(SpriteAtlas)
    public atlas_ele: SpriteAtlas = null;
    @property(Prefab)
    private pre_ele: Prefab = null;

    public pool_ele: ResPool<PSEle> = null;

    protected onLoad(): void {
        if(PSRes.instance)console.warn("PSRes is have");
        PSRes.instance = this;
        this.pool_ele = new ResPool<PSEle>(PSEle, this.pre_ele, 200);
    }
    protected onDestroy(): void {
        this.pool_ele.clear();
        PSRes.instance = null;
    }
}


