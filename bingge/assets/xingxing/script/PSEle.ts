import { _decorator, Component, Node, Sprite, tween } from 'cc';
import { PSRes } from './PSRes';
const { ccclass, property } = _decorator;

@ccclass('PSEle')
export class PSEle extends Component {
     
    private sp: Sprite = null;

    public type = 0;
    protected onLoad(): void {
        this.sp = this.node.getComponent(Sprite);
    }

    public init(type:number){
        this.type = type;
        this.sp.spriteFrame = PSRes.instance.atlas_ele.getSpriteFrame("ele" + type);
    }
    public move(x:number, y:number){
        tween(this.node)
            .toPro(0.1, {x, y})
            .start();
    }
}


