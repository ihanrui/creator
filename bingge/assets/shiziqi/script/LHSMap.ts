import { _decorator, Component, Node, Vec3 } from 'cc';
import { LHSState, SZConst } from './SZConst';
import { ChessControl } from '../../master/script/com/ChessControl';
import { GridGraphics } from '../../master/script/com/GridGraphics';
import { SZMap } from './SZMap';
import { SZChess } from './SZChess';
import { SZRes } from './SZRes';
const { ccclass, property } = _decorator;

const CanTo = [[-1,1,3],[-1,0,2,4],[-1,1,5],[0,4],[3,5],[2,4]];
@ccclass('LHSMap')
export class LHSMap extends SZMap {

    private state = LHSState.Play;
    private big_x = 0;
    private big_y = 0;
    private count = 16;

    public init(control: ChessControl, gra: GridGraphics): void {
        super.init(control, gra);
        this.gra.init(5, 5, 150);
        this.gra.drawLhs();
        this.control.init(30, 5, 5, 150);
        this.control.initChessSize(30, this.onClick, this);
        this.gra.node.addY(-100);
        this.control.node.addY(-100);
        this.control.addHeight(180);
        this.chess = new Array(5);
        this.chess[0] = new Array(5);
        this.chess[4] = new Array(5);
        for(let i = 1; i < 4; i++) this.chess[i] = new Array(8);
        this.initChess();
    }
    private addOneChess(x:number, y:number){
        this.chess[x][y] = SZRes.instance.pool_chess.alloc();
        this.gra.addChess(this.chess[x][y], x, y);
        this.chess[x][y].init(1, 1);
    }
    public initChess(){
        this.cur_site = 0;
        let chess:SZChess = SZRes.instance.pool_chess.alloc();
        this.gra.addChess(chess, 2, 2);
        chess.init(0, 0);
        chess.setBig(1.5);
        this.chess[2][2] = chess;
        for(let i = 0; i < 5; i++){
            this.addOneChess(i, 0);
            this.addOneChess(i, 4);
        }
        this.addOneChess(0, 1);
        this.addOneChess(0, 2);
        this.addOneChess(0, 3);
        this.addOneChess(4, 1);
        this.addOneChess(4, 2);
        this.addOneChess(4, 3);
        this.big_x = this.big_y = 2;
        this.count = 16;
        this.onPlay();
        SZConst.setChongkai(false);
    }
    private onPlay(){
        this.state = LHSState.Play;
        if(this.cur_site == 0)SZConst.showTip("老和尚走");
        else SZConst.showTip("小和尚走");
    }

    private moveTo(x:number, y:number, x1:number, y1:number){
        let chess = this.chess[x][y];
        if(y1 < 5){
            this.gra.moveChess(chess, x1, y1);
        }
    }
    private checkEat(){

    }
    public onClick(x:number, y:number){
        if(x == -1){
            let arr = this.gra.checkExPosIndex(y as any);
            if(arr[0] == -1)return;
            x = arr[0], y = arr[1];
        }
        switch(this.state){
            case LHSState.Play:
                if(this.cur_site == 0){
                    if(this.chess[x][y])return;
                    this.state = LHSState.Move;
                    this.moveTo(this.big_x, this.big_y, x, y);
                    this.big_x = x;
                    this.big_y = y;
                    this.scheduleOnce(this.checkEat, 0.2);
                }
                break;
        }
    }


}