import { _decorator, Component, Node, Sprite } from 'cc';
import { SZChess } from './SZChess';
import { ChessControl } from '../../master/script/com/ChessControl';
import { GridGraphics } from '../../master/script/com/GridGraphics';
import { SZRes } from './SZRes';
const { ccclass, property } = _decorator;

export class SZMap extends Component {
    protected chess:SZChess[][] = null;
    protected control: ChessControl = null;
    protected gra: GridGraphics = null;

    public cur_site = 0;
    public type_site:Array<number> = null;
    public init(control:ChessControl, gra: GridGraphics){
        this.control = control;
        this.gra = gra;
    }
    public clear(){
        for(let i = 0; i < this.chess.length; i++){
            SZRes.instance.pool_chess.recyAll(this.chess[i]);
        }
    }
    public initChess(){}
}