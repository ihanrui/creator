import { _decorator, Component, Node, Prefab, SpriteAtlas } from 'cc';
import { ResPool } from '../../master/script/game/ResPool';
import { SZChess } from './SZChess';
const { ccclass, property } = _decorator;

@ccclass('SZRes')
export class SZRes extends Component {
    public static instance: SZRes = null;

    @property(SpriteAtlas)
    public atlas_chess: SpriteAtlas = null;
    @property(Prefab)
    public pre_chess: Prefab = null;

    public pool_chess: ResPool<SZChess> = null;
    onLoad() {
        if(SZRes.instance)console.warn("SZRes is have");
        SZRes.instance = this;
        this.pool_chess = new ResPool<SZChess>(SZChess, this.pre_chess, 20);
    }
    protected onDestroy(): void {
        SZRes.instance = null;
    }

    //update(dt: number) {
    //}
}