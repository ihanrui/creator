import { _decorator, Component, Node, Sprite, tween } from 'cc';
import { SZRes } from './SZRes';
const { ccclass, property } = _decorator;

@ccclass('SZChess')
export class SZChess extends Component {
    private sp: Sprite = null;

    public type = 0;
    public site = 0;
    protected onLoad(): void {
        this.sp = this.node.getComponent(Sprite);
    }

    public init(site:number, type:number){
        this.type = type;
        this.site = site;
        this.sp.spriteFrame = SZRes.instance.atlas_chess.getSpriteFrame("chess" + type);
    }

    public setBig(scale:number){
        this.node.toScaleXY(scale);
    }
    public moveTo(x:number, y:number){
        tween(this.node)
            .toPro(0.2, {x, y})
            .start();
    }
}