import { _decorator, Component, Label, Node } from 'cc';
import { SZMap } from './SZMap';
import { ChessControl } from '../../master/script/com/ChessControl';
import { GridGraphics } from '../../master/script/com/GridGraphics';
import { SZRes } from './SZRes';
import { SZChess } from './SZChess';
import { RSYState, SZConst } from './SZConst';
const { ccclass, property } = _decorator;

@ccclass('RSYMap')
export class RSYMap extends SZMap {

    private state = RSYState.Play;
    private select_x = 0;
    private select_y = 0;
    private counts = [4, 4];
    public init(control: ChessControl, gra: GridGraphics): void {
        super.init(control, gra);
        this.gra.init(4, 4, 150);
        this.control.init(30, 4, 4, 150);
        this.control.initChessSize(30, this.onClick, this);
        this.chess = new Array(4);
        for(let i = 0; i < 4; i++) this.chess[i] = new Array(4);
        this.initChess();
    }
    public onPlay(){
        this.state = RSYState.Play;
        SZConst.showTip(SZConst.colors[this.type_site[this.cur_site]] + "走");
    }
    public initChess(){
        this.cur_site = 0;
        this.type_site = [0, 1];
        let chess:SZChess = null;
        for(let i = 0; i < 4; i++){
            chess = this.chess[i][0] = SZRes.instance.pool_chess.alloc();
            this.gra.addChess(chess, i, 0);
            this.chess[i][0].init(0, this.type_site[0]);

            chess = this.chess[i][3] = SZRes.instance.pool_chess.alloc();
            this.gra.addChess(chess, i, 3);
            this.chess[i][3].init(1, this.type_site[1]);
        }
        this.counts[0] = this.counts[1] = 4;
        this.onPlay();
        SZConst.setChongkai(false);
    }
    public clear(){
        for(let i = 0; i < 4; i++){
            for(let j = 0; j < 4; j++){
                if(this.chess[i][j])SZRes.instance.pool_chess.recycle(this.chess[i][j]);
                this.chess[i][j] = null;
            }
        }
    }
    private checkOneEat(x:number, y:number, check_x:boolean){
        let total = 0;
        let other_index = -1;
        if(check_x){
            for(let i = 0; i < 4; i++){
                if(!this.chess[i][y])total += 0;
                else if(this.chess[i][y].site == this.cur_site)total += 1;
                else{
                    other_index = i;
                    total -= 1;
                }
            }
            if(total == 1 && other_index > -1){
                if(!this.chess[1][y] || !this.chess[2][y])return -1;
                if(x > 0 && this.chess[x - 1][y]?.site == this.cur_site)return other_index;
                if(x < 3 && this.chess[x + 1][y]?.site == this.cur_site)return other_index;
            }
        }
        else{
            for(let i = 0; i < 4; i++){
                if(!this.chess[x][i])total += 0;
                else if(this.chess[x][i].site == this.cur_site)total += 1;
                else{
                    other_index = i;
                    total -= 1;
                }
            }
            if(total == 1 && other_index > -1){
                if(!this.chess[x][1] || !this.chess[x][2])return -1;
                if(y > 0 && this.chess[x][y - 1]?.site == this.cur_site)return other_index;
                if(y < 3 && this.chess[x][y + 1]?.site == this.cur_site)return other_index;
            }
        }
        return -1;
    }
    private moveAndCheck(chess:SZChess, x:number, y:number){
        this.chess[x][y] = chess;
        let x_index = this.checkOneEat(x, y, true);
        let y_index = this.checkOneEat(x, y, false);
        if(x_index > -1 && y_index > -1)return;
        let other_site = this.cur_site == 0 ? 1 : 0;
        if(x_index > -1){
            SZRes.instance.pool_chess.recycle(this.chess[x_index][y]);
            this.chess[x_index][y] = null;
            this.counts[other_site]--;
        }
        else if(y_index > -1){
            SZRes.instance.pool_chess.recycle(this.chess[x][y_index]);
            this.chess[x][y_index] = null;
            this.counts[other_site]--;
        }
    }
    public onClick(x:number, y:number){
        if(this.state == RSYState.Play){
            let chess = this.chess[x][y];
            if(!chess || chess.site != this.cur_site)return;
            this.select_x = x;
            this.select_y = y;
            chess.setBig(1.2);
            this.state = RSYState.Select;
        }
        else if(this.state == RSYState.Select){
            if(x == this.select_x && y == this.select_y){
                this.chess[x][y].setBig(1);
                this.state = RSYState.Play;
            }
            if(Math.abs(x - this.select_x) + Math.abs(y - this.select_y) != 1)return;
            if(this.chess[x][y])return;
            let chess = this.chess[this.select_x][this.select_y];
            chess.setBig(1);
            this.chess[this.select_x][this.select_y] = null;
            this.gra.moveChess(chess, x, y);
            this.state = RSYState.Move;
            this.scheduleOnce(function(){
                this.moveAndCheck(chess, x, y)
                if(this.cur_site == 0)this.cur_site = 1;
                else this.cur_site = 0;
                console.log("改变出手")
                if(this.counts[this.cur_site] == 1){
                    SZConst.showTip(SZConst.colors[this.type_site[this.cur_site]] + "输");
                    SZConst.setChongkai(true);
                }
                else this.onPlay();
            }, 0.2)
        }

    }
}