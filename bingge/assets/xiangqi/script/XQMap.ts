import { _decorator, Color, Component, EventTouch, Node, UITransform, Vec3 } from 'cc';
import { XQSite, XQState, XQType } from './XQConst';
import { GameConst } from '../../master/script/game/GameConst';
import { XQEle } from './XQEle';
import { XQRes } from './XQRes';
import { UtilsPanel } from '../../master/script/util/UtilsPanel';
import { XQUI } from './XQUI';
const { ccclass, property } = _decorator;

const OneHeight = 68;
const Tmp_V3 = new Vec3();
const Offset_Begin = 40;
const Half_Chess = 25;

const Chess_Pos = [[0,0,0],[1,0,1],[2,0,2],[3,0,3],[4,0,4],[5,0,3],[6,0,2],[7,0,1],[8,0,0],
                    [0,3,6],[2,3,6],[4,3,6],[6,3,6],[8,3,6],[1,2,5],[7,2,5]];
@ccclass('XQMap')
export class XQMap extends Component {
    private uitrans: UITransform = null;
	private node_chess: Node = null;
	private node_tip: Node = null;
	private node_select: Node = null;
	private node_last: Node = null;
    private ui: XQUI = null;

    private offset_x: Array<number> = null;
    private offset_y: Array<number> = null;
    private c_site = 0;
    private can_path: Array<Array<number>> = [];
    private s_row = 0;
    private s_col = 0;
    private t_row = 0;
    private t_col = 0;
    private state = XQState.Play;
    private red_is_bottom = true;
    private eles: Array<Array<XQEle>> = null;
    private tips: Array<Node> = [];
    onLoad() {
        this.uitrans = this.node.getComponent(UITransform);
        UtilsPanel.getAllNeedCom(this, this.node);
        this.offset_x = GameConst.getOffsetArr2(9, Offset_Begin, OneHeight);
        this.offset_y = GameConst.getOffsetArr2(10, Offset_Begin, OneHeight);
        this.eles = new Array(9);
        for(let i = 0 ; i < 9; i++)this.eles[i] = new Array(9)
        UtilsPanel.addBtnEvent2(this.node, "onClick", this, "").transition = 0;
    }

    public init(ui:XQUI, red_is_bottom:boolean){
        this.ui = ui;
        let site0 = 0, site1 = 1;
        if(!red_is_bottom){
            site0 = 1;
            site1 = 0;
        }
        let chess:XQEle = null;
        let pool = XQRes.instance.pool_ele;
        let info = null;
        for(let i = 0; i < Chess_Pos.length; i++){
            chess = pool.alloc();
            chess.node.setParent(this.node_chess);
            info = Chess_Pos[i];
            chess.node.toXY(this.offset_x[info[0]], this.offset_y[info[1]]);
            chess.init(info[2], site0);
            this.eles[info[0]][info[1]] = chess;

            chess = pool.alloc();
            chess.node.setParent(this.node_chess);
            chess.node.toXY(this.offset_x[info[0]], this.offset_y[9 - info[1]]);
            chess.init(info[2], site1);
            this.eles[info[0]][9 - info[1]] = chess;
        }
    }

    public clear(){
        for(let i = 0; i < 9; i++){
            XQRes.instance.pool_ele.recyAll2(this.eles[i]);
        }
    }
    private curIsBottom(){
        return this.red_is_bottom && this.c_site == XQSite.Red;
    }
    //无需过滤
    private getChePath(x:number, y:number){
        let site = this.c_site;
        let arr = []
        for(let i = x - 1; i >= 0; i--){
            if(!this.eles[i][y])arr.push([i, y]);
            else{
                if(this.eles[i][y].site != site)arr.push([i,y]);
                break;
            }
        }
        for(let i = x + 1; i < 9; i++){
            if(!this.eles[i][y])arr.push([i, y]);
            else{
                if(this.eles[i][y].site != site)arr.push([i,y]);
                break;
            }
        }
        for(let i = y-1; i >= 0; i--){
            if(!this.eles[x][i])arr.push([x, i]);
            else{
                if(this.eles[x][i].site != site)arr.push([x, i]);
                break;
            }
        }
        for(let i = y+1; i < 10; i++){
            if(!this.eles[x][i])arr.push([x, i]);
            else{
                if(this.eles[x][i].site != site)arr.push([x, i]);
                break;
            }
        }
        return arr;
    }
    private getMaPath(x:number, y:number){
        let arr = [];
        if(x + 2 < 9 && !this.eles[x + 1][y]){
            if(y + 1 < 10)arr.push([x+2, y+1]);
            if(y - 1 >= 0)arr.push([x+2, y-1]);
        }
        if(x - 2 >= 0 && !this.eles[x - 1][y]){
            if(y + 1 < 10)arr.push([x-2, y+1]);
            if(y - 1 >= 0)arr.push([x-2, y-1]);
        }
        if(y + 2 < 10 && !this.eles[x][y+1]){
            if(x + 1 < 9)arr.push([x+1, y+2])
            if(x - 1 >= 0)arr.push([x-1, y+2]);
        }
        if(y - 2 >= 0 && !this.eles[x][y-1]){
            if(x + 1 < 9)arr.push([x+1, y-2])
            if(x - 1 >= 0)arr.push([x-1, y-2]);
        }
        return arr;
    }
    private getXiangPath(x:number, y:number){
        let arr = [];
        let temp = [[x+2, y+2], [x-2,y-2], [x+2, y-2], [x-2, y+2]];
        for(let i = 0; i < 4; i++){
            if(temp[i][0] < 0 || temp[i][0] >= 9 || temp[i][1] < 0 || temp[i][1] >= 10)continue;
            if(y == 3 || y == 6)continue;
            if(this.eles[(x + temp[i][0]) / 2][(y + temp[i][1]) / 2])continue;
            arr.push(temp[i]);
        }
        return arr;
    }
    private getShiPath(x:number, y: number){
        let arr = null;
        if(x == 4){
            arr = [[x+1, y+1],[x-1, y-1],[x+1, y-1],[x-1, y+1]]
        }
        else{
            if(y < 5)arr = [[4, 1]];
            else arr = [[4, 8]];
        }
        return arr;
    }
    private getShuaiPath(x:number, y:number){
        let arr = [];
        let temp = [[x+1, y], [x-1,y], [x, y-1], [x, y+1]];
        let x_border = [2, 6];
        let y_border = [-1, 10, 3, 6];
        for(let i = 0 ; i < temp.length; i++){
            if(x_border.includes(temp[i][0]) || y_border.includes(temp[i][1]))continue;
            arr.push(temp[i]);
        }
        return arr;
    }
    //无需过滤
    private getPaoPath(x:number, y:number){
        let site = this.c_site;
        let arr = [];
        let find = 0;
        let i = 0;
        for(i = x-1; i >= 0; i--){
            if(find == 0){
                if(!this.eles[i][y])arr.push([i, y]);
                else find = 1;
            }
            else if(this.eles[i][y]){
                if(this.eles[i][y].site != site)arr.push([i,y]);
                break;
            }
        }
        find = 0;
        for(i = x+1; i < 9; i++){
            if(find == 0){
                if(!this.eles[i][y])arr.push([i, y]);
                else find = 1;
            }
            else if(this.eles[i][y]){
                if(this.eles[i][y].site != site)arr.push([i,y]);
                break;
            }
        }
        find = 0;
        for(i = y-1; i >= 0; i--){
            if(find == 0){
                if(!this.eles[x][i])arr.push([x, i]);
                else find = 1
            }
            else if(this.eles[x][i]){
                if(this.eles[x][i].site != site)arr.push([x, i]);
                break;
            }
        }
        find = 0;
        for(i = y+1; i < 10; i++){
            if(find == 0){
                if(!this.eles[x][i])arr.push([x, i]);
                else find = 1
            }
            else if(this.eles[x][i]){
                if(this.eles[x][i].site != site)arr.push([x, i]);
                break;
            }
        }
        return arr;
    }
    private getBingPath(x:number, y:number){
        let to_up = this.red_is_bottom && this.c_site == XQSite.Red;
        let arr = [];
        if(to_up){
            if(y < 9)arr.push([x, y+1]);
            if(y > 4){
                if(x > 0)arr.push([x-1, y]);
                if(x < 8)arr.push([x+1, y]);
            }
        }
        else{
            if(y > 0)arr.push([x, y-1]);
            if(y < 5){
                if(x > 0)arr.push([x-1, y]);
                if(x < 8)arr.push([x+1, y]);
            }
        }
        return arr;
    }
    private filterPath(arr:Array<Array<number>>){
        let res = [];
        let ele = null;
        for(let i = 0; i < arr.length; i++){
            ele = this.eles[arr[i][0]][arr[i][1]]
            if(!ele || ele.site != this.c_site)res.push(arr[i]);
        }
        return res;
    }
    private getCanMove(x:number, y:number){
        let type = this.eles[x][y].type;
        switch(type){
            case XQType.Che:
                return this.getChePath(x, y);
            case XQType.Ma:
                return this.filterPath(this.getMaPath(x, y));
            case XQType.Xiang:
                return this.filterPath(this.getXiangPath(x, y));
            case XQType.Shi:
                return this.filterPath(this.getShiPath(x, y));
            case XQType.Shuai:
                return this.filterPath(this.getShuaiPath(x, y));
            case XQType.Pao:
                return this.getPaoPath(x, y);
            case XQType.Bing:
                return this.filterPath(this.getBingPath(x, y));

        }
    }

    public showTips(){
        let node = null;
        let pos = null;
        for(let i = 0; i < this.can_path.length; i++){
            node = XQRes.instance.pool_tip.alloc();
            node.setParent(this.node_tip);
            pos = this.can_path[i];
            node.toXY(this.offset_x[pos[0]], this.offset_y[pos[1]]);
            this.tips.push(node);
        }
    }
    public clearTips(){
        this.node_tip.removeAllChildren();
        XQRes.instance.pool_tip.freeArray(this.tips);
        this.tips.length = 0;
    }
    private onPlay(){
        this.state = XQState.Play;
        this.node_select.active = false;
        this.clearTips();
        if(this.c_site == 0){
            this.ui.setTipColor(Color.RED);
            this.ui.showTip("红棋走");
        }
        else{
            this.ui.setTipColor(Color.BLACK);
            this.ui.showTip("黑棋走");
        }
    }
    public onClick(e:EventTouch){
        if(this.state != XQState.Play && this.state != XQState.Select)return;
        e.getUILocation(Tmp_V3 as any);
        this.uitrans.convertToNodeSpaceAR(Tmp_V3, Tmp_V3);
        let x = -1, y = -1;
        for(let i = 0; i < this.offset_x.length; i++){
            if(Tmp_V3.x > this.offset_x[i] - Half_Chess && Tmp_V3.x < this.offset_x[i] + Half_Chess){
                x = i;
                break;
            }
        }
        for(let i = 0; i < this.offset_y.length; i++){
            if(Tmp_V3.y > this.offset_y[i] - Half_Chess && Tmp_V3.y < this.offset_y[i] + Half_Chess){
                y = i;
                break;
            }
        }
        if(x == -1 || y == -1)return;
        if(this.state == XQState.Play){
            if(!this.eles[x][y] || this.eles[x][y].site != this.c_site)return;
            this.can_path = this.getCanMove(x, y);
            if(this.can_path.length > 0){
                this.state = XQState.Select;
                this.s_row = x;
                this.s_col = y;
                this.node_select.active = true;
                this.node_select.toXY(this.offset_x[x], this.offset_y[y]);
                this.showTips();
            }

        }
        else{
            if(this.s_row == x && this.s_col == y){
                this.onPlay();
                return;
            }
            if(this.can_path.find(item=>item[0]==x && item[1] == y)){
                this.state = XQState.Move;
                this.eles[this.s_row][this.s_col].move(this.offset_x[x], this.offset_y[y]);
                this.t_row = x;
                this.t_col = y;
                this.node_last.active = true;
                this.node_last.toXY(this.offset_x[this.s_row], this.offset_y[this.s_col]);
                this.scheduleOnce(this.moveEnd, 0.2);
            }
        }
    }
    private moveEnd(){
        let ele = this.eles[this.t_row][this.t_col];
        let is_over = false;
        if(ele){
            if(ele.type == XQType.Shuai)is_over = true;
            XQRes.instance.pool_ele.recycle(this.eles[this.t_row][this.t_col]);
        }
        this.eles[this.t_row][this.t_col] = this.eles[this.s_row][this.s_col];
        this.eles[this.s_row][this.s_col] = null;
        if(is_over){
            if(this.c_site == 0)this.ui.showTip("红方胜");
            else this.ui.showTip("黑方胜");
            this.clearTips();
            this.node_select.active = false;
        }
        else{
            if(this.c_site == 0)this.c_site = 1;
            else this.c_site = 0;
            this.onPlay();
        }
    }

}