import { _decorator, Component, instantiate, Node, Pool, Prefab, SpriteAtlas } from 'cc';
import { ResPool } from '../../master/script/game/ResPool';
import { XQEle } from './XQEle';
const { ccclass, property } = _decorator;

@ccclass('XQRes')
export class XQRes extends Component {
    public static instance: XQRes = null;
    
    @property(SpriteAtlas)
    public atlas_ele: SpriteAtlas = null;
    @property(Prefab)
    private pre_ele: Prefab = null;
    @property(Prefab)
    private pre_tip: Prefab = null;
    

    public pool_ele: ResPool<XQEle> = null;
    public pool_tip: Pool<Node> = null;

    protected onLoad(): void {
        if(XQRes.instance)console.warn("XQRes is have");
        XQRes.instance = this;
        this.pool_ele = new ResPool<XQEle>(XQEle, this.pre_ele, 32);
        this.pool_tip = new Pool(()=>{return instantiate(this.pre_tip)}, 15);
    }
    protected onDestroy(): void {
        this.pool_ele.clear();
        this.pool_tip.destroy();
        XQRes.instance = null;
    }

}