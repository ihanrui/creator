
export enum XQState{
    Play,
    Select,
    Move,
    Over,
}

export enum XQType{
    Che,
    Ma,
    Xiang,
    Shi,
    Shuai,
    Pao,
    Bing,
}
export enum XQSite{
    Red,
    Black,
}
export class XQConst {
}