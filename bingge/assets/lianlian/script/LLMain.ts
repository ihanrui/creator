
import { _decorator, Component, director, find, Node } from 'cc';
import { LLMap } from './LLMap';
import { UtilsPanel } from '../../master/script/util/UtilsPanel';
const { ccclass } = _decorator;

@ccclass('LLMain')
export class LLMain extends Component {
    private map: LLMap = null;

    protected onLoad(): void {
        UtilsPanel.getAllNeedCom(this, find("Canvas"));
    }
    protected start(): void {
        this.map.init(8, 8, 10, [1]);
    }
	private onBackClick(){
		director.loadScene("lobby");
	}

}
