
import { _decorator, Component, EventTouch, Graphics, Node, UITransform, Vec3 } from 'cc';
import { LLEle } from './LLEle';
import { LLState } from './LLConst';
import { LLRes } from './LLRes';
import { GameConst } from '../../master/script/game/GameConst';
import { UtilsPanel } from '../../master/script/util/UtilsPanel';
import { LLAStar } from './LLAStar';
const { ccclass } = _decorator;

const MaxCount = 10;
const OneHeight = 64;
const HalfHeight = 32;
const Tmp_V3 = new Vec3();

@ccclass('LLMap')
export class LLMap extends Component {
	private uitrans_ele: UITransform = null;
	private graphics: Graphics = null;
    private node_light: Node = null;

    private eles: LLEle[][] = null;
    private map: number[][] = null;
    private offset_x: Array<number> = null;
    private offset_y: Array<number> = null;
    private cur_row = 0;
    private cur_col = 0;
    private state = LLState.NoStart;
    //select_info
    // private s_i = [0,0,0,0];
    private s0 = 0;
    private s1 = 0;

    protected onLoad(): void {
        UtilsPanel.getAllNeedCom(this, this.node);
        this.eles = new Array(MaxCount);
        this.map = new Array(MaxCount + 2);
        this.offset_x = new Array(MaxCount);
        this.offset_y = new Array(MaxCount);
        for(let i = 0; i < MaxCount; i++){
            this.eles[i] = new Array(MaxCount);
            this.map[i + 1] = new Array(MaxCount + 2);
        }
        this.map[0] = new Array(MaxCount + 2).fill(-1);
        this.map[MaxCount + 1] = new Array(MaxCount + 2);
        LLAStar.setMap(this.map);
        UtilsPanel.addBtnEvent2(this.uitrans_ele.node, "onClick", this, "").transition = 0;
    }
    public init(row:number, col:number, type_count:number, indexs:Array<number>){
        this.clear();
        this.cur_row = row;
        this.cur_col = col;
        LLAStar.setSize(row + 2, col + 2);
        let type = 0;
        let index_count = indexs.length;
        let ele:LLEle = null;
        GameConst.getOffsetArr2(row, OneHeight / 2, OneHeight, this.offset_x);
        GameConst.getOffsetArr2(col, OneHeight / 2, OneHeight, this.offset_y);
        this.uitrans_ele.setContentSize(row * OneHeight, col * OneHeight);
        // this.graphics.node._uiProps.uiTransformComp.setContentSize((row  + 2)* OneHeight, (col + 2) * OneHeight);
        this.node.toXY(-row * OneHeight / 2, -col * OneHeight / 2);
        this.graphics.node.toXY(-OneHeight, -OneHeight);
        for(let i = 0; i < row; i++){
            for(let j = 0; j < col; j++){
                ele = LLRes.instance.pool_ele.alloc();
                ele.node.setParent(this.uitrans_ele.node);
                ele.node.toXY(this.offset_x[i], this.offset_y[j]);
                type = Math.floor(Math.random() * type_count);
                this.eles[i][j] = ele;
                this.map[i+1][j+1] = ele.init(type, indexs[Math.floor(Math.random() * index_count)]);
            }
        }
        // LLAStar.logMap();
        this.state = LLState.Play;
    }
    public clear(){
        for(let i = 0; i < this.cur_row; i++){
            for(let j = 0; j < this.cur_col; j++){
                LLRes.instance.pool_ele.recycle(this.eles[i][j]);
                this.eles[i][j] = null;
            }
        }
    }
    public onClick(e:EventTouch){
        e.getUILocation(Tmp_V3 as any);
        this.uitrans_ele.convertToNodeSpaceAR(Tmp_V3, Tmp_V3);
        if(this.state == LLState.Select){
            let s2 = Math.floor(Tmp_V3.x / OneHeight);
            let s3 = Math.floor(Tmp_V3.y / OneHeight);
            if(this.s0 == s2 && this.s1 == s3){
                this.node_light.active = false;
                this.state = LLState.Play;
            }
            else if(this.map[this.s0 + 1][this.s1 + 1]!= this.map[s2+1][s3+1]){
                return;
            }
            else{
                let res = LLAStar.canJoin([this.s0 + 1, this.s1 + 1], [s2 + 1, s3 + 1]);
                console.log(res);
                if(res){
                    this.showLine(res);
                    LLRes.instance.pool_ele.recycle(this.eles[this.s0][this.s1]);
                    LLRes.instance.pool_ele.recycle(this.eles[s2][s3]);
                    this.eles[this.s0][this.s1] = this.eles[s2][s3] = null;
                    this.map[this.s0 + 1][this.s1 + 1] = this.map[s2 + 1][s3 + 1] = -1
                }
                this.node_light.active = false;
                this.state = LLState.Play;
            }
        }
        else if(this.state == LLState.Play){
            this.s0 = Math.floor(Tmp_V3.x / OneHeight);
            this.s1 = Math.floor(Tmp_V3.y / OneHeight);
            if(this.map[this.s0 + 1][this.s1 + 1] == -1)return;
            this.state = LLState.Select;
            this.node_light.active = true;
            this.node_light.toXY(this.offset_x[this.s0], this.offset_y[this.s1]);
        }
    }

    private showLine(res: Array<Array<number>>){
        this.graphics.moveTo(res[0][0] * OneHeight + HalfHeight, res[0][1] * OneHeight + HalfHeight);
        for(let i = 1 ; i < res.length; i++){
            this.graphics.lineTo(res[i][0] * OneHeight + HalfHeight, res[i][1] * OneHeight + HalfHeight);
        }
        this.graphics.stroke();
        this.scheduleOnce(this.clearGraphics, 0.3);
    }
    private clearGraphics(){
        this.graphics.clear();
    }

}
