
import { _decorator, Component, Node, Sprite } from 'cc';
import { UtilsPanel } from '../../master/script/util/UtilsPanel';
import { MTRes } from './MTRes';
const { ccclass } = _decorator;

@ccclass('MTEff')
export class MTEff extends Component {
    private sp: Sprite = null;
    
    protected onLoad(): void {
        this.sp = this.node.getComponent(Sprite);
    }
    public showBoom(){
        UtilsPanel.playSeqFrames(0.05, this.sp, MTRes.instance.frames_boom, ()=>{
            if(!this.isValid)return;
            MTRes.instance.pool_eff.recycle(this);
        });
    }
}
