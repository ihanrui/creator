
import { _decorator, Component, Label, Node, Sprite, Tween, tween } from 'cc';
import { Color_Ele, Type_Ele } from './MTConst';
import { MTRes } from './MTRes';
import { UtilsPanel } from '../../master/script/util/UtilsPanel';
const { ccclass } = _decorator;

@ccclass('MTEle')
export class MTEle extends Component {
    private sp: Sprite = null;
    
    public type = 0;
    public remove_flag = false;
    private tween_ele: Tween<any> = null;
    protected onLoad(): void {
        this.sp = this.node.getComponent(Sprite);
    }
    public init(type:number){
        this.type = type;
        this.sp.spriteFrame = MTRes.instance.frames_ele[type][0];
    }
    public move(x:number, y:number){
        tween(this.node)
            .toPro(0.1, {x, y})
            .start();
        }
    public reset(){
        this.remove_flag = false;
        if(this.tween_ele){
            this.tween_ele.stop();
            this.tween_ele = null;
        }
    }
    public setSelect(val:boolean){
        if(this.tween_ele){
            this.tween_ele.stop();
            this.tween_ele = null;
        }
        if(val){
           this.tween_ele = UtilsPanel.playLoopFrames(0.1, this.sp, MTRes.instance.frames_ele[this.type]);
        }
        else{
            this.sp.spriteFrame = MTRes.instance.frames_ele[this.type][0];
        }
    }
}
