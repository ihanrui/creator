
import { _decorator, Component, director, find, Node } from 'cc';
import { MTMap } from './MTMap';
import { UtilsPanel } from '../../master/script/util/UtilsPanel';
const { ccclass } = _decorator;

@ccclass('MTMain')
export class MTMain extends Component {
    private map: MTMap = null;

    protected onLoad(): void {
        UtilsPanel.getAllNeedCom(this, find("Canvas"));
    }
    protected start(): void {
        this.map.init(8, 8);
    }
    private onBackClick(){
		director.loadScene("lobby");
	}
}
