
import { _decorator, Component, Node, Sprite } from 'cc';
import { MTRes } from './MTRes';
const { ccclass } = _decorator;

@ccclass('MTFloor')
export class MTFloor extends Component {
    private sp: Sprite = null;
    
    public type = 0;
    protected onLoad(): void {
        this.sp = this.node.getComponent(Sprite);
    }

    public init(type:number){
        this.type = type;
        this.sp.spriteFrame = MTRes.instance.getEle("floor_" + type);
    }
}
