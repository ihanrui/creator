import { _decorator, Node, Button, Component, EventHandler, js, sys, native } from 'cc';

export class UtilsCommon {

    public static createEventHandler(node: Node, com: any, func_name: string, data = "") {
        let handler = new Component.EventHandler;
        handler.target = node;
        handler.component = js.getClassName(com);
        handler.handler = func_name;
        handler.customEventData = data;
        return handler;
    }
    
    public static removeArrAt(arr:Array<any>, i:number){
        for(let l = arr.length; i < l; i++)arr[i] = arr[i + 1];
    }
    public static saveTxt(name:string, text:string){
        if(sys.isBrowser){
            var blob = new Blob([text], { type: "text/plain"}); 
            var anchor = document.createElement("a"); 
            anchor.download = name; 
            anchor.href = window.URL.createObjectURL(blob); 
            anchor.target ="_blank"; 
            anchor.style.display = "none"; // just to be safe! 
            document.body.appendChild(anchor); 
            anchor.click(); 
            document.body.removeChild(anchor); 
        }
        else{
            let path = native.fileUtils.getWritablePath();
            path += name;
            console.log(path);
            native.fileUtils.writeStringToFile(text, path);
        }
    }
}


