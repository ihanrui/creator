import { AssetManager, SceneAsset, assetManager, director } from "cc";


class Hall {
    public enterGame(name:string){
        assetManager.loadBundle(name, function(err, bundle:AssetManager.Bundle){
            console.time(name);
            bundle.loadScene(name, function(err,scene:SceneAsset){
                console.timeEnd(name);
                if(err)return;
                director.runScene(scene);
            })
        })
    }
}

export const hall = new Hall();