import { _decorator, Color, Component, Graphics, Node, Vec3 } from 'cc';
import { GameConst } from '../game/GameConst';
import { SZChess } from '../../../shiziqi/script/SZChess';
const { ccclass, property } = _decorator;

@ccclass('GridGraphics')
export class GridGraphics extends Component {
    private gra: Graphics = null;

    private arr_x: Array<number> = null;
    private arr_y: Array<number> = null;
    private color_x: Color = null;
    private color_y: Color = null;
    private ex_pos: Array<Array<number>> = null;
    onLoad() {
        this.gra = this.node.getComponent(Graphics);
    }
    public initColor(color_x:Color, color_y:Color){
        this.color_x = color_x;
        this.color_y = color_y;
    }

    public init(row:number, col:number, offset:number){
        this.arr_x = GameConst.getOffsetArr2(row, 0, offset);
        this.arr_y = GameConst.getOffsetArr2(col, 0, offset);
        this.node.toXY(-this.arr_x[row - 1] / 2, -this.arr_y[col - 1] / 2);
        if(!this.color_x)return;
        this.gra.clear();
        this.gra.strokeColor = this.color_x;
        for(let i = 0; i < row; i++){
            this.gra.moveTo(this.arr_x[i], this.arr_y[0]);
            this.gra.lineTo(this.arr_x[i], this.arr_y[col - 1]);
        }
        this.gra.stroke();
        this.gra.strokeColor = this.color_y;
        for(let i = 0; i < col; i++){
            this.gra.moveTo(this.arr_x[0], this.arr_y[i]);
            this.gra.lineTo(this.arr_x[row - 1], this.arr_y[i]);
        }
        this.gra.stroke();
    }
    public drawLhs(){
        let row = this.arr_x.length;
        let col = this.arr_y.length;
        this.gra.moveTo(this.arr_x[0], this.arr_y[0]);
        this.gra.lineTo(this.arr_x[row - 1], this.arr_y[col - 1]);
        this.gra.moveTo(this.arr_x[0], this.arr_y[col - 1]);
        this.gra.lineTo(this.arr_x[row - 1], this.arr_y[0]);
        
        this.gra.moveTo(this.arr_x[0], this.arr_y[2]);
        this.gra.lineTo(this.arr_x[2], this.arr_y[0]);
        this.gra.lineTo(this.arr_x[row - 1], this.arr_y[2]);
        this.gra.lineTo(this.arr_x[2], this.arr_y[col - 1]);
        this.gra.lineTo(this.arr_x[0], this.arr_y[2]);
        this.gra.stroke();
        this.ex_pos = [];
        let height = 100;
        let x = this.arr_x[2], y = this.arr_y[col - 1];
        this.ex_pos.push([x-height, y+height]);
        this.ex_pos.push([x, y+height]);
        this.ex_pos.push([x+height, y+height]);
        height *= 1.8;
        this.ex_pos.push([x-height, y+height]);
        this.ex_pos.push([x, y+height]);
        this.ex_pos.push([x+height, y+height]);
        this.gra.moveTo(x, y);
        this.gra.lineTo(this.ex_pos[3][0], this.ex_pos[3][1]);
        this.gra.lineTo(this.ex_pos[5][0], this.ex_pos[5][1]);
        this.gra.lineTo(x, y);
        this.gra.moveTo(this.ex_pos[0][0], this.ex_pos[1][1]);
        this.gra.lineTo(this.ex_pos[2][0], this.ex_pos[2][1]);
        this.gra.moveTo(x, y);
        this.gra.lineTo(this.ex_pos[4][0], this.ex_pos[4][1]);
        this.gra.stroke();
    }

    public addChess(chess:SZChess, x:number, y:number){
        chess.node.setParent(this.node);
        chess.node.toXY(this.arr_x[x], this.arr_y[y]);
    }
    public moveChess(chess:SZChess, x:number, y:number){
        chess.moveTo(this.arr_x[x], this.arr_y[y]);
    }
    public moveChessEx(chess:SZChess, index:number){
        let pos = this.ex_pos[index];
        chess.moveTo(pos[0], pos[1]);
    }
    public checkExPosIndex(pos:Vec3){
        let half = 30;
        if(pos.y > this.ex_pos[0][1] - half && pos.y < this.ex_pos[0][1] + half){
            for(let i = 0; i < 3; i++){
                if(pos.x > this.ex_pos[i][0] - half && pos.x < this.ex_pos[i][0] + half)
                return [i + 1, 5];
            }
        }
        else if(pos.y > this.ex_pos[3][1] - half && pos.y < this.ex_pos[3][1] + half){
            for(let i = 3; i < 6; i++){
                if(pos.x > this.ex_pos[i][0] - half && pos.x < this.ex_pos[i][0] + half)
                return [i - 2, 6];
            }
        }
        return [-1];
    }
}