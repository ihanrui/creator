import { _decorator, Component, find, Node } from 'cc';
import { UtilsPanel } from '../util/UtilsPanel';
import { hall } from '../Hall';
import { UtilsCommon } from '../util/UtilsCommon';
import { NativeHelper } from '../util/NativeHelper';
const { ccclass, property } = _decorator;

@ccclass('Lobby')
export class Lobby extends Component {
    onLoad() {
        UtilsPanel.getAllNeedCom(this, find("Canvas"));
        NativeHelper.init();
    }
    protected start(): void {
        NativeHelper.hideSplash();
    }
    private onXiaoxiaoClick(){
		hall.enterGame("xiaoxiao");
	}
	private onLianlianClick(){
		hall.enterGame("lianlian");
	}
	private onXiaomieClick(){
        hall.enterGame("xingxing");
	}
    private onXiangqiClick(){
        hall.enterGame("xiangqi");
	}
    private onShiziqiClick(){
        hall.enterGame("shiziqi");
	}


    //update(dt: number) {
    //}
}