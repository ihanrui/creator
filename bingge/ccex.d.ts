declare module "cc"{
    interface Node{
         //添加子节点时自动排序
         autoSort():void;
         //按position.z为自己点排序
         sortByPosZ():void;
         toX(x:number):void;
         addX(add_x: number):void;
         toY(y:number):void;
         addY(add_y: number):void;
         toZ(z: number, need_sort:boolean):void;
         toXY(x:number, y:number):void;
         toXYByArr(pos:Array<number>):void;
         //position. x  = .y   = 0;
         to0():void;
         toScaleX(x: number):void;
         toScaleY(y: number):void;
         toScaleXY(scale: number):void;
         toOpacity(opacity: number):void;
    }
    interface Tween<T>{
        toPro(duration: number, props: any, opts?: ITweenOption): Tween<T>;
        byPro(duration: number, props: any, opts?: ITweenOption): Tween<T>;
        bezierTo(duration: number, end_pos: Vec3 | Vec2, c1: Vec3 | Vec2, c2: Vec3 | Vec2, opts?: any): Tween<T>;
        bezierToByArr(duration: number, end_pos: Array<number>, c: Array<number>, opts?: any): Tween<T>;
    }
}