import { Node, Tween, TweenAction, Vec2, Vec3, bezier } from "cc";

function getDealInfo(target: Node, name: string){
    switch(name){
        case "x":
            return [target.position.x, Node.prototype.toX];
        case "y":
            return [target.position.y, Node.prototype.toY];
        case "scaleX":
            return [target.scale.x, Node.prototype.toScaleX];
        case "scaleY":
            return [target.scale.y, Node.prototype.toScaleY];
        case "scale":
            return [target.scale.y, Node.prototype.toScaleXY];
        case "opacity":
            return [target._uiProps.localOpacity, Node.prototype.toOpacity];
    }
    return null;
}
export class TweenActionPro extends TweenAction {
    declare _props:any;
    declare _opts:any;
    startWithTarget (target: Record<string, unknown>) {
        const props = this._props;
        const relative = !!this._opts.relative;
        for (const property in props) {
            const prop: any = props[property];
            let info = getDealInfo(target as any, property);
            if(info){
                prop.func_adapter = info[1];
                prop.start = info[0];
                prop.current = info[0];

                const value = prop.value;
                prop.end = relative ? info[0] + value : value;
            }
        }
        super.startWithTarget(target);
    }

    update (t: number) {
        const target = this.target;
        if (!target) return;

        const props = this._props;
        const opts = this._opts;

        let easingTime = t;
        if (opts.easing) easingTime = opts.easing(t);

        const progress = opts.progress;
        for (const name in props) {
            const prop = props[name];
            const time = prop.easing ? prop.easing(t) : easingTime;
            const interpolation = prop.progress ? prop.progress : progress;

            const start = prop.start;
            const end = prop.end;
            if (typeof start === 'number') {
                prop.current = interpolation(start, end, prop.current, time);
            } else if (typeof start === 'object') {
                // const value = prop.value;
                for (const k in start) {
                    // if (value[k].easing) {
                    //     time = value[k].easing(t);
                    // }
                    // if (value[k].progress) {
                    //     interpolation = value[k].easing(t);
                    // }
                    prop.current[k] = interpolation(start[k], end[k], prop.current[k], time);
                }
            }
            if(prop.func_adapter)prop.func_adapter.call(target, prop.current)
            else target[name] = prop.current;
        }
        if (opts.onUpdate) { opts.onUpdate(this.target, t); }
        if (t === 1 && opts.onComplete) { opts.onComplete(this.target); }
    }

    // progress (start: number, end: number, current: number, t: number) {
    //     return current = start + (end - start) * t;
    // }
}

Tween.prototype.toPro = function(duration: number, props: any, opts?: any){
    opts = opts || Object.create(null);
    (opts as any).relative = false;
    const action = new TweenActionPro(duration, props, opts);
    this._actions.push(action);
    return this;
}
Tween.prototype.byPro = function(duration: number, props: any, opts?: any){
    opts = opts || Object.create(null);
    (opts as any).relative = true;
    const action = new TweenActionPro(duration, props, opts);
    this._actions.push(action);
    return this;
}
Tween.prototype.bezierTo = function(duration: number, end_pos: Vec3 | Vec2, c1: Vec3 | Vec2, c2: Vec3 | Vec2, opts?: any){
    opts = opts || Object.create(null);
    (opts as any).relative = false;
    let c0x = c1.x, c0y = c1.y,
        c1x = c2.x, c1y = c2.y;
    let sx = this._target.position.x;
    let sy = this._target.position.y;
    opts.progress = (start: number, end: number, current: number, ratio: number)=> {
        let x = bezier(sx, c0x, c1x, end_pos.x, ratio);
        let y = bezier(sy, c0y, c1y, end_pos.y, ratio);
        this._target.toY(y);
        return x;
    }
    return this.toPro(duration, {x: end_pos.x}, opts);
}
Tween.prototype.bezierToByArr = function(duration: number, end_pos: Array<number>, c: Array<number>, opts?: any){
    opts = opts || Object.create(null);
    (opts as any).relative = false;
    let c0x = c[0], c0y = c[1],
        c1x = c[2], c1y = c[3];
    let sx = this._target.position.x;
    let sy = this._target.position.y;
    opts.progress = (start: number, end: number, current: number, ratio: number)=> {
        let x = bezier(sx, c0x, c1x, end_pos[0], ratio);
        let y = bezier(sy, c0y, c1y, end_pos[1], ratio);
        this._target.toY(y);
        return x;
    }
    return this.toPro(duration, {x: end_pos[0]}, opts);
}